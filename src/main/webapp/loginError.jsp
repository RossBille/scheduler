<%-- 
    Document   : loginError
    Created on : Nov 23, 2012, 4:49:05 PM
    Author     : Ross Bille 3127333
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        Invalid username or password.<br />
        <a href="Init">Try again?</a>
    </body>
</html>
