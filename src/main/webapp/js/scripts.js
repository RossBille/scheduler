/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function validate()
{
    var to = document.getElementById("requiredUsers");
    var resultStatus = true;
    var message = "The following errors were detected:";
    if(!to)
    {
        resultStatus = false;
        message += "\n- Could not find any recipients";
    }
    
    var toValue = to.value;
    
    if(toValue == "")
    {
        resultStatus = false;
        message += "\n- Could not find any recipients";
    }
    var desc = document.getElementById("description").value;
    desc = desc.replace(/(\r\n|\n|\r)/gm, '<br />');
    document.getElementById("description").value = desc;
    if(!resultStatus)
        alert(message);
    return resultStatus;
}

