<%-- 
    Document   : createMeeting
    Created on : Dec 4, 2012, 2:15:38 PM
    Author     : rossbille
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@taglib prefix="sx" uri="/struts-dojo-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <s:head/>
        <sx:head/>
        <s:if test="type==1">
            <title>Create Meeting</title>
            <script type="text/javascript" src="/Scheduler/js/scripts.js"></script>
        </s:if>
        <s:else>
            <title>Block time-slot</title>
        </s:else>
    </head>
    <body>
        <div class="meeting">
            <s:form action="generateTimes" theme="simple" onsubmit="return validate()" >
                <s:if test="type==1">
                    <h1>Create Meeting</h1>
                    <table>
                        <tr>
                            <td>
                                To:
                            </td>
                            <td>
                                <s:select label="to" list="users" name="params.to" value="default" multiple="true" required="true" key="requiredUsers" id="requiredUsers"/>
                            </td>
                            <td>
                                Cc:
                            </td>
                            <td>
                                <s:select label="Cc" list="users" name="params.optional" value="default" multiple="true" required="true"/>
                            </td>
                        </tr>
                    </table>
                    Subject: <s:textfield cssClass="label" placeholder="Subject" id="subject" name="params.subject" size="100" theme="simple"/><br />
                    Message:<br />
                    <s:textarea cssClass="label" id="description" name="params.description" cols="80" theme="simple"/><br />
                </s:if>
                <s:else>
                    <h1>Block time-slot</h1>
                    <s:hidden id="to" name="params.to" value="" />
                    <s:hidden id="cc" name="params.optional" value="" />
                    <s:hidden id="subject" name="params.subject" value="Blocked" />
                    <s:hidden id="description" name="params.description" value="Timeslot has been blocked" />
                </s:else>
                <div class="time">
                    Date Range:<br />
                    <sx:datetimepicker name="params.date" label="Date" displayFormat="yyyy-MM-dd" value="%{'today'}"/><br />
                    Duration: <s:select label="duration" headerKey="1" headerValue="1" list="durations" name="params.duration" value="1"/><br />
                    <s:radio label="Week or Day" name="params.dateRange" list="weekOrDay" value="defaultWoD" />
                </div>
                    <s:submit type="button" id="submit" value="%{'Submit'}" label="Submit"/>
            </s:form>
        </div>
    </body>
</html>
