<%-- 
    Document   : errorPage
    Created on : Dec 12, 2012, 11:39:46 PM
    Author     : rossbille
--%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error!</title>
    </head>
    <body>
        <h1>Error!</h1>
        Something has gone wrong while processing your request.<br />
        Not to worry, things will be back to normal if you <s:a href="init.action">Click Here</s:a>.
    </body>
</html>
