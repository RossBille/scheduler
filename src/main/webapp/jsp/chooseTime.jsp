<%-- 
    Document   : chooseTime
    Created on : Dec 4, 2012, 2:15:57 PM
    Author     : rossbille
--%>
<%@taglib prefix="s" uri="/struts-tags" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/Scheduler/css/styles.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Select a Time</title>
    </head>
    <body>
        <div class="possibleTimes">
            <table>
                <s:set name="week" value="times" />
                <tr>
                    <%--Build the table head--%>
                    <s:iterator value="%{#week.head}" var="cell">
                        <th class="<s:property value="%{#cell.format}"/>">
                            <s:property value="%{#cell.text}" escapeHtml="false"/>
                        </th>
                    </s:iterator>
                </tr>
                <%--Build the table body--%>
                <s:iterator value="%{#week.weeklySchedule}" var="x">
                    <tr>
                        <s:iterator value="%{#x.row}" var="cell">
                            <td class="<s:property value="%{#cell.format}"/>">
                                <s:if test="%{#cell.format == 'head'}">
                                    <s:property value="%{#cell.text}"/>
                                </s:if>
                                <s:else>
                                    <s:if test="%{#cell.text == '&nbsp;'}">
                                        &nbsp;
                                    </s:if>
                                    <s:else>
                                        <s:url action="finalise" var="urlTag">
                                            <s:param name="selectedTime"><s:property value="%{#cell.text}"/></s:param>
                                        </s:url>
                                        <s:a href="%{urlTag}">Select</s:a>
                                    </s:else>
                                </s:else>
                            </td>
                        </s:iterator>
                    </tr>
                </s:iterator>
            </table>
        </div>
    </body>
</html>
