<%-- 
    Document   : homePage
    Created on : Dec 4, 2012, 2:15:04 PM
    Author     : rossbille
--%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/Scheduler/css/styles.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
    </head>
    <body>
        <div class="container">
            <div class="left">
                <div class="month">
                    <s:include value="include/sideBar.jsp" />
                </div>
            </div>
            <div class="right">
                <div class="top">
                    <div class="date">
                        <s:property value="title" />
                    </div>
                    <div class="login">
                        <s:property value="%{#session.sessionUser.firstname}" />
                        <s:property value="%{#session.sessionUser.lastname}" /><br />
                        <s:submit onclick="parent.location='logout.action'" value="Log Out" theme="simple"/>
                    </div>
                </div>
                <div class="lower">
                    <div class="weekly_schedule">
                        <s:include value="include/week.jsp" />
                    </div>  
                </div>
            </div>
        </div>
    </body>
</html>