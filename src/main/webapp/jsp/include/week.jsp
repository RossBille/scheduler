<%-- 
    Document   : week
    Created on : Dec 6, 2012, 9:18:18 AM
    Author     : rossbille
--%>
                <%@page trimDirectiveWhitespaces="true"%>
                <%@taglib prefix="s" uri="/struts-tags" %>
                <s:set name="week" value="weeklySchedule" />
                <table>
                    <%--Build the table head--%>
                    <tr>
                        <s:iterator value="%{#week.head}" var="cell">
                            <th class="<s:property value="%{#cell.format}"/>">
                                <s:property value="%{#cell.text}" escapeHtml="false"/>
                            </th>
                        </s:iterator>
                    </tr>
                    <%--Build the table body--%>
                    <s:iterator value="%{#week.weeklySchedule}" var="x">
                        <tr>
                            <s:iterator value="%{#x.row}" var="cell">
                                <td class="<s:property value="%{#cell.format}"/>">
                                    <s:if test="%{#cell.format == 'head'}">
                                        <s:property value="%{#cell.text}" escapeHtml="false"/>
                                    </s:if>
                                    <s:else>
                                        <s:if test="%{#cell.appointment == null}">
                                            &nbsp;
                                        </s:if>
                                        <s:else>
                                            <s:url action="details" var="appointmentDetails">
                                                <s:param name="appointmentID" value="%{#cell.appointment.appointmentId}"/>
                                            </s:url>
                                            <s:a href="%{appointmentDetails}">
                                                <div class="link">
                                                    <s:property value="%{#cell.text}" escapeHtml="false"/>
                                                </div>
                                            </s:a>                                            
                                        </s:else>
                                    </s:else>
                                </td>
                            </s:iterator>
                        </tr>
                    </s:iterator>
                </table>