<%-- 
    Document   : sideBar
    Created on : Dec 6, 2012, 9:18:30 AM
    Author     : rossbille
--%>
                <%@page trimDirectiveWhitespaces="true"%>
                <%@taglib prefix="s" uri="/struts-tags" %>  
                <div>
                    <s:set name="month" value="month0" />
                    <div class="month_name"><s:property value="%{#month.name}" /></div>
                    <table>
                        <tr>
                            <th>S</th>
                            <th>M</th>
                            <th>T</th>
                            <th>W</th>
                            <th>T</th>
                            <th>F</th>
                            <th>S</th>
                        </tr>
                        <s:iterator value="%{#month.month}" var="week">
                            <tr class="<s:property value="%{#week.format}"/>">
                                <s:iterator value="%{#week.row}" var="cell">
                                    <td class="<s:property value="%{#cell.format}"/>">
                                        <s:if test="%{#cell.contents != -1}">
                                            <s:url action="init.action" var="urlTag">
                                                <s:param name="time"><s:property value="%{#cell.param}"/></s:param>
                                            </s:url>
                                            <s:a href="%{urlTag}">
                                                <s:if test="%{#cell.activity}">
                                                    <div class="activity"><s:property value="%{#cell.contents}" escapeHtml="false"/></div>
                                                </s:if>
                                                <s:else>
                                                    <s:property value="%{#cell.contents}" escapeHtml="false"/>
                                                </s:else>
                                            </s:a>
                                        </s:if>
                                        <s:else>
                                            <s:property value="%{#cell.contents}" escapeHtml="false"/>
                                        </s:else>
                                    </td>
                                </s:iterator>
                            </tr>
                        </s:iterator>
                    </table>
                </div>
                <div>
                    <s:set name="month" value="month1" />
                    <div class="month_name"><s:property value="%{#month.name}" /></div>
                    <table>
                        <tr>
                            <th>S</th>
                            <th>M</th>
                            <th>T</th>
                            <th>W</th>
                            <th>T</th>
                            <th>F</th>
                            <th>S</th>
                        </tr>
                        <s:iterator value="%{#month.month}" var="week">
                            <tr>
                                <s:iterator value="%{#week.row}" var="cell">
                                    <td class="<s:property value="%{#cell.format}"/>">
                                        <s:if test="%{#cell.contents != -1}">
                                            <s:url action="init.action" var="urlTag">
                                                <s:param name="time"><s:property value="%{#cell.param}"/></s:param>
                                            </s:url>
                                            <s:a href="%{urlTag}">
                                                <s:if test="%{#cell.activity}">
                                                    <div class="activity"><s:property value="%{#cell.contents}" escapeHtml="false"/></div>
                                                </s:if>
                                                <s:else>
                                                    <s:property value="%{#cell.contents}" escapeHtml="false"/>
                                                </s:else>
                                            </s:a>
                                        </s:if>
                                        <s:else>
                                            <s:property value="%{#cell.contents}" escapeHtml="false"/>
                                        </s:else>
                                    </td>
                                </s:iterator>
                            </tr>
                        </s:iterator>
                    </table>
                </div>
                <div>
                    <s:set name="month" value="month2" />
                    <div class="month_name"><s:property value="%{#month.name}" /></div>
                    <table>
                        <tr>
                            <th>S</th>
                            <th>M</th>
                            <th>T</th>
                            <th>W</th>
                            <th>T</th>
                            <th>F</th>
                            <th>S</th>
                        </tr>
                        <s:iterator value="%{#month.month}" var="week">
                            <tr>
                                <s:iterator value="%{#week.row}" var="cell">
                                    <td class="<s:property value="%{#cell.format}"/>">
                                        <s:if test="%{#cell.contents != -1}">
                                            <s:url action="init.action" var="urlTag">
                                                <s:param name="time"><s:property value="%{#cell.param}"/></s:param>
                                            </s:url>
                                            <s:a href="%{urlTag}">
                                                <s:if test="%{#cell.activity}">
                                                    <div class="activity">
                                                        <s:property value="%{#cell.contents}" escapeHtml="false"/>
                                                    </div>
                                                </s:if>
                                                <s:else>
                                                    <s:property value="%{#cell.contents}" escapeHtml="false"/>
                                                </s:else>
                                            </s:a>
                                        </s:if>
                                        <s:else>
                                            <s:property value="%{#cell.contents}" escapeHtml="false"/>
                                        </s:else>
                                    </td>
                                </s:iterator>
                            </tr>
                        </s:iterator>
                    </table>
                </div>
                <div class="navigation">
                    <s:url action="init.action" var="prev">
                        <s:param name="time"><s:property value="prevTime"/></s:param>
                    </s:url>
                    <s:submit type="image" src="/Scheduler/images/prev.jpg" onclick="parent.location='%{prev}'" value="Prev"  theme="simple" id="prev" />
                    <s:url action="init.action" var="next">
                        <s:param name="time"><s:property value="nextTime"/></s:param>
                    </s:url>
                     <s:submit type="image" src="/Scheduler/images/next.jpg" onclick="parent.location='%{next}'" value="Next"  theme="simple" id="next"/><br />
                     <s:url action="init.action" var="reset">
                        <s:param name="time"><s:property value="0"/></s:param>
                    </s:url>
                    <s:submit onclick="parent.location='%{reset}'" value="Today"  theme="simple" id="today" cssClass="button"/><br />
                    <s:submit onclick="parent.location='meeting.action'" value="Create Meeting"  theme="simple" id="cm" cssClass="button"/><br />
                    <s:submit onclick="parent.location='block.action'" value="Block Time"  theme="simple" id="bts" cssClass="button"/><br />
                </div>