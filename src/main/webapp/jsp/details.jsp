<%-- 
    Document   : details
    Created on : Dec 4, 2012, 2:15:44 PM
    Author     : rossbille
--%>
<%@page trimDirectiveWhitespaces="true"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/Scheduler/css/details.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Meeting Details</title>
    </head>
    <body>
        <div class="container">
            <h1>Details</h1>
            <div class="appointment" id="main">
                <s:set var="appointmentDetails" value="%{#session.appointment}"/>
                Subject: <s:property value="%{#appointmentDetails.appointment.subject}"/><br />
                Date: <s:property value="%{#appointmentDetails.date}"/><br />
                Time: <s:property value="%{#appointmentDetails.time}"/>
                <div class="users">
                    <div class="float">
                        <h4>Required:</h4>
                        <div class="required">
                            <table>
                                <s:iterator value="%{#appointmentDetails.requiredUsers}" var="user">
                                    <tr>
                                        <td>
                                            <s:property value="%{#user.firstname}"/> <s:property value="%{#user.lastname}"/>
                                        </td>
                                    </tr>
                                </s:iterator>
                            </table>
                        </div>
                    </div>
                    <s:if test="%{#appointmentDetails.optionalUsers.size != 0}">
                        <div class="float">
                            <h4>Guests:</h4>
                            <div class="guests">
                                <table>
                                     <s:iterator value="%{#appointmentDetails.optionalUsers}" var="user">
                                        <tr>
                                            <td>
                                                <s:property value="%{#user.firstname}"/> <s:property value="%{#user.lastname}"/>
                                            </td>
                                        </tr>
                                    </s:iterator>
                                </table>
                            </div>
                        </div>
                    </s:if>
                </div>                
                <div class="message">
                    Message:
                    <div class="contents">
                        <s:property value="%{#appointmentDetails.appointment.message}" escapeHtml="false"/>
                    </div>
                </div>
                <div class="action">
                    Action:
                    <s:if test="%{#appointmentDetails.approved || #appointmentDetails.attending}">
                        You are attending this meeting.<br />
                        <s:submit onclick="parent.location='reject.action'" value="Cancel"  theme="simple" id="cancel"/>
                    </s:if>
                    <s:else>
                        <s:if test="%{#appointmentDetails.required}">
                            <s:submit onclick="parent.location='accept.action'" value="Accept"  theme="simple" id="accept"/>
                            <s:submit onclick="parent.location='reject.action'" value="Reject"  theme="simple" id="reject"/>
                        </s:if>
                        <s:else>
                            Your attendance isn't mandatory.
                        </s:else>
                    </s:else>
                </div>
            </div>
            <s:if test="%{#session.overlap.size > 0}">
                <div class="warning">
                    The following appointments are affected by the above selected appointment:
                </div>
                <s:iterator value="%{#session.overlap}" var="details">
                    <div class="appointment conflict">
                        <s:set var="appointmentDetails" value="%{#details}"/>
                        Subject: <s:property value="%{#appointmentDetails.appointment.subject}"/><br />
                        Date: <s:property value="%{#appointmentDetails.date}"/><br />
                        Time: <s:property value="%{#appointmentDetails.time}"/>
                        <div class="users">
                            <div class="float">
                                <h4>Required:</h4>
                                <div class="required">
                                    <table>
                                        <s:iterator value="%{#appointmentDetails.requiredUsers}" var="user">
                                            <tr>
                                                <td>
                                                    <s:property value="%{#user.firstname}"/> <s:property value="%{#user.lastname}"/>
                                                </td>
                                            </tr>
                                        </s:iterator>
                                    </table>
                                </div>
                            </div>
                            <s:if test="%{#appointmentDetails.optionalUsers.size != 0}">
                                <div class="float">
                                    <h4>Guests:</h4>
                                    <div class="guests">
                                        <table>
                                             <s:iterator value="%{#appointmentDetails.optionalUsers}" var="user">
                                                <tr>
                                                    <td>
                                                        <s:property value="%{#user.firstname}"/> <s:property value="%{#user.lastname}"/>
                                                    </td>
                                                </tr>
                                            </s:iterator>
                                        </table>
                                    </div>
                                </div>
                            </s:if>
                        </div>                
                        <div class="message">
                            Message:
                            <div class="contents">
                                <s:property value="%{#appointmentDetails.appointment.message}" escapeHtml="false"/>
                            </div>
                        </div>
                        <div class="action">
                            Action:
                            <s:if test="%{#appointmentDetails.approved || #appointmentDetails.attending}">
                                You are attending this meeting.
                                
                            </s:if>
                            <s:else>
                                <s:if test="%{#!appointmentDetails.required}">
                                    Your attendance is not mandatory.
                               </s:if>
                               <s:else>
                                   Pending
                               </s:else>
                            </s:else>
                        </div>
                    </div>
                </s:iterator>
            </s:if>
        </div>
    </body>
</html>
