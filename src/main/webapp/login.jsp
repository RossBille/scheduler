<%-- 
    Document   : login
    Created on : Nov 23, 2012, 4:48:46 PM
    Author     : Ross Bille 3127333
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        <style type="text/css">
            div.center{text-align: center;}
        </style>
    </head>
    <body>
        <div class="center">
            <form action="j_security_check" method="post">
                <label for="j_username">Username:</label><input type="text" name="j_username" /><br />
                <label for="j_password">Password: </label><input type="password" name="j_password" /><br />
                <input type="submit" value="Log in" name="submit" id="submit" />
            </form>
        </div>
    </body>
</html>