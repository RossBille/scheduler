package com.scheduler.userInterface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.scheduler.database.Appointment;
import com.scheduler.database.DatabaseController;
import com.scheduler.database.User;
import com.scheduler.userInterface.tables.month.Month;
import com.scheduler.userInterface.tables.week.WeeklySchedule;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

/**
 *
 * @author rossbille
 */
public class Init extends ActionSupport
{
    private static final int BEGINNING = 99;//signify the start of a meeting
    private static final int END = Integer.MAX_VALUE;//signify the end of a meeting
    private static final int BLOCKED = -1;//signify a blocked timeslot
    private static final int MIDDLE = 1;
    private static final int SINGLE = Integer.MIN_VALUE;
    private LinkedList<Appointment> appointments;
    private Month month0, month1, month2;
    private User user;
    private long time, prevTime, nextTime;
    private String title;
    private WeeklySchedule weeklySchedule;
    @Override
    public String execute() throws Exception
    {

        //gain access to the session object
        Map<String, Object> mySession = ActionContext.getContext().getSession();
        //gain access to the user who is in the session 
        user = (User)mySession.get("sessionUser");
        //set up calendar
        Calendar now = Calendar.getInstance(Locale.US);
        //clear time values
        now.set(Calendar.MILLISECOND,0);
        now.set(Calendar.SECOND,0);
        now.set(Calendar.MINUTE,0);
        now.set(Calendar.HOUR_OF_DAY,0);
        appointments = (LinkedList<Appointment>) DatabaseController.getUsersAppointments(user.getUserId());
        if(time == 0L)
            time = now.getTimeInMillis();
        now.setTimeInMillis(time);
        //set the months to be displayed in the sidebar
        month0 = new Month(time, 0, appointments);
        month1 = new Month(time, 1, appointments);
        month2 = new Month(time, 2, appointments);  
        //set the title to be displayed on the page
        title = new SimpleDateFormat("dd MMMM yyyy").format(now.getTime());
        //set the parameters to be used in the next/prev buttons on the sidebar
        now.add(Calendar.MONTH, -3);
        prevTime = now.getTimeInMillis();
        now.add(Calendar.MONTH, +6);
        nextTime = now.getTimeInMillis();
        now.setTimeInMillis(time);
        //set up the weekly schedule
        weeklySchedule = new WeeklySchedule(time, appointments);
        mySession.put("appointments", appointments);
        return SUCCESS;
    }

    public long getPrevTime() {
        return prevTime;
    }

    public void setPrevTime(long prevTime) {
        this.prevTime = prevTime;
    }

    public long getNextTime() {
        return nextTime;
    }

    public void setNextTime(long nextTime) {
        this.nextTime = nextTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
    
    public Month getMonth1() {
        return month1;
    }

    public void setMonth1(Month month1) {
        this.month1 = month1;
    }

    public Month getMonth2() {
        return month2;
    }

    public void setMonth2(Month month2) {
        this.month2 = month2;
    }

    public LinkedList<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(LinkedList<Appointment> appointments) {
        this.appointments = appointments;
    }

    public Month getMonth0() {
        return month0;
    }

    public void setMonth0(Month month0) {
        this.month0 = month0;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public WeeklySchedule getWeeklySchedule() {
        return weeklySchedule;
    }

    public void setWeeklySchedule(WeeklySchedule weeklySchedule) {
        this.weeklySchedule = weeklySchedule;
    }
    
}
