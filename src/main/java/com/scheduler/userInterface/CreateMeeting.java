/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.userInterface;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.scheduler.beans.MeetingParams;
import com.scheduler.database.DatabaseController;
import com.scheduler.database.User;
import com.scheduler.userInterface.tables.week.PossibleTimes;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rossbille
 */
public class CreateMeeting extends ActionSupport
{
    private static final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    private static final int BLOCK = 0;
    private static final int MEETING = 1;
    private static final int BUILD = 2;
    private static final int FINALISE = 3;
    private int type;
    private List<String> weekOrDay;
    private MeetingParams params;
    private PossibleTimes times;
    private String selectedTime;
    private User currentUser;
    private List<String> users;
    private String defaultWoD;
    private List<Integer> durations;
    @Override
    public String execute() throws Exception
    {
        Map<String, Object> mySession = ActionContext.getContext().getSession();
        currentUser = (User) mySession.get("sessionUser");
        weekOrDay = new LinkedList<String>();
        weekOrDay.add("week");
        weekOrDay.add("day");
        //set up the list of selectable users
        users = new LinkedList<String>();
        for(User u: (List<User>)DatabaseController.listAll(User.class))
            if(!u.getUsername().equals(currentUser.getUsername()))
                users.add(u.getUsername());
        //set up duration list
        durations = new LinkedList<Integer>();
        for(int i=1; i<=14; i++)
            durations.add(i);
        mySession.put("type",type);
        if(type == MEETING)
            return "meeting";
        if(type == BLOCK)
            return "block";
        return "error";
    }
    
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String finalise() 
    {
        Map<String, Object> mySession = ActionContext.getContext().getSession();
        currentUser = (User) mySession.get("sessionUser");
        params = (MeetingParams) mySession.get("params"); 
        try {
            //parse any variables still not in their orrect form
            Date date = new Date();
            date.setTime(Long.parseLong(selectedTime));
            int duration = Integer.parseInt(params.getDuration());
            if(params.getOptional().isEmpty())
                params.setOptionalUserIds(new LinkedList<Integer>());
            else
                params.setOptionalUserIds(DatabaseController.convert(params.getOptional()));
            
            DatabaseController.createMeeting(date, duration, params.getSubject(), params.getDescription(), params.getRequiredUserIds(), params.getOptionalUserIds(), currentUser.getUserId());
            //clear params
            mySession.remove("params");
            mySession.remove("type");
            return SUCCESS;
        } catch (Exception e)
        {
            Logger.getLogger(CreateMeeting.class.getName()).log(Level.SEVERE, null, e);
            return ERROR;
        }
       
                

    }

    public String build() 
    {
        try 
        {
            Map<String, Object> mySession = ActionContext.getContext().getSession();
            //gain access to the user who is in the session 
            User user = (User)mySession.get("sessionUser");
            type = Integer.parseInt(mySession.get("type").toString());
            System.out.println("type = "+type);
            if(type == MEETING)
                params.setRequiredUserIds(DatabaseController.convert(params.getTo()));
            else
                params.setRequiredUserIds(new LinkedList<Integer>());
            times = new PossibleTimes(params, user.getUserId());
            //put params into the session object s they persist after this method is complete
            mySession.put("params", params);
            return "success";
        } catch (Exception ex) 
        {
            Logger.getLogger(CreateMeeting.class.getName()).log(Level.SEVERE, null, ex);
            return ERROR;
        }
    }

    public List<String> getWeekOrDay() {
        return weekOrDay;
    }

    public void setWeekOrDay(List<String> weekOrDay) {
        this.weekOrDay = weekOrDay;
    }

    public MeetingParams getParams() {
        return params;
    }

    public void setParams(MeetingParams params) {
        this.params = params;
    }

    public PossibleTimes getTimes() {
        return times;
    }

    public void setTimes(PossibleTimes times) {
        this.times = times;
    }   

    public String getSelectedTime() {
        return selectedTime;
    }

    public void setSelectedTime(String selectedTime) {
        this.selectedTime = selectedTime;
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public String getDefaultWoD() {
        return "week";
    }

    public List<Integer> getDurations() {
        return durations;
    }

    public void setDurations(List<Integer> durations) {
        this.durations = durations;
    }
    
    
}
