/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.userInterface.tables.week;

/**
 *
 * @author rossbille
 */
public class Row 
{
    private Cell[] row;
    private String format;

    public Row() 
    {
        this(null, "blank");
    }

    public Row(Cell[] row, String format) {
        this.row = row;
        this.format = format;
    }

    public Cell[] getRow() {
        return row;
    }

    public void setRow(Cell[] row) {
        this.row = row;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }
    
    
}
