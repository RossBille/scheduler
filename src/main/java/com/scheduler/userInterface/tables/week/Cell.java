/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.userInterface.tables.week;

import com.scheduler.database.Appointment;

/**
 *
 * @author rossbille
 */
public class Cell 
{
    private String format;
    private String text;
    private Appointment appointment;

    public Cell(String format, String text, Appointment appointment) {
        this.format = format;
        this.text = text;
        this.appointment = appointment;
    }

    public Cell() 
    {
        this("blank", "&nbsp;", null);
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }
    
}
