/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.userInterface.tables.week;

import com.scheduler.beans.MeetingParams;
import com.scheduler.database.Appointment;
import com.scheduler.database.DatabaseController;
import com.scheduler.database.exceptions.DatabaseException;
import com.scheduler.database.exceptions.NoSuchEntityException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;

/**
 *
 * @author rossbille
 */
public class PossibleTimes 
{
    private static final int BLOCKED = -1;//signify a blocked timeslot
    private Cell[] head;
    private Row[] weeklySchedule;
    private static final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    public PossibleTimes(MeetingParams params, int userID) throws NoSuchEntityException, DatabaseException, ParseException 
    {
        
        LinkedList<Appointment> appointments = new LinkedList<Appointment>();
        //build a list of all the involved users appointments
        LinkedList<Integer> requiredUserIds = params.getRequiredUserIds();
        requiredUserIds.add(userID);
        for(Integer i: requiredUserIds)
            appointments.addAll(DatabaseController.getUsersAppointments(i));
        
        //parse all parameters into their proper type
        Date date = formatter.parse(params.getDate());
        long selected = date.getTime();
        int duration = Integer.parseInt(params.getDuration());
        boolean isDateRange = !params.getDateRange().equals("day");
        //set up calendar
        Calendar now  = Calendar.getInstance(Locale.US);
        now.setTimeInMillis(selected);
        now.set(Calendar.MILLISECOND,0);
        now.set(Calendar.SECOND,0);
        now.set(Calendar.MINUTE,0);
        now.set(Calendar.HOUR_OF_DAY,6);//start from 6am
        //store selected date for highlighting
        int selectedDate = now.get(Calendar.DAY_OF_WEEK);
        
        //make it sunday
        now.set(Calendar.DAY_OF_WEEK,1);
        
        //build array of appointments
        int[][] formatting = new int[14][7];
        long[][] times = new long[14][7];
        for(int i=0 ; i<14 ; i++)
        {
            for(int j=0 ; j<7 ; j++)
            {
                times[i][j] = now.getTimeInMillis();
                if(isDateRange || now.get(Calendar.DAY_OF_WEEK) == selectedDate)
                {
                    for(Appointment a: appointments)
                    {
                        Calendar tempTime = Calendar.getInstance(Locale.US);
                        tempTime.setTime(a.getTime());
                        if(tempTime.equals(now))
                        {
                            for(int k=0 ; k<a.getDuration() ; k++)
                            {
                                //if there is an aprroved appointment or
                                //if the current user has already an approved appointment
                                //then it makes no sense that they would want to suggest here
                                if(DatabaseController.isApproved(a.getAppointmentId())||DatabaseController.hasAproved(userID, a.getAppointmentId()))
                                    formatting[i+k][j] = BLOCKED;
                                //check if any of the included users have aprroved this appointment
                                for(Integer u: params.getRequiredUserIds())
                                    if(DatabaseController.hasAproved(u, a.getAppointmentId()))
                                        formatting[i+k][j] = BLOCKED;
                            }
                        }
                    }
                }else{
                    formatting[i][j] = BLOCKED;
                }
                now.add(Calendar.DAY_OF_WEEK,1);
            }
            //go back to sunday
            now.add(Calendar.DAY_OF_WEEK,-7);
            //increment hour
            now.add(Calendar.HOUR_OF_DAY, 1);  
        }
        //block times that are too small for appoinments
        for(int i=0 ; i<14 ; i++)
        {
            for(int j=0 ; j<7 ; j++)
            {
                int freespace = 0;
                int k = i;
                while(k<14 && formatting[k][j] != BLOCKED)
                {
                    freespace++;
                    k++;
                }
                if(freespace<duration)
                {
                    k = i;
                    while(k<14 && formatting[k][j] != BLOCKED)
                    {
                        formatting[k][j] = BLOCKED;
                        k++;
                    }
                }
                    
            }
        }
        //block saturdays and sundays
        for(int i=0 ; i<7 ; i+=6)
            for(int j=0 ; j<14 ; j++)
                formatting[j][i] = BLOCKED;
        
        //block boundary times
        for(int j=0 ; j<7 ; j++)
            for(int k = 13 ; k>14-duration; k--)
                formatting[k][j] = BLOCKED;
        //table header
        SimpleDateFormat tableHeader = new SimpleDateFormat("EEE - dd/MM");
        head = new Cell[8];
        head[0] = new Cell("blank", "&nbsp;", null);
        for(int i=1;i<head.length;i++)
        {
            now.set(Calendar.DAY_OF_WEEK, i);
            if(i==selectedDate)
                head[i] = new Cell("selected", tableHeader.format(now.getTime()), null);
            else
                head[i] = new Cell("blank", tableHeader.format(now.getTime()), null);
        }
        
        //build body
        weeklySchedule = new Row[14];
        for(int i=0 ; i<14 ; i++)
        {
            Cell[] tempRow = new Cell[8];
            tempRow[0] = new Cell("head", (i+6)+":00", null);
            for(int j=0 ; j<7 ; j++)
            {
                Cell tempCell = new Cell();
                //start building cells                    
                if(formatting[i][j] == BLOCKED)
                {
                    tempCell.setFormat("blocked");
                }else{
                    tempCell.setFormat("blank");
                    tempCell.setText(String.valueOf(times[i][j]));
                }
                tempRow[1+j] = tempCell;
            }
            weeklySchedule[i] = new Row(tempRow, "blank");
        }        
    }

    public Cell[] getHead() {
        return head;
    }

    public void setHead(Cell[] head) {
        this.head = head;
    }

    public Row[] getWeeklySchedule() {
        return weeklySchedule;
    }

    public void setWeeklySchedule(Row[] weeklySchedule) {
        this.weeklySchedule = weeklySchedule;
    }    
}
