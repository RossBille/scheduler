/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.userInterface.tables.week;

import com.scheduler.database.Appointment;
import com.scheduler.database.DatabaseController;
import com.scheduler.database.exceptions.DatabaseException;
import com.scheduler.database.exceptions.NoSuchEntityException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Locale;

/**
 *
 * @author rossbille
 */
public class WeeklySchedule 
{
    private static final int BEGINNING = 99;//signify the start of a meeting
    private static final int END = Integer.MAX_VALUE;//signify the end of a meeting
    private static final int BLOCKED = -1;//signify a blocked timeslot
    private static final int MIDDLE = 1;
    private static final int SINGLE = Integer.MIN_VALUE;
    private Cell[] head;
    private Row[] weeklySchedule;

    public WeeklySchedule(long selected, LinkedList<Appointment> appointments) throws DatabaseException, NoSuchEntityException 
    {
        SimpleDateFormat tableHeader = new SimpleDateFormat("EEE dd/MM");
        //setup calendar
        Calendar now  = Calendar.getInstance(Locale.US);
        now.setTimeInMillis(selected);
        now.set(Calendar.MILLISECOND,0);
        now.set(Calendar.SECOND,0);
        now.set(Calendar.MINUTE,0);
        now.set(Calendar.HOUR_OF_DAY,6);//start from 6am
        int selectedDate = now.get(Calendar.DAY_OF_WEEK);
        //make it sunday
        now.set(Calendar.DAY_OF_WEEK,1);
        
        //build array of appointments
        Appointment[][] app = new Appointment[14][7];
        int[][] formatting = new int[14][7];
        
        for(int i=0 ; i<14 ; i++)
        {
            for(int j=0 ; j<7 ; j++)
            {
                for(Appointment a: appointments)
                {
                    Calendar tempTime = Calendar.getInstance(Locale.US);
                    tempTime.setTime(a.getTime());
                    if(tempTime.equals(now))
                    {
                        //check for overlap at each hour
                        if(app[i][j] != null)
                        {
                            //add to existing overlapped list
                            app[i][j].getOverlap().add(a);
                            //point current appointments overlap to 
                            //existing appointments overlap list
                            a.setOverlap(app[i][j].getOverlap());
                        }
                        if(!DatabaseController.isMeeting(a.getAppointmentId()))
                        {
                            //user blocked timeslot
                            for(int k=0 ; k<a.getDuration() ; k++)
                            {
                                app[i+k][j] = a;
                                formatting[i+k][j] = BLOCKED;
                            }
                        }else{
                            //meeting
                            if(a.getDuration() > 1)
                            {
                                app[i][j] = a;
                                formatting[i][j] = BEGINNING;
                                for(int k=1 ; k<a.getDuration() ; k++)
                                {
                                    app[i+k][j] = a;
                                    formatting[i+k][j] = k;
                                }
                                formatting[i+a.getDuration()-1][j] = END;
                            }else{
                                app[i][j] = a;
                                formatting[i][j] = SINGLE;
                            }
                        }
                    }
                }
                now.add(Calendar.DAY_OF_WEEK,1);
            }
            //go back to sunday
            now.add(Calendar.DAY_OF_WEEK,-7);
            //increment hour
            now.add(Calendar.HOUR_OF_DAY, 1);            
        }
        //build table
        //table header
        head = new Cell[8];
        head[0] = new Cell("plain", "&nbsp;", null);
        for(int i=1;i<head.length;i++)
        {
            now.set(Calendar.DAY_OF_WEEK, i);
            if(i==selectedDate)
                head[i] = new Cell("selected_date", tableHeader.format(now.getTime()), null);
            else
                head[i] = new Cell("blank", tableHeader.format(now.getTime()), null);
        }
        //build body
        weeklySchedule = new Row[14];
        for(int i=0 ; i<14 ; i++)
        {
            Cell[] tempRow = new Cell[8];
            tempRow[0] = new Cell("head", (i+6)+":00", null);
            for(int j=0 ; j<7 ; j++)
            {
                Cell tempCell = new Cell();
                Appointment a = app[i][j];
                //check if the appointment is approved or not
                //start building cells                    
                //if the formatting matrix idetifies there is the beginning of a meeting here
                if(formatting[i][j] == BEGINNING || formatting[i][j] == SINGLE)
                {
                    //check if the appointment has been approved or is pending
                    tempCell.setAppointment(a);
                    if(DatabaseController.isApproved(a.getAppointmentId()))
                    {
                        //if its approved we dont need to worry about overlap as they can't exist
                        tempCell.setFormat("start_approved");
                        tempCell.setText("Meeting<br /> Approved");
                    }else{
                        //if the appointment isnt approved
                        //check if its overlapping another non-approved appointment
                        if(a.getOverlap().size()>1)
                        {
                            //mark it if its overlapped
                            tempCell.setFormat("start_pending");
                            tempCell.setText("*Awaiting*<br /> Approval");
                        }else{
                            tempCell.setFormat("start_pending");
                            tempCell.setText("Awaiting<br /> Approval");
                        }
                    }
                    if(formatting[i][j] == SINGLE)
                    {
                         if(DatabaseController.isApproved(a.getAppointmentId()))
                            tempCell.setFormat("single_approved"); 
                        else
                            tempCell.setFormat("single_pending");
                    }
                }else if(formatting[i][j] == END)
                {
                    //simmilar to above, we have the end of an appointment block here
                    //is it an approved or pending appointment?
                    //format respectively
                    tempCell.setAppointment(a);
                    if(DatabaseController.isApproved(a.getAppointmentId()))                    
                        tempCell.setFormat("end_approved");
                    else
                        tempCell.setFormat("end_pending");
                }
                else if(formatting[i][j] >= MIDDLE)
                {
                    //simmilar to above, we have the end of an appointment block here
                    //is it an approved or pending appointment?
                    //format respectively
                    tempCell.setAppointment(a);
                    if(DatabaseController.isApproved(a.getAppointmentId()))
                        tempCell.setFormat("middle_approved");
                    else
                        tempCell.setFormat("middle_pending");
                }else if(formatting[i][j] == BLOCKED)
                {
                    //grey this cell out if its idicated as a blocked time
                    tempCell.setFormat("blocked");
                }
                tempRow[1+j] = tempCell;
            }
            weeklySchedule[i] = new Row(tempRow, "blank");
        }
    }

    public Cell[] getHead() {
        return head;
    }

    public void setHead(Cell[] head) {
        this.head = head;
    }

    public Row[] getWeeklySchedule() {
        return weeklySchedule;
    }

    public void setWeeklySchedule(Row[] weeklySchedule) {
        this.weeklySchedule = weeklySchedule;
    }
    
}






