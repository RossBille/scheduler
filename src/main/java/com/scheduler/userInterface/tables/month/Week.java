/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.userInterface.tables.month;

/**
 *
 * @author rossbille
 */
public class Week 
{
    private Cell[] row;
    private String format;

    public Week(Cell[] row, String format) {
        this.row = row;
        this.format = format;
    }

    public Week() {
    }

    public Cell[] getRow() {
        return row;
    }

    public void setRow(Cell[] row) {
        this.row = row;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }
    
}
