/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.userInterface.tables.month;

/**
 *
 * @author rossbille
 */
public class Cell 
{
    private String format;
    private String contents;
    private long param;
    private boolean activity;

    public Cell(String format, String contents, long param) {
        this.format = format;
        this.contents = contents;
        this.param = param;
        this.activity = false;
    }

    public Cell() 
    {
        this.format = "blank";
        this.contents = "&nbsp;";
        this.param = -1;
        this.activity = false;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public long getParam() {
        return param;
    }

    public void setParam(long param) {
        this.param = param;
    }

    public boolean isActivity() {
        return activity;
    }

    public void setActivity(boolean activity) {
        this.activity = activity;
    }
    
}
