/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.userInterface.tables.month;

import com.scheduler.database.Appointment;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Locale;

/**
 *
 * @author rossbille
 */
public class Month 
{
    private Week[] month;
    private String name;

    public Month(long selected, int monthOffset, LinkedList<Appointment> appointments) 
    {
        //build calendar
        Calendar now = Calendar.getInstance(Locale.US);
        //set to the correct date
        now.setTimeInMillis(selected);
        now.add(Calendar.MONTH, monthOffset);
        //clear time values
        now.set(Calendar.MILLISECOND,0);
        now.set(Calendar.SECOND,0);
        now.set(Calendar.MINUTE,0);
        now.set(Calendar.HOUR_OF_DAY,0);
        
        //find which cell to start the calendar on
        now.set(Calendar.DAY_OF_MONTH,1);
        int current = now.get(Calendar.DAY_OF_WEEK)*-1;
        
        //set flags
        Calendar selectedCalendar = Calendar.getInstance(Locale.US);
        selectedCalendar.setTimeInMillis(selected);
        int selectedMonth =  selectedCalendar.get(Calendar.MONTH);
        int selectedDay =  selectedCalendar.get(Calendar.DAY_OF_MONTH);
        int thisMonth = now.get(Calendar.MONTH);
        
        //set limits
        int days_in_month = now.getActualMaximum(Calendar.DAY_OF_MONTH);
        int weeks_in_month = now.getActualMaximum(Calendar.WEEK_OF_MONTH);
        
        //set the name of the month
        name = new SimpleDateFormat("MMMM - yyyy").format(now.getTimeInMillis());
        //build month
        month = new Week[weeks_in_month];
        for(int i=0; i<month.length;i++)
        {
            Cell[] tempRow = new Cell[7];
            Week tempWeek = new Week();
            tempWeek.setFormat("blank");
            for(int j=0;j<7;j++)
            {
                Cell tempCell = new Cell();
                current++;
                if(current==0)
                    current=1;
                now.set(Calendar.DAY_OF_MONTH,current);
                if(!(current<=0 || current>days_in_month))//date is out of range
                {
                    tempCell.setContents(String.valueOf(current));
                    tempCell.setParam(now.getTimeInMillis());
                    
                    if(now.getTime().getTime() == selected)
                    {
                        tempCell.setFormat("selected");
                        tempWeek.setFormat("selected");
                    }else{
                        tempCell.setFormat("blank");
                    }
                    for(Appointment a: appointments)
                    {
                        Calendar tempTime = Calendar.getInstance(Locale.US);
                        tempTime.setTime(a.getTime());
                        if(now.get(Calendar.DAY_OF_YEAR) == tempTime.get(Calendar.DAY_OF_YEAR))
                        {
                            tempCell.setActivity(true);
                            break;
                        }
                    }
                }
                tempRow[j] = tempCell;
            }
            tempWeek.setRow(tempRow);
            month[i] = tempWeek;
        }    
    }

    public Week[] getMonth() 
    {
        return month;
    }

    public void setMonth(Week[] month) 
    {
        this.month = month;
    }

    public String getName() 
    {
        return name;
    }

    public void setName(String name) 
    {
        this.name = name;
    }
    
}
