/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.userInterface;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.scheduler.database.Appointment;
import com.scheduler.database.AppointmentDetails;
import com.scheduler.database.DatabaseController;
import com.scheduler.database.User;
import com.scheduler.database.exceptions.DatabaseException;
import com.scheduler.database.exceptions.NoSuchEntityException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rossbille
 */
public class MeetingDetails extends ActionSupport
{
    private int appointmentID;
    private Appointment appointment;
    private LinkedList<Appointment> overlap;
    public String prepareDetails()
    {
        //get all the appointments
        Map<String, Object> mySession = ActionContext.getContext().getSession();
        LinkedList<Appointment> appointments = (LinkedList<Appointment>) mySession.get("appointments");
        User currentUser = (User) mySession.get("sessionUser");
        for(Appointment a:appointments)
        {
            if(a.getAppointmentId() == appointmentID)
            {
                appointment = a;
                break;
            }
        }
        overlap = (LinkedList<Appointment>) appointment.getOverlap().clone();//clone as to not ruin the overlap list if a user uses the back button
        for (Iterator<Appointment> it = overlap.iterator(); it.hasNext();) 
        {
            Appointment a = it.next();
            if(!appointment.isDirectlyEffectedBy(a))
                it.remove();
            else if(appointment.getAppointmentId() == a.getAppointmentId())
                it.remove();
        }
        try 
        {
            //convert into appointmentDetail items
            AppointmentDetails app = new AppointmentDetails(appointment, currentUser.getUserId());
            //make appointment persist by storing in the session
            mySession.put("appointment", app);
        } 
        catch (Exception ex) {
            Logger.getLogger(MeetingDetails.class.getName()).log(Level.SEVERE, null, ex);
            return ERROR;
        }
        LinkedList<AppointmentDetails> overLappingDetails = new LinkedList<AppointmentDetails>();
        for(Appointment a: overlap)
        {
            try 
            {
                overLappingDetails.add(new AppointmentDetails(a, currentUser.getUserId()));
            } 
            catch (DatabaseException ex) {
                Logger.getLogger(MeetingDetails.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NoSuchEntityException ex) {
                Logger.getLogger(MeetingDetails.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //make overlap persist by storing in the session
        mySession.put("overlap", overLappingDetails);
        return SUCCESS;
    }
    public String accept()
    {
        
        Map<String, Object> mySession = ActionContext.getContext().getSession();
        //get the correct user out of the session
        User currentUser = (User) mySession.get("sessionUser");
        //get the correct appointment out of the session
        AppointmentDetails ad = (AppointmentDetails) mySession.get("appointment");
        //update its row
        try
        {
            DatabaseController.approve(ad.getAppointment().getAppointmentId(), currentUser.getUserId());
            //get any conflicting appointments from the session
            LinkedList<AppointmentDetails> overLappingDetails = (LinkedList<AppointmentDetails>) mySession.get("overlap");
            for(AppointmentDetails a: overLappingDetails)
                DatabaseController.deleteMeeting(a.getAppointment().getAppointmentId());
        }
        catch(Exception e)
        {
            Logger.getLogger(MeetingDetails.class.getName()).log(Level.SEVERE, null, e);
            return ERROR;
        }
        //clear session objects
        mySession.remove("overlap");
        mySession.remove("appointment");
        return SUCCESS;
        
    }
    public String reject()
    {
        Map<String, Object> mySession = ActionContext.getContext().getSession();
        //get the correct appointment out of the session
        AppointmentDetails ad = (AppointmentDetails) mySession.get("appointment");
        try
        {
            DatabaseController.deleteMeeting(ad.getAppointment().getAppointmentId());
        }
        catch(Exception e)
        {
            Logger.getLogger(MeetingDetails.class.getName()).log(Level.SEVERE, null, e);
            return ERROR;
        }
        //clear session objects
        mySession.remove("overlap");
        mySession.remove("appointment");
        return SUCCESS;
    }

    public int getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(int appointmentID) {
        this.appointmentID = appointmentID;
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public LinkedList<Appointment> getOverlap() {
        return overlap;
    }

    public void setOverlap(LinkedList<Appointment> overlap) {
        this.overlap = overlap;
    }
    
}
