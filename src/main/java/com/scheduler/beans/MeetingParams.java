/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.beans;

import java.util.LinkedList;


/**
 *
 * @author Ross Bille 3127333
 */
public class MeetingParams implements java.io.Serializable
{
    private String to;
    private String optional;
    private String subject;
    private String description;
    private String dateRange;
    private String date;
    private String duration;
    private LinkedList<Integer> requiredUserIds, optionalUserIds;

    public MeetingParams() 
    {
        
    }

    public MeetingParams(String to, String optional, String subject, String description, String dateRange, String date, String duration) {
        this.to = to;
        this.optional = optional;
        this.subject = subject;
        this.description = description;
        this.dateRange = dateRange;
        this.date = date;
        this.duration = duration;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getOptional() {
        return optional;
    }

    public void setOptional(String optional) {
        this.optional = optional;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDateRange() {
        return dateRange;
    }

    public void setDateRange(String dateRange) {
        this.dateRange = dateRange;
    }

    public LinkedList<Integer> getRequiredUserIds() {
        return requiredUserIds;
    }

    public void setRequiredUserIds(LinkedList<Integer> requiredUserIds) {
        this.requiredUserIds = requiredUserIds;
    }

    public LinkedList<Integer> getOptionalUserIds() {
        return optionalUserIds;
    }

    public void setOptionalUserIds(LinkedList<Integer> optionalUserIds) {
        this.optionalUserIds = optionalUserIds;
    }
    

    @Override
    public String toString() {
        return "MeetingParams{" + "to=" + to + ", optional=" + optional + ", subject=" + subject + ", description=" + description + ", dateRange=" + dateRange + ", date=" + date + ", duration=" + duration + '}';
    }
    
    
}
