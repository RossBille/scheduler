package com.scheduler.sessionManagement;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.config.ConfigurationManager;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.Dispatcher;
import org.apache.struts2.interceptor.SessionAware;


public class Logout extends ActionSupport implements SessionAware {
    
    private Map<String, Object> session;

    @Override
    public String execute() throws Exception
    {
        if (session.get("sessionUser") != null)
            session.remove("sessionUser");
        HttpServletRequest request = ServletActionContext.getRequest();
        //clear user
        request.logout();
        //clear session
        request.getSession().invalidate();
        return "success";
    }
    @Override
    public void setSession(Map<String, Object> myMap)
    {
        this.session = myMap;
    }
    public Map getSession()
    {
        return this.session;
    }
}
