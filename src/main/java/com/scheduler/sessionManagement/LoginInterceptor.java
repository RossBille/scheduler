package com.scheduler.sessionManagement;


import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import com.scheduler.database.DatabaseController;
import com.scheduler.database.User;
import com.scheduler.database.exceptions.DatabaseException;
import com.scheduler.database.exceptions.NoSuchEntityException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author rowanburgess
 */
public class LoginInterceptor extends AbstractInterceptor
{

    public LoginInterceptor()
    {}
    

    @Override
    public void destroy()
    {}
    

    @Override
    public void init()
    {}
    

    @Override
    public String intercept(ActionInvocation myActionInvocation) throws Exception
    {
        Map<String, Object> mySession = ActionContext.getContext().getSession();
        //try and get/cast an object matching the username out of the session
        User myUser = (User) mySession.get("sessionUser");
        if (myUser == null) // if user object wasnt found, redirect to login
            return "loginRedirect";
        return myActionInvocation.invoke();
    }
}
