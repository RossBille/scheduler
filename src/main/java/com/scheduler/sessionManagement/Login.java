package com.scheduler.sessionManagement;

import com.opensymphony.xwork2.ActionSupport;
import com.scheduler.database.DatabaseController;
import com.scheduler.database.User;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;


public class Login extends ActionSupport implements SessionAware {
    
    private Map<String, Object> session;
        
    @Override
    public String execute() throws Exception
    {
        HttpServletRequest request = ServletActionContext.getRequest();
        try
        {
            User u = DatabaseController.getUserByUsername(request.getRemoteUser());
            if(u != null)
            {
                session.put("sessionUser" , u);
                return "success";
            }
            throw new Exception();
        }
        catch(Exception e)
        {
            //clear user
            request.logout();
            //clear session
            request.getSession().invalidate();
            return "loginRedirect";
        }
    }
    @Override
    public void setSession(Map<String, Object> myMap) 
    {
        this.session = myMap;
    }
    public Map getSession()
    {
        return session;
    }	
}

