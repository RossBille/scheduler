package com.scheduler.database;
// Generated Dec 4, 2012 6:20:43 PM by Hibernate Tools 3.2.1.GA


import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;

/**
 * Appointment generated by hbm2java
 */
public class Appointment  implements java.io.Serializable {


    private Integer appointmentId;
    private Date time;
    private Integer duration;
    private String subject;
    private String message;
    private LinkedList<Appointment> overlap;

    public Appointment() {
        this.overlap = new LinkedList<Appointment>();
        overlap.add(this);
    }

    public Appointment(Date time) {
        this.time = time;
        this.overlap = new LinkedList<Appointment>();
        overlap.add(this);
    }
    public Appointment(Date time, Integer duration, String subject, String message) {
       this.time = time;
       this.duration = duration;
       this.subject = subject;
       this.message = message;
       this.overlap = new LinkedList<Appointment>();
        overlap.add(this);
    }
   
    public Integer getAppointmentId() {
        return this.appointmentId;
    }
    
    public void setAppointmentId(Integer appointmentId) {
        this.appointmentId = appointmentId;
    }
    public Date getTime() {
        return this.time;
    }
    
    public void setTime(Date time) {
        this.time = time;
    }
    public Integer getDuration() {
        return this.duration;
    }
    
    public void setDuration(Integer duration) {
        this.duration = duration;
    }
    public String getSubject() {
        return this.subject;
    }
    
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public String getMessage() {
        return this.message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public boolean isDirectlyEffectedBy(Appointment a)
    {
        //calculate end time for this appointment
        Calendar thisEnd = Calendar.getInstance(Locale.US);
        thisEnd.setTime(time);
        thisEnd.add(Calendar.HOUR_OF_DAY, this.duration);
        //calculate end time for the comparing appointment
        Calendar aEnd = Calendar.getInstance(Locale.US);
        aEnd.setTime(a.getTime());
        aEnd.add(Calendar.HOUR_OF_DAY, a.getDuration());
        
        //check for conflict
        return (a.getTime().before(this.time) && aEnd.getTime().after(this.time)) ||
               (this.time.before(a.getTime()) && thisEnd.getTime().after(a.getTime()))||
               (this.time.equals(a.getTime()) && this.duration==a.getDuration());
    }
    
    public LinkedList<Appointment> getOverlap() {
        return overlap;
    }

    public void setOverlap(LinkedList<Appointment> overlap) {
        this.overlap = overlap;
    }

	
    @Override
    public String toString() {
        return "Appointment{" + "appointmentId=" + appointmentId + ", time=" + time + ", duration=" + duration + ", subject=" + subject + ", message=" + message + '}';
    }
}


