/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.database;

import static com.scheduler.database.HibernateUtil.getSessionFactory;
import static com.scheduler.database.HibernateUtil.tryRollBack;
import com.scheduler.database.exceptions.DatabaseException;
import com.scheduler.database.exceptions.NoSuchEntityException;
import com.scheduler.database.exceptions.UpdateFailException;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Ross Bille 3127333
 */
public class DatabaseController 
{
    public static List listAll(Class type) throws DatabaseException
    {
        Session session = null;
        try
        {
            session = getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List list = session.createCriteria(type).list();
            session.getTransaction().commit();
            return list;
        }
        catch(HibernateException e)
        {
            tryRollBack(session.getTransaction());
            throw new DatabaseException("ListAll", e);
        }
    }
    public static Object getById(Class type, int id) throws DatabaseException, NoSuchEntityException
    {
        Session session = null;
        try
        {
            session = getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Object o = session.get(type, id);
            session.getTransaction().commit();
            if(o==null) {
                throw new NoSuchEntityException(String.valueOf(id));
            }
            return o;
        }
        catch(HibernateException e)
        {
            tryRollBack(session.getTransaction());
            throw new DatabaseException("getById", e);
        }
    }
    public static List<Appointment> getUsersAppointments(int userID) throws DatabaseException, NoSuchEntityException
    {
        Session session = null;
        List<AppointmentHistory> ah = null;
        try
        {
            //firstly get all the AppointmentIDs that this user is involved in
            session = getSessionFactory().getCurrentSession();
            session.beginTransaction();
            ah = (List<AppointmentHistory>) session.createCriteria(AppointmentHistory.class)
                    .add(Restrictions.like("userId",userID))
                    .list();
            session.getTransaction().commit();
        }
        catch(HibernateException e)
        {
            tryRollBack(session.getTransaction());
            throw new DatabaseException("getUserAppointments", e);
        }
        //make a list of all the appointments
        List<Appointment> list = new LinkedList<Appointment>();
        for(AppointmentHistory a : ah)
            list.add((Appointment)getById(Appointment.class, a.getAppointmentId()));
        return list;
    }
    public static User getUserByUsername(String username) throws DatabaseException, NoSuchEntityException
    {
        Session session = null;
        try
        {
            session = getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List users = session.createCriteria(User.class)
                    .add(Restrictions.like("username",username))
                    .list(); 
            session.getTransaction().commit();
            if(users.isEmpty())
                throw new NoSuchEntityException(username);
            return (User) users.get(0);
        }
        catch(HibernateException he)
        {
            tryRollBack(session.getTransaction());
            throw new DatabaseException("getuserByUserName", he);
        }
    }
    public static AppointmentHistory getAppointmentHistory(int userID, int appointmentID) throws DatabaseException, NoSuchEntityException
    {
        Session session = null;
        try
        {
            session = getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<AppointmentHistory> ah = (List<AppointmentHistory>)session.createCriteria(AppointmentHistory.class)
                    .add(Restrictions.like("userId", userID))
                    .add(Restrictions.like("appointmentId", appointmentID))
                    .list();
            session.getTransaction().commit();
            if(ah.isEmpty())
                throw new NoSuchEntityException(userID+","+appointmentID);
            return (AppointmentHistory)ah.get(0);
        }
        catch(HibernateException e)
        {
            tryRollBack(session.getTransaction());
            throw new DatabaseException("getAppointmentHistory", e);
        }
    }
    //return true if a certain user has approved the appointment
    public static boolean hasAproved(int userID, int appointmentID) throws DatabaseException
    {
        AppointmentHistory ah;
        try {
            ah = getAppointmentHistory(userID, appointmentID);
        } catch (NoSuchEntityException ex) {
            return false;
        }
        if(ah == null)
            return false;
        return ah.isApproved();
    }
    //return true if all required users have approved the appointment
    public static boolean isApproved(int appointmentID) throws DatabaseException, NoSuchEntityException
    {
        Session session = null;
        try
        {
            session = getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<AppointmentHistory> ah = (List<AppointmentHistory>) session.createCriteria(AppointmentHistory.class)
                    .add(Restrictions.like("appointmentId", appointmentID))
                    .list();
            session.getTransaction().commit();
            if(ah.isEmpty())
                throw new NoSuchEntityException(String.valueOf(appointmentID));
            for(AppointmentHistory a: ah)
                if(a.isRequired() && !a.isApproved())
                    return false;
            return true;
        }
        catch(HibernateException he)
        {
            tryRollBack(session.getTransaction());
            throw new DatabaseException("isApproved", he);
        }
    }
    public static boolean isMeeting(int appointmentID) throws DatabaseException
    {
        Session session = null;
        try
        {
            session = getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List list = session.createCriteria(AppointmentHistory.class)
                    .add(Restrictions.like("appointmentId", appointmentID))
                    .list();
            session.getTransaction().commit();
            return list.size() > 1;
        }
        catch(HibernateException he)
        {
            tryRollBack(session.getTransaction());
            throw new DatabaseException("isMeeting", he);
        }
        catch(Exception e)
        {
            return false;
        }
    }
    //convert string of users to list of userIds
    public static LinkedList<Integer> convert(String usersString) throws NoSuchEntityException, DatabaseException
    {
        String[] users = usersString.split(", ");//split the string
        LinkedList<Integer> userIds = new LinkedList<Integer>();
        for(int i=0 ; i<users.length ; i++)
            if(!users[i].equals("-1"))
                userIds.add(getUserByUsername(users[i]).getUserId());
        return userIds;   
    }
    public static void approve(int appointmentID, int userID) throws DatabaseException, NoSuchEntityException
    {
        //pull the row from the database
        AppointmentHistory appointmentHistory = getAppointmentHistory(userID, appointmentID);
        //modify it
        appointmentHistory.setApproved(true);
        //update it
        update(appointmentHistory);
    }
    public static boolean isRequired(int userID, int appointmentID) throws DatabaseException, NoSuchEntityException
    {
        //pull the row from the database
        AppointmentHistory appointmentHistory = getAppointmentHistory(userID, appointmentID);
        return appointmentHistory.isRequired();
    }
    public static LinkedList<User> allInvolvedUsers(int appointmentID, boolean required) throws DatabaseException, NoSuchEntityException
    {
        Session session;
        //get all userIDs connected to this appointmentID
        List<AppointmentHistory> ah;
        try
        {
            session = getSessionFactory().getCurrentSession();
            session.beginTransaction();
            ah = session.createCriteria(AppointmentHistory.class)
                    .add(Restrictions.like("appointmentId", appointmentID))
                    .add(Restrictions.like("required", required)).list();
            session.getTransaction().commit();
        }
        catch(HibernateException e)
        {
            throw new DatabaseException("allInvolved",e);
        }
        //create a list of users and populate with users who fulfill the criteria
        LinkedList<User> users = new LinkedList<User>();
        for(AppointmentHistory a: ah)
            users.add((User)getById(User.class, a.getUserId()));
        return users;
    }
    public static void deleteMeeting(int appointmentID) throws DatabaseException
    {
        deleteAppointmentHistory(appointmentID);
        try 
        {
            delete(getById(Appointment.class, appointmentID));
        } 
        catch(NoSuchEntityException ex) 
        {            
        }
    }
    private static void deleteAppointmentHistory(int appointmentID) throws DatabaseException
    {
        Session session = null;
        //get the list of AppointmentHistory to delete
        List<AppointmentHistory> ah; 
        try
        {
            session = getSessionFactory().getCurrentSession();
            session.beginTransaction();
            ah = session.createCriteria(AppointmentHistory.class)
                    .add(Restrictions.like("appointmentId", appointmentID))
                    .list();
            session.getTransaction().commit();
        }
        catch(HibernateException h)
        {
            tryRollBack(session.getTransaction());
            throw new DatabaseException("deleteAppointmentHistory", h);
        }
        //delete them all
        for(AppointmentHistory a: ah)
            delete(a);
    }
    public static int createMeeting(Date date, int duration, String subject, String message, LinkedList<Integer> requiredUsers, LinkedList<Integer> optionalUsers, int userID) throws DatabaseException, UpdateFailException, NoSuchEntityException
    {
        //insert the appointment
        int appointmentID = insertAppointment(date, duration, subject, message);
        //is the timeslot a meeting or a blocked slot
            //if there is only 1 user in the appointment
            //then this is actually a blocked timeslot for that user
            //so auto approve it
        boolean isBlock = requiredUsers.size()<=1;
        //insert a new row into AppointmentHistory for each required user
        for(Integer i:requiredUsers)
            System.out.print("user:"+i.toString());
        
        
        for(Integer i: requiredUsers)
            insert(new AppointmentHistory(i, appointmentID, isBlock, true));
        //insert a new row for all optional users
        for(Integer i: optionalUsers)
            if(!requiredUsers.contains(i))
                insert(new AppointmentHistory(i, appointmentID, false, false));
        approve(appointmentID, userID);
        return appointmentID;        
    }
    private static int insertAppointment(Date time, int duration, String subject, String message) throws DatabaseException, UpdateFailException
    {
        Session session = null;
        //insert new appointment
        Appointment a = new Appointment(time, duration, subject, message);
        insert(a);
        //return the id of that appointment
        try
        {
            session = getSessionFactory().getCurrentSession();
            session.beginTransaction();
            List<Appointment> appointments = session.createCriteria(Appointment.class)
                    .add(Restrictions.like("time",time))
                    .add(Restrictions.like("duration", duration))
                    .add(Restrictions.like("subject", subject))
                    .add(Restrictions.like("message" , message))
                    .list();
            session.getTransaction().commit();
            a = appointments.get(appointments.size()-1);
        }
        catch(HibernateException h)
        {
            tryRollBack(session.getTransaction());
            throw new DatabaseException("insert", h);
        }
        catch(Exception e)
        {
            throw new UpdateFailException(a.toString());
        }
        return a.getAppointmentId();        
    }
    private static void insert(Object o) throws DatabaseException
    {
        Session session = null;
        try
        {
            session = getSessionFactory().getCurrentSession();
            session.beginTransaction();
            Serializable save = session.save(o);
            session.getTransaction().commit();
        }
        catch(HibernateException h)
        {
            tryRollBack(session.getTransaction());
            throw new DatabaseException("insert", h);
        }        
    }
    private static void update(Object o) throws DatabaseException
    {
        Session session = null;
        try
        {
            session = getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();
        }
        catch(HibernateException e)
        {
            tryRollBack(session.getTransaction());
            throw new DatabaseException("delete", e);
        }
    }
    private static void delete(Object o) throws DatabaseException
    {
        Session session = null;
        try
        {
            session = getSessionFactory().getCurrentSession();
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();
        }
        catch(HibernateException h)
        {
            tryRollBack(session.getTransaction());
            throw new DatabaseException("delete", h);
        } 
    }
}
