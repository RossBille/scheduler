/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.database.exceptions;

/**
 *
 * @author Ross Bille 3127333
 */
public class DatabaseException  extends Exception 
{

    public DatabaseException(String error, Exception e) 
    {
        super(error, e);
    }
    public DatabaseException(String error) 
    {
        super(error);
    }
    public DatabaseException() 
    {
        super();
    }
    
}
