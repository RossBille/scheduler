/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.database.exceptions;

/**
 *
 * @author Ross Bille 3127333
 */
public class UpdateFailException extends Exception 
{
    public UpdateFailException(String x) 
    {
        super(String.format("Sorry, %s, was not updated", x));
    }   
}
