/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.scheduler.database;

import com.scheduler.database.exceptions.DatabaseException;
import com.scheduler.database.exceptions.NoSuchEntityException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author rossbille
 */
public class AppointmentDetails implements java.io.Serializable
{
    private Appointment appointment;
    private List<User> requiredUsers, optionalUsers;
    private boolean approved, attending, required;
    private String date;
    private String time;

    public AppointmentDetails(Appointment appointment, int userID) throws DatabaseException, NoSuchEntityException 
    {
        this.appointment = appointment;
        requiredUsers = DatabaseController.allInvolvedUsers(appointment.getAppointmentId(), true);
        optionalUsers = DatabaseController.allInvolvedUsers(appointment.getAppointmentId(), false);
        //check if the current user has already approved this meeting
        attending = DatabaseController.hasAproved(userID, appointment.getAppointmentId());
        //check if the meeting as a whole has been approved (i.e. all required have approved)
        approved = DatabaseController.isApproved(appointment.getAppointmentId());
        required = DatabaseController.isRequired(userID, appointment.getAppointmentId());
        date = new SimpleDateFormat("dd MMMM yyyy").format(appointment.getTime());
        time = new SimpleDateFormat("HH:mm").format(appointment.getTime());
    }

    public Appointment getAppointment() {
        return appointment;
    }

    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
    }

    public List<User> getRequiredUsers() {
        return requiredUsers;
    }

    public void setRequiredUsers(List<User> requiredUsers) {
        this.requiredUsers = requiredUsers;
    }

    public List<User> getOptionalUsers() {
        return optionalUsers;
    }

    public void setOptionalUsers(List<User> optionalUsers) {
        this.optionalUsers = optionalUsers;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public boolean isAttending() {
        return attending;
    }

    public void setAttending(boolean attending) {
        this.attending = attending;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    
}   
