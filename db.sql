-- MySQL dump 10.13  Distrib 5.5.28, for osx10.6 (i386)
--
-- Host: localhost    Database: 
-- ------------------------------------------------------
-- Server version	5.5.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `AppointmentScheduler`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `appointmentscheduler` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `AppointmentScheduler`;

--
-- Table structure for table `Appointment`
--

DROP TABLE IF EXISTS `Appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Appointment` (
  `AppointmentID` int(10) NOT NULL AUTO_INCREMENT,
  `Time` datetime NOT NULL,
  `Duration` int(10) DEFAULT NULL,
  `Subject` varchar(25) DEFAULT NULL,
  `Message` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`AppointmentID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AppointmentHistory`
--

DROP TABLE IF EXISTS `AppointmentHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AppointmentHistory` (
  `UserID` int(10) NOT NULL,
  `AppointmentID` int(10) NOT NULL,
  `Approved` tinyint(1) NOT NULL DEFAULT '0',
  `Required` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UserID`,`AppointmentID`),
  KEY `AppointmentID` (`AppointmentID`),
  CONSTRAINT `appointmenthistory_ibfk_1` FOREIGN KEY (`UserID`) REFERENCES `User` (`UserID`),
  CONSTRAINT `appointmenthistory_ibfk_2` FOREIGN KEY (`AppointmentID`) REFERENCES `Appointment` (`AppointmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `UserID` int(10) NOT NULL AUTO_INCREMENT,
  `Username` varchar(20) NOT NULL,
  `Firstname` char(15) NOT NULL,
  `Lastname` char(15) NOT NULL,
  `Email` varchar(150) NOT NULL,
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `Username` (`Username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `Budget`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `budget` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `Budget`;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=714841443 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `CityEvolutions`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `cityevolutions` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `CityEvolutions`;

--
-- Table structure for table `Applications`
--

DROP TABLE IF EXISTS `Applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Applications` (
  `applicationID` int(10) NOT NULL AUTO_INCREMENT,
  `applicationName` varchar(25) NOT NULL,
  PRIMARY KEY (`applicationID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Transactions`
--

DROP TABLE IF EXISTS `Transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Transactions` (
  `transactionID` int(10) NOT NULL AUTO_INCREMENT,
  `userID` int(10) NOT NULL,
  `applicationID` int(10) NOT NULL,
  `dateAccessed` datetime NOT NULL,
  PRIMARY KEY (`transactionID`),
  KEY `userID` (`userID`),
  KEY `applicationID` (`applicationID`),
  CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`),
  CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`applicationID`) REFERENCES `Applications` (`applicationID`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserInformation`
--

DROP TABLE IF EXISTS `UserInformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserInformation` (
  `userID` int(10) NOT NULL,
  `userTypeID` int(1) NOT NULL,
  `firstname` char(15) NOT NULL,
  `lastname` char(15) NOT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `joindate` datetime NOT NULL,
  PRIMARY KEY (`userID`),
  KEY `userTypeID` (`userTypeID`),
  CONSTRAINT `userinformation_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`),
  CONSTRAINT `userinformation_ibfk_2` FOREIGN KEY (`userTypeID`) REFERENCES `UserTypes` (`UserTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserTypes`
--

DROP TABLE IF EXISTS `UserTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserTypes` (
  `UserTypeID` int(1) NOT NULL AUTO_INCREMENT,
  `userType` varchar(20) NOT NULL,
  PRIMARY KEY (`UserTypeID`),
  UNIQUE KEY `userType` (`userType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `userID` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `user_password` char(255) NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `assignment3`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `assignment3` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `assignment3`;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group` (
  `gid` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `coordinator` int(4) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`gid`),
  KEY `gid` (`gid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `uid` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(15) NOT NULL,
  `position` varchar(17) NOT NULL DEFAULT 'researchvisitors',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `flightpub`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `flightpub` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `flightpub`;

--
-- Table structure for table `Airline`
--

DROP TABLE IF EXISTS `Airline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Airline` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CountryID` int(10) unsigned NOT NULL,
  `AirlineCode` char(2) NOT NULL,
  `AirlineName` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `AirlineCountryID_FK` (`CountryID`),
  CONSTRAINT `AirlineCountryID_FK` FOREIGN KEY (`CountryID`) REFERENCES `Country` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Availability`
--

DROP TABLE IF EXISTS `Availability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Availability` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FlightID` int(10) unsigned NOT NULL,
  `TicketClassID` int(10) unsigned NOT NULL,
  `TicketTypeID` int(10) unsigned NOT NULL,
  `NumberAvailableSeatsLeg1` int(11) NOT NULL,
  `NumberAvailableSeatsLeg2` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `AvailabilityFlightID_FK` (`FlightID`),
  KEY `AvailabilityTicketClassID_FK` (`TicketClassID`),
  KEY `AvailabilityTicketTypeID_FK` (`TicketTypeID`),
  CONSTRAINT `AvailabilityFlightID_FK` FOREIGN KEY (`FlightID`) REFERENCES `Flight` (`ID`),
  CONSTRAINT `AvailabilityTicketClassID_FK` FOREIGN KEY (`TicketClassID`) REFERENCES `TicketClass` (`ID`),
  CONSTRAINT `AvailabilityTicketTypeID_FK` FOREIGN KEY (`TicketTypeID`) REFERENCES `TicketType` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=243809 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Booking`
--

DROP TABLE IF EXISTS `Booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Booking` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserID` int(10) unsigned NOT NULL,
  `AgentID` int(10) unsigned DEFAULT NULL,
  `TotalCost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `BPAYReference` char(16) NOT NULL DEFAULT '0000000000000000',
  `ReceiptNumber` char(16) NOT NULL DEFAULT '0000000000000000',
  `AgentReviewed` bit(1) NOT NULL DEFAULT b'0',
  `PaymentComplete` bit(1) NOT NULL DEFAULT b'0',
  `Created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  KEY `BookingUserID_FK` (`UserID`),
  CONSTRAINT `BookingUserID_FK` FOREIGN KEY (`UserID`) REFERENCES `User` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Country`
--

DROP TABLE IF EXISTS `Country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Country` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CountryCode2` char(2) NOT NULL,
  `CountryCode3` char(3) NOT NULL,
  `CountryName` varchar(80) NOT NULL DEFAULT '',
  `AlternateName1` varchar(80) NOT NULL DEFAULT '',
  `AlternateName2` varchar(80) NOT NULL DEFAULT '',
  `MotherCountryCode3` char(3) NOT NULL DEFAULT '',
  `MotherCountryComment` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Destination`
--

DROP TABLE IF EXISTS `Destination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Destination` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CountryID` int(10) unsigned NOT NULL,
  `DestinationCode` char(3) NOT NULL,
  `Airport` varchar(30) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `DestinationCountryID_FK` (`CountryID`),
  CONSTRAINT `DestinationCountryID_FK` FOREIGN KEY (`CountryID`) REFERENCES `Country` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Distance`
--

DROP TABLE IF EXISTS `Distance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Distance` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DestinationID1` int(10) unsigned NOT NULL,
  `DestinationID2` int(10) unsigned NOT NULL,
  `DistanceInKms` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `DistanceDestinationID2_FK` (`DestinationID2`),
  KEY `DistanceDestinationID1_FK` (`DestinationID1`),
  CONSTRAINT `DistanceDestinationID1_FK` FOREIGN KEY (`DestinationID1`) REFERENCES `Destination` (`ID`),
  CONSTRAINT `DistanceDestinationID2_FK` FOREIGN KEY (`DestinationID2`) REFERENCES `Destination` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Flight`
--

DROP TABLE IF EXISTS `Flight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Flight` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AirlineID` int(10) unsigned NOT NULL,
  `FlightNumber` varchar(6) NOT NULL,
  `DepartureID` int(10) unsigned NOT NULL,
  `StopOverID` int(10) unsigned DEFAULT NULL,
  `DestinationID` int(10) unsigned NOT NULL,
  `DepartureTime` datetime NOT NULL,
  `ArrivalTimeStopOver` datetime DEFAULT NULL,
  `DepartureTimeStopOver` datetime DEFAULT NULL,
  `ArrivalTime` datetime NOT NULL,
  `PlaneTypeID` int(10) unsigned NOT NULL,
  `DurationFirstLeg` int(11) NOT NULL,
  `DurationSecondLeg` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FlightFlightNumber_K` (`FlightNumber`),
  KEY `FlightAirlineID_FK` (`AirlineID`),
  KEY `FlightDepartureID_FK` (`DepartureID`),
  KEY `FlightStopOverID_FK` (`StopOverID`),
  KEY `FlightDestinationID_FK` (`DestinationID`),
  KEY `FlightPlaneTypeID_FK` (`PlaneTypeID`),
  CONSTRAINT `FlightAirlineID_FK` FOREIGN KEY (`AirlineID`) REFERENCES `Airline` (`ID`),
  CONSTRAINT `FlightDepartureID_FK` FOREIGN KEY (`DepartureID`) REFERENCES `Destination` (`ID`),
  CONSTRAINT `FlightDestinationID_FK` FOREIGN KEY (`DestinationID`) REFERENCES `Destination` (`ID`),
  CONSTRAINT `FlightPlaneTypeID_FK` FOREIGN KEY (`PlaneTypeID`) REFERENCES `PlaneType` (`ID`),
  CONSTRAINT `FlightStopOverID_FK` FOREIGN KEY (`StopOverID`) REFERENCES `Destination` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=30477 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PlaneType`
--

DROP TABLE IF EXISTS `PlaneType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PlaneType` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PlaneCode` varchar(20) NOT NULL,
  `Details` varchar(50) NOT NULL,
  `NumFirstClass` smallint(5) unsigned NOT NULL,
  `NumBusiness` smallint(5) unsigned NOT NULL,
  `NumPremiumEconomy` smallint(5) unsigned NOT NULL,
  `NumEconomy` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Price`
--

DROP TABLE IF EXISTS `Price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Price` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AirlineID` int(10) unsigned NOT NULL,
  `FlightNumber` varchar(6) NOT NULL,
  `TicketClassID` int(10) unsigned NOT NULL,
  `TicketTypeID` int(10) unsigned NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `Price` decimal(10,2) NOT NULL,
  `PriceLeg1` decimal(10,2) DEFAULT NULL,
  `PriceLeg2` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PriceAirlineID_FK` (`AirlineID`),
  KEY `PriceFlightNumber_K` (`FlightNumber`),
  KEY `PriceTicketTypeID_FK` (`TicketTypeID`),
  KEY `PriceTicketClassID_FK` (`TicketClassID`),
  CONSTRAINT `PriceAirlineID_FK` FOREIGN KEY (`AirlineID`) REFERENCES `Airline` (`ID`),
  CONSTRAINT `PriceTicketClassID_FK` FOREIGN KEY (`TicketClassID`) REFERENCES `TicketClass` (`ID`),
  CONSTRAINT `PriceTicketTypeID_FK` FOREIGN KEY (`TicketTypeID`) REFERENCES `TicketType` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17641 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Ticket`
--

DROP TABLE IF EXISTS `Ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ticket` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `BookingID` int(10) unsigned NOT NULL,
  `FlightID` int(10) unsigned NOT NULL,
  `Leg` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `TicketClassID` int(10) unsigned NOT NULL,
  `TicketTypeID` int(10) unsigned NOT NULL,
  `TicketNumber` varchar(20) NOT NULL,
  `TotalCost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `ExtraLuggage` tinyint(1) NOT NULL DEFAULT '0',
  `SeatNumber` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `TicketTicketNumber_K` (`TicketNumber`),
  KEY `TicketBookingID_FK` (`BookingID`),
  KEY `TicketFlightID_FK` (`FlightID`),
  KEY `TicketTicketClassID_FK` (`TicketClassID`),
  KEY `TicketTicketTypeID_FK` (`TicketTypeID`),
  CONSTRAINT `TicketBookingID_FK` FOREIGN KEY (`BookingID`) REFERENCES `Booking` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `TicketFlightID_FK` FOREIGN KEY (`FlightID`) REFERENCES `Flight` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `TicketTicketClassID_FK` FOREIGN KEY (`TicketClassID`) REFERENCES `TicketClass` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `TicketTicketTypeID_FK` FOREIGN KEY (`TicketTypeID`) REFERENCES `TicketType` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TicketClass`
--

DROP TABLE IF EXISTS `TicketClass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TicketClass` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ClassCode` char(3) NOT NULL,
  `Details` varchar(20) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TicketType`
--

DROP TABLE IF EXISTS `TicketType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TicketType` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TicketCode` char(1) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Transferrable` bit(1) NOT NULL,
  `Refundable` bit(1) NOT NULL,
  `Exchangeable` bit(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Email` varchar(128) NOT NULL DEFAULT '',
  `Password` varchar(80) NOT NULL,
  `IsAdmin` bit(1) NOT NULL DEFAULT b'0',
  `IsAgent` bit(1) NOT NULL DEFAULT b'0',
  `Name` varchar(80) NOT NULL DEFAULT '',
  `Address` text NOT NULL,
  `Phone` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UserEmail_K` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `flightpubdata`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `flightpubdata` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `flightpubdata`;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `addressId` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `address1` text NOT NULL,
  `address2` text,
  `stateProvince` varchar(100) NOT NULL,
  `postCode` varchar(10) NOT NULL,
  `country` varchar(74) NOT NULL,
  `company` tinytext,
  `city` varchar(45) NOT NULL,
  PRIMARY KEY (`addressId`),
  KEY `address_fkUser_idx` (`customerId`),
  CONSTRAINT `address_fkUser` FOREIGN KEY (`customerId`) REFERENCES `customer` (`customerId`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking` (
  `bookingId` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `lastModified` datetime DEFAULT NULL,
  `approved` tinyint(1) NOT NULL,
  `totalPrice` decimal(10,2) NOT NULL,
  PRIMARY KEY (`bookingId`),
  KEY `booking_customer_FKX_idx` (`customerId`),
  CONSTRAINT `booking_customer_FKX` FOREIGN KEY (`customerId`) REFERENCES `customer` (`customerId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `booking_flight`
--

DROP TABLE IF EXISTS `booking_flight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_flight` (
  `bookingFlightId` int(11) NOT NULL AUTO_INCREMENT,
  `flightId` int(11) NOT NULL COMMENT 'external fk to flights... oh wait...',
  `bookingId` int(11) NOT NULL,
  `classCode` char(3) NOT NULL,
  `ticketCode` char(1) NOT NULL,
  PRIMARY KEY (`bookingFlightId`),
  KEY `bookingFlight_booking_idx` (`bookingId`),
  CONSTRAINT `bookingFlight_fkBooking` FOREIGN KEY (`bookingId`) REFERENCES `booking` (`bookingId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `customerId` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `phone` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`customerId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customer_email`
--

DROP TABLE IF EXISTS `customer_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_email` (
  `customerEmailId` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `emailId` int(11) NOT NULL,
  `emailTypeId` int(11) NOT NULL,
  PRIMARY KEY (`customerEmailId`),
  KEY `customerEmail_idCustomerEmail_FK_idx` (`customerId`),
  KEY `customerEmail_idEmail_idx` (`emailId`),
  KEY `customerEmail_idEmailType_idx` (`emailTypeId`),
  CONSTRAINT `customerEmail_customerId_FK` FOREIGN KEY (`customerId`) REFERENCES `customer` (`customerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `customerEmail_emailId_FK` FOREIGN KEY (`emailId`) REFERENCES `email` (`emailId`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `customerEmail_emailTypeId_FK` FOREIGN KEY (`emailTypeId`) REFERENCES `email_type` (`emailTypeId`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `destinations`
--

DROP TABLE IF EXISTS `destinations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destinations` (
  `DestinationCode` char(3) NOT NULL,
  `Airport` varchar(30) NOT NULL,
  `CountryCode3` char(3) NOT NULL,
  `TimeZone` decimal(3,1) NOT NULL DEFAULT '0.0',
  `Latitude` decimal(13,10) NOT NULL DEFAULT '0.0000000000',
  `Longitude` varchar(45) NOT NULL DEFAULT '0',
  `DaylightSavingsZone` char(1) DEFAULT NULL,
  `Altitude` int(11) DEFAULT NULL,
  PRIMARY KEY (`DestinationCode`),
  KEY `DestinationCountryCode_FK` (`CountryCode3`),
  CONSTRAINT `DestinationCountryCode_FK` FOREIGN KEY (`CountryCode3`) REFERENCES `country` (`countryCode3`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `email`
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email` (
  `emailId` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`emailId`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `email_type`
--

DROP TABLE IF EXISTS `email_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_type` (
  `emailTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  PRIMARY KEY (`emailTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search_history`
--

DROP TABLE IF EXISTS `search_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_history` (
  `searchHistoryId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `lastAccessed` datetime NOT NULL,
  PRIMARY KEY (`searchHistoryId`),
  KEY `searchHistory_userId_FK_idx` (`userId`),
  CONSTRAINT `searchHistory_userId_FK` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search_history_parameter`
--

DROP TABLE IF EXISTS `search_history_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_history_parameter` (
  `searchHistoryParamId` int(11) NOT NULL AUTO_INCREMENT,
  `searchHistoryId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`searchHistoryParamId`),
  KEY `searchHistoryParam_searchHistory_idx` (`searchHistoryId`),
  CONSTRAINT `searchHistoryParam_fkSearchHistory` FOREIGN KEY (`searchHistoryId`) REFERENCES `search_history` (`searchHistoryId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userTypeId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `password` varchar(45) NOT NULL,
  `created` datetime NOT NULL DEFAULT '2013-09-12 00:00:00',
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `tempPassword` varchar(45) DEFAULT NULL,
  `tempPassExpiry` datetime DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `customerId_UNIQUE` (`customerId`),
  UNIQUE KEY `userName_UNIQUE` (`userName`),
  KEY `user_userType_idx` (`userTypeId`),
  KEY `user_userName_idx` (`userName`),
  CONSTRAINT `user_fkCustomer` FOREIGN KEY (`customerId`) REFERENCES `customer` (`customerId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_fkUserType` FOREIGN KEY (`userTypeId`) REFERENCES `user_type` (`userTypeId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_type`
--

DROP TABLE IF EXISTS `user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_type` (
  `userTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `displayName` varchar(255) NOT NULL,
  `description` text,
  PRIMARY KEY (`userTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `flightsearch`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `flightsearch` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `flightsearch`;

--
-- Table structure for table `airlines`
--

DROP TABLE IF EXISTS `airlines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `airlines` (
  `AirlineCode` char(2) NOT NULL,
  `AirlineName` varchar(30) NOT NULL,
  `CountryCode3` char(3) NOT NULL,
  PRIMARY KEY (`AirlineCode`),
  KEY `AirlinesCountryCode3_FK` (`CountryCode3`),
  CONSTRAINT `AirlinesCountryCode3_FK` FOREIGN KEY (`CountryCode3`) REFERENCES `country` (`countryCode3`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `availability`
--

DROP TABLE IF EXISTS `availability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `availability` (
  `AirlineCode` char(2) NOT NULL,
  `FlightNumber` varchar(6) NOT NULL,
  `DepartureTime` datetime NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `TicketCode` char(1) NOT NULL,
  `NumberAvailableSeatsLeg1` int(11) NOT NULL,
  `NumberAvailableSeatsLeg2` int(11) DEFAULT NULL,
  PRIMARY KEY (`AirlineCode`,`FlightNumber`,`DepartureTime`,`ClassCode`,`TicketCode`),
  KEY `AvailabilityClassCode_FK` (`ClassCode`),
  KEY `AvailabilityTicketCode_FK` (`TicketCode`),
  CONSTRAINT `AvailabilityAirlineCode_FK` FOREIGN KEY (`AirlineCode`) REFERENCES `airlines` (`AirlineCode`),
  CONSTRAINT `AvailabilityClassCode_FK` FOREIGN KEY (`ClassCode`) REFERENCES `ticketclass` (`ClassCode`),
  CONSTRAINT `AvailabilityTicketCode_FK` FOREIGN KEY (`TicketCode`) REFERENCES `tickettype` (`TicketCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `countryCode2` char(2) NOT NULL,
  `countryCode3` char(3) NOT NULL,
  `countryName` varchar(80) NOT NULL DEFAULT '',
  `alternateName1` varchar(80) NOT NULL DEFAULT '',
  `alternateName2` varchar(80) NOT NULL DEFAULT '',
  `motherCountryCode3` char(3) NOT NULL DEFAULT '',
  `motherCountryComment` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`countryCode3`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `destinations`
--

DROP TABLE IF EXISTS `destinations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destinations` (
  `DestinationCode` char(3) NOT NULL,
  `Airport` varchar(30) NOT NULL,
  `CountryCode3` char(3) NOT NULL,
  `TimeZone` decimal(3,1) NOT NULL DEFAULT '0.0',
  `Latitude` decimal(13,10) NOT NULL DEFAULT '0.0000000000',
  `Longitude` decimal(13,10) NOT NULL DEFAULT '0.0000000000',
  `DaylightSavingsZone` char(1) DEFAULT NULL,
  `Altitude` int(11) DEFAULT NULL,
  PRIMARY KEY (`DestinationCode`),
  KEY `DestinationCountryCode_FK` (`CountryCode3`),
  CONSTRAINT `DestinationCountryCode_FK` FOREIGN KEY (`CountryCode3`) REFERENCES `country` (`countryCode3`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `distances`
--

DROP TABLE IF EXISTS `distances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distances` (
  `DestinationCode1` char(3) NOT NULL,
  `DestinationCode2` char(3) NOT NULL,
  `DistancesInKms` int(11) NOT NULL,
  PRIMARY KEY (`DestinationCode1`,`DestinationCode2`),
  KEY `DestinationCode2_FK` (`DestinationCode2`),
  CONSTRAINT `DestinationCode1_FK` FOREIGN KEY (`DestinationCode1`) REFERENCES `destinations` (`DestinationCode`),
  CONSTRAINT `DestinationCode2_FK` FOREIGN KEY (`DestinationCode2`) REFERENCES `destinations` (`DestinationCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flights`
--

DROP TABLE IF EXISTS `flights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flights` (
  `flightId` int(11) NOT NULL AUTO_INCREMENT,
  `AirlineCode` char(2) NOT NULL,
  `FlightNumber` varchar(6) NOT NULL,
  `DepartureCode` char(3) NOT NULL,
  `StopOverCode` char(3) DEFAULT NULL,
  `DestinationCode` char(3) NOT NULL,
  `DepartureTime` datetime NOT NULL,
  `ArrivalTimeStopOver` datetime DEFAULT NULL,
  `DepartureTimeStopOver` datetime DEFAULT NULL,
  `ArrivalTime` datetime NOT NULL,
  `PlaneCode` varchar(20) NOT NULL,
  `Duration` int(11) NOT NULL,
  `DurationSecondLeg` int(11) DEFAULT NULL,
  PRIMARY KEY (`AirlineCode`,`FlightNumber`,`DepartureTime`),
  UNIQUE KEY `index6` (`AirlineCode`,`FlightNumber`,`DepartureTime`),
  UNIQUE KEY `flightId_UNIQUE` (`flightId`),
  KEY `FlightsDepartureCode_FK` (`DepartureCode`),
  KEY `FlightsStopOverCode_FK` (`StopOverCode`),
  KEY `FlightsDestinationCode_FK` (`DestinationCode`),
  KEY `FlightsPlaneCode_FK` (`PlaneCode`),
  CONSTRAINT `FlightsAirlineCode_FK` FOREIGN KEY (`AirlineCode`) REFERENCES `airlines` (`AirlineCode`),
  CONSTRAINT `FlightsDepartureCode_FK` FOREIGN KEY (`DepartureCode`) REFERENCES `destinations` (`DestinationCode`),
  CONSTRAINT `FlightsDestinationCode_FK` FOREIGN KEY (`DestinationCode`) REFERENCES `destinations` (`DestinationCode`),
  CONSTRAINT `FlightsPlaneCode_FK` FOREIGN KEY (`PlaneCode`) REFERENCES `planetype` (`PlaneCode`),
  CONSTRAINT `FlightsStopOverCode_FK` FOREIGN KEY (`StopOverCode`) REFERENCES `destinations` (`DestinationCode`)
) ENGINE=InnoDB AUTO_INCREMENT=30477 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `planetype`
--

DROP TABLE IF EXISTS `planetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planetype` (
  `PlaneCode` varchar(20) NOT NULL,
  `Details` varchar(50) NOT NULL,
  `NumFirstClass` int(11) NOT NULL,
  `NumBusiness` int(11) NOT NULL,
  `NumPremiumEconomy` int(11) NOT NULL,
  `Economy` int(11) NOT NULL,
  PRIMARY KEY (`PlaneCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `price`
--

DROP TABLE IF EXISTS `price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price` (
  `AirlineCode` char(2) NOT NULL,
  `FlightNumber` varchar(6) NOT NULL,
  `ClassCode` char(3) NOT NULL,
  `TicketCode` char(1) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime NOT NULL,
  `Price` decimal(10,2) NOT NULL,
  `PriceLeg1` decimal(10,2) DEFAULT NULL,
  `PriceLeg2` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`AirlineCode`,`FlightNumber`,`ClassCode`,`TicketCode`,`StartDate`),
  KEY `PriceClassCode_FK` (`ClassCode`),
  KEY `PriceTicketCode_FK` (`TicketCode`),
  CONSTRAINT `PriceAirlineCode_FK` FOREIGN KEY (`AirlineCode`) REFERENCES `airlines` (`AirlineCode`),
  CONSTRAINT `PriceClassCode_FK` FOREIGN KEY (`ClassCode`) REFERENCES `ticketclass` (`ClassCode`),
  CONSTRAINT `PriceTicketCode_FK` FOREIGN KEY (`TicketCode`) REFERENCES `tickettype` (`TicketCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `route` (
  `RouteId` int(11) NOT NULL AUTO_INCREMENT,
  `AirlineCode` char(2) NOT NULL,
  `FlightNumber` varchar(6) NOT NULL,
  `DepartureCode` char(3) NOT NULL,
  `StopOverCode` char(3) DEFAULT NULL,
  `DestinationCode` char(3) NOT NULL,
  PRIMARY KEY (`RouteId`),
  UNIQUE KEY `route.UniqueFlight_UN` (`AirlineCode`,`FlightNumber`,`DepartureCode`,`DestinationCode`,`StopOverCode`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ticketclass`
--

DROP TABLE IF EXISTS `ticketclass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticketclass` (
  `ClassCode` char(3) NOT NULL,
  `Details` varchar(20) NOT NULL,
  PRIMARY KEY (`ClassCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tickettype`
--

DROP TABLE IF EXISTS `tickettype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tickettype` (
  `TicketCode` char(1) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Transferrable` bit(1) NOT NULL,
  `Refundable` bit(1) NOT NULL,
  `Exchangeable` bit(1) NOT NULL,
  `FrequentFlyerPoints` bit(1) NOT NULL,
  PRIMARY KEY (`TicketCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `mysql`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `mysql` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mysql`;

--
-- Table structure for table `general_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `general_log` (
  `event_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_host` mediumtext NOT NULL,
  `thread_id` int(11) NOT NULL,
  `server_id` int(10) unsigned NOT NULL,
  `command_type` varchar(64) NOT NULL,
  `argument` mediumtext NOT NULL
) ENGINE=CSV DEFAULT CHARSET=utf8 COMMENT='General log';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slow_log`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `slow_log` (
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_host` mediumtext NOT NULL,
  `query_time` time NOT NULL,
  `lock_time` time NOT NULL,
  `rows_sent` int(11) NOT NULL,
  `rows_examined` int(11) NOT NULL,
  `db` varchar(512) NOT NULL,
  `last_insert_id` int(11) NOT NULL,
  `insert_id` int(11) NOT NULL,
  `server_id` int(10) unsigned NOT NULL,
  `sql_text` mediumtext NOT NULL
) ENGINE=CSV DEFAULT CHARSET=utf8 COMMENT='Slow log';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `columns_priv`
--

DROP TABLE IF EXISTS `columns_priv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `columns_priv` (
  `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(16) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Table_name` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Column_name` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Column_priv` set('Select','Insert','Update','References') CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`Host`,`Db`,`User`,`Table_name`,`Column_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column privileges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `db`
--

DROP TABLE IF EXISTS `db`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `db` (
  `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(16) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Select_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Insert_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Update_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Delete_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Drop_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Grant_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `References_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Index_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Alter_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_tmp_table_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Lock_tables_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Show_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Alter_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Execute_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Event_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Trigger_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Host`,`Db`,`User`),
  KEY `User` (`User`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database privileges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `db` char(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `name` char(64) NOT NULL DEFAULT '',
  `body` longblob NOT NULL,
  `definer` char(77) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `execute_at` datetime DEFAULT NULL,
  `interval_value` int(11) DEFAULT NULL,
  `interval_field` enum('YEAR','QUARTER','MONTH','DAY','HOUR','MINUTE','WEEK','SECOND','MICROSECOND','YEAR_MONTH','DAY_HOUR','DAY_MINUTE','DAY_SECOND','HOUR_MINUTE','HOUR_SECOND','MINUTE_SECOND','DAY_MICROSECOND','HOUR_MICROSECOND','MINUTE_MICROSECOND','SECOND_MICROSECOND') DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_executed` datetime DEFAULT NULL,
  `starts` datetime DEFAULT NULL,
  `ends` datetime DEFAULT NULL,
  `status` enum('ENABLED','DISABLED','SLAVESIDE_DISABLED') NOT NULL DEFAULT 'ENABLED',
  `on_completion` enum('DROP','PRESERVE') NOT NULL DEFAULT 'DROP',
  `sql_mode` set('REAL_AS_FLOAT','PIPES_AS_CONCAT','ANSI_QUOTES','IGNORE_SPACE','NOT_USED','ONLY_FULL_GROUP_BY','NO_UNSIGNED_SUBTRACTION','NO_DIR_IN_CREATE','POSTGRESQL','ORACLE','MSSQL','DB2','MAXDB','NO_KEY_OPTIONS','NO_TABLE_OPTIONS','NO_FIELD_OPTIONS','MYSQL323','MYSQL40','ANSI','NO_AUTO_VALUE_ON_ZERO','NO_BACKSLASH_ESCAPES','STRICT_TRANS_TABLES','STRICT_ALL_TABLES','NO_ZERO_IN_DATE','NO_ZERO_DATE','INVALID_DATES','ERROR_FOR_DIVISION_BY_ZERO','TRADITIONAL','NO_AUTO_CREATE_USER','HIGH_NOT_PRECEDENCE','NO_ENGINE_SUBSTITUTION','PAD_CHAR_TO_FULL_LENGTH') NOT NULL DEFAULT '',
  `comment` char(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `originator` int(10) unsigned NOT NULL,
  `time_zone` char(64) CHARACTER SET latin1 NOT NULL DEFAULT 'SYSTEM',
  `character_set_client` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `collation_connection` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `db_collation` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `body_utf8` longblob,
  PRIMARY KEY (`db`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Events';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `func`
--

DROP TABLE IF EXISTS `func`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `func` (
  `name` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ret` tinyint(1) NOT NULL DEFAULT '0',
  `dl` char(128) COLLATE utf8_bin NOT NULL DEFAULT '',
  `type` enum('function','aggregate') CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User defined functions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `help_category`
--

DROP TABLE IF EXISTS `help_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `help_category` (
  `help_category_id` smallint(5) unsigned NOT NULL,
  `name` char(64) NOT NULL,
  `parent_category_id` smallint(5) unsigned DEFAULT NULL,
  `url` char(128) NOT NULL,
  PRIMARY KEY (`help_category_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='help categories';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `help_keyword`
--

DROP TABLE IF EXISTS `help_keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `help_keyword` (
  `help_keyword_id` int(10) unsigned NOT NULL,
  `name` char(64) NOT NULL,
  PRIMARY KEY (`help_keyword_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='help keywords';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `help_relation`
--

DROP TABLE IF EXISTS `help_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `help_relation` (
  `help_topic_id` int(10) unsigned NOT NULL,
  `help_keyword_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`help_keyword_id`,`help_topic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='keyword-topic relation';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `help_topic`
--

DROP TABLE IF EXISTS `help_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `help_topic` (
  `help_topic_id` int(10) unsigned NOT NULL,
  `name` char(64) NOT NULL,
  `help_category_id` smallint(5) unsigned NOT NULL,
  `description` text NOT NULL,
  `example` text NOT NULL,
  `url` char(128) NOT NULL,
  PRIMARY KEY (`help_topic_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='help topics';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `host`
--

DROP TABLE IF EXISTS `host`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `host` (
  `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Select_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Insert_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Update_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Delete_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Drop_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Grant_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `References_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Index_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Alter_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_tmp_table_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Lock_tables_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Show_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Alter_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Execute_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Trigger_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Host`,`Db`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Host privileges;  Merged with database privileges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ndb_binlog_index`
--

DROP TABLE IF EXISTS `ndb_binlog_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ndb_binlog_index` (
  `Position` bigint(20) unsigned NOT NULL,
  `File` varchar(255) NOT NULL,
  `epoch` bigint(20) unsigned NOT NULL,
  `inserts` bigint(20) unsigned NOT NULL,
  `updates` bigint(20) unsigned NOT NULL,
  `deletes` bigint(20) unsigned NOT NULL,
  `schemaops` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`epoch`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `plugin`
--

DROP TABLE IF EXISTS `plugin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plugin` (
  `name` varchar(64) NOT NULL DEFAULT '',
  `dl` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='MySQL plugins';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `proc`
--

DROP TABLE IF EXISTS `proc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proc` (
  `db` char(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `name` char(64) NOT NULL DEFAULT '',
  `type` enum('FUNCTION','PROCEDURE') NOT NULL,
  `specific_name` char(64) NOT NULL DEFAULT '',
  `language` enum('SQL') NOT NULL DEFAULT 'SQL',
  `sql_data_access` enum('CONTAINS_SQL','NO_SQL','READS_SQL_DATA','MODIFIES_SQL_DATA') NOT NULL DEFAULT 'CONTAINS_SQL',
  `is_deterministic` enum('YES','NO') NOT NULL DEFAULT 'NO',
  `security_type` enum('INVOKER','DEFINER') NOT NULL DEFAULT 'DEFINER',
  `param_list` blob NOT NULL,
  `returns` longblob NOT NULL,
  `body` longblob NOT NULL,
  `definer` char(77) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sql_mode` set('REAL_AS_FLOAT','PIPES_AS_CONCAT','ANSI_QUOTES','IGNORE_SPACE','NOT_USED','ONLY_FULL_GROUP_BY','NO_UNSIGNED_SUBTRACTION','NO_DIR_IN_CREATE','POSTGRESQL','ORACLE','MSSQL','DB2','MAXDB','NO_KEY_OPTIONS','NO_TABLE_OPTIONS','NO_FIELD_OPTIONS','MYSQL323','MYSQL40','ANSI','NO_AUTO_VALUE_ON_ZERO','NO_BACKSLASH_ESCAPES','STRICT_TRANS_TABLES','STRICT_ALL_TABLES','NO_ZERO_IN_DATE','NO_ZERO_DATE','INVALID_DATES','ERROR_FOR_DIVISION_BY_ZERO','TRADITIONAL','NO_AUTO_CREATE_USER','HIGH_NOT_PRECEDENCE','NO_ENGINE_SUBSTITUTION','PAD_CHAR_TO_FULL_LENGTH') NOT NULL DEFAULT '',
  `comment` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `character_set_client` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `collation_connection` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `db_collation` char(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `body_utf8` longblob,
  PRIMARY KEY (`db`,`name`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Stored Procedures';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `procs_priv`
--

DROP TABLE IF EXISTS `procs_priv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procs_priv` (
  `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(16) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Routine_name` char(64) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `Routine_type` enum('FUNCTION','PROCEDURE') COLLATE utf8_bin NOT NULL,
  `Grantor` char(77) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Proc_priv` set('Execute','Alter Routine','Grant') CHARACTER SET utf8 NOT NULL DEFAULT '',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Host`,`Db`,`User`,`Routine_name`,`Routine_type`),
  KEY `Grantor` (`Grantor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Procedure privileges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `proxies_priv`
--

DROP TABLE IF EXISTS `proxies_priv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proxies_priv` (
  `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(16) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Proxied_host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Proxied_user` char(16) COLLATE utf8_bin NOT NULL DEFAULT '',
  `With_grant` tinyint(1) NOT NULL DEFAULT '0',
  `Grantor` char(77) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`Host`,`User`,`Proxied_host`,`Proxied_user`),
  KEY `Grantor` (`Grantor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User proxy privileges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `servers`
--

DROP TABLE IF EXISTS `servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servers` (
  `Server_name` char(64) NOT NULL DEFAULT '',
  `Host` char(64) NOT NULL DEFAULT '',
  `Db` char(64) NOT NULL DEFAULT '',
  `Username` char(64) NOT NULL DEFAULT '',
  `Password` char(64) NOT NULL DEFAULT '',
  `Port` int(4) NOT NULL DEFAULT '0',
  `Socket` char(64) NOT NULL DEFAULT '',
  `Wrapper` char(64) NOT NULL DEFAULT '',
  `Owner` char(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`Server_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='MySQL Foreign Servers table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tables_priv`
--

DROP TABLE IF EXISTS `tables_priv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tables_priv` (
  `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Db` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(16) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Table_name` char(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Grantor` char(77) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Table_priv` set('Select','Insert','Update','Delete','Create','Drop','Grant','References','Index','Alter','Create View','Show view','Trigger') CHARACTER SET utf8 NOT NULL DEFAULT '',
  `Column_priv` set('Select','Insert','Update','References') CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`Host`,`Db`,`User`,`Table_name`),
  KEY `Grantor` (`Grantor`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table privileges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_zone`
--

DROP TABLE IF EXISTS `time_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_zone` (
  `Time_zone_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Use_leap_seconds` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`Time_zone_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Time zones';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_zone_leap_second`
--

DROP TABLE IF EXISTS `time_zone_leap_second`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_zone_leap_second` (
  `Transition_time` bigint(20) NOT NULL,
  `Correction` int(11) NOT NULL,
  PRIMARY KEY (`Transition_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Leap seconds information for time zones';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_zone_name`
--

DROP TABLE IF EXISTS `time_zone_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_zone_name` (
  `Name` char(64) NOT NULL,
  `Time_zone_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Time zone names';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_zone_transition`
--

DROP TABLE IF EXISTS `time_zone_transition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_zone_transition` (
  `Time_zone_id` int(10) unsigned NOT NULL,
  `Transition_time` bigint(20) NOT NULL,
  `Transition_type_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`Time_zone_id`,`Transition_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Time zone transitions';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_zone_transition_type`
--

DROP TABLE IF EXISTS `time_zone_transition_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_zone_transition_type` (
  `Time_zone_id` int(10) unsigned NOT NULL,
  `Transition_type_id` int(10) unsigned NOT NULL,
  `Offset` int(11) NOT NULL DEFAULT '0',
  `Is_DST` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `Abbreviation` char(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`Time_zone_id`,`Transition_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Time zone transition types';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `Host` char(60) COLLATE utf8_bin NOT NULL DEFAULT '',
  `User` char(16) COLLATE utf8_bin NOT NULL DEFAULT '',
  `Password` char(41) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `Select_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Insert_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Update_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Delete_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Drop_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Reload_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Shutdown_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Process_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `File_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Grant_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `References_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Index_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Alter_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Show_db_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Super_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_tmp_table_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Lock_tables_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Execute_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Repl_slave_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Repl_client_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Show_view_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Alter_routine_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_user_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Event_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Trigger_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `Create_tablespace_priv` enum('N','Y') CHARACTER SET utf8 NOT NULL DEFAULT 'N',
  `ssl_type` enum('','ANY','X509','SPECIFIED') CHARACTER SET utf8 NOT NULL DEFAULT '',
  `ssl_cipher` blob NOT NULL,
  `x509_issuer` blob NOT NULL,
  `x509_subject` blob NOT NULL,
  `max_questions` int(11) unsigned NOT NULL DEFAULT '0',
  `max_updates` int(11) unsigned NOT NULL DEFAULT '0',
  `max_connections` int(11) unsigned NOT NULL DEFAULT '0',
  `max_user_connections` int(11) unsigned NOT NULL DEFAULT '0',
  `plugin` char(64) COLLATE utf8_bin DEFAULT '',
  `authentication_string` text COLLATE utf8_bin,
  PRIMARY KEY (`Host`,`User`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and global privileges';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `revit`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `revit` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `revit`;

--
-- Table structure for table `air terminal types`
--

DROP TABLE IF EXISTS `air terminal types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `air terminal types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Max Flow` double DEFAULT NULL,
  `Min Flow` double DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKAir Terminal Types1` (`OmniClass Number`),
  KEY `FKAir Terminal Types2` (`Assembly Code`),
  CONSTRAINT `FKAir Terminal Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKAir Terminal Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `air terminals`
--

DROP TABLE IF EXISTS `air terminals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `air terminals` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `System Abbreviation` varchar(255) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `System Type` int(11) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Flow` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKAir Terminals1` (`Type Id`),
  KEY `FKAir Terminals2` (`Phase Created`),
  KEY `FKAir Terminals3` (`Phase Demolished`),
  KEY `FKAir Terminals4` (`Design Option`),
  CONSTRAINT `FKAir Terminals1` FOREIGN KEY (`Type Id`) REFERENCES `air terminal types` (`Id`),
  CONSTRAINT `FKAir Terminals2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKAir Terminals3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKAir Terminals4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `area loads`
--

DROP TABLE IF EXISTS `area loads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area loads` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Nature` int(11) DEFAULT NULL,
  `All non 0 loads` varchar(255) DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Fz 3` double DEFAULT NULL,
  `Fy 3` double DEFAULT NULL,
  `Fx 3` double DEFAULT NULL,
  `Fz 2` double DEFAULT NULL,
  `Fy 2` double DEFAULT NULL,
  `Fx 2` double DEFAULT NULL,
  `Fz 1` double DEFAULT NULL,
  `Fy 1` double DEFAULT NULL,
  `Fx 1` double DEFAULT NULL,
  `Is Reaction` int(11) DEFAULT NULL,
  `Orient to` varchar(255) DEFAULT NULL,
  `Load Case` int(11) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKArea Loads1` (`Phase Created`),
  KEY `FKArea Loads2` (`Phase Demolished`),
  KEY `FKArea Loads3` (`Design Option`),
  CONSTRAINT `FKArea Loads1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKArea Loads2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKArea Loads3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `area schemes`
--

DROP TABLE IF EXISTS `area schemes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area schemes` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `areas`
--

DROP TABLE IF EXISTS `areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areas` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Area Scheme Id` int(11) DEFAULT NULL,
  `Area Type` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Perimeter` double DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Number` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKAreas1` (`Area Scheme Id`),
  KEY `FKAreas2` (`Level`),
  CONSTRAINT `FKAreas1` FOREIGN KEY (`Area Scheme Id`) REFERENCES `area schemes` (`Id`),
  CONSTRAINT `FKAreas2` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assemblies`
--

DROP TABLE IF EXISTS `assemblies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assemblies` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Naming Category` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKAssemblies1` (`Type Id`),
  KEY `FKAssemblies2` (`Phase Created`),
  KEY `FKAssemblies3` (`Phase Demolished`),
  KEY `FKAssemblies4` (`Design Option`),
  CONSTRAINT `FKAssemblies1` FOREIGN KEY (`Type Id`) REFERENCES `assembly types` (`Id`),
  CONSTRAINT `FKAssemblies2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKAssemblies3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKAssemblies4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assembly codes`
--

DROP TABLE IF EXISTS `assembly codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assembly codes` (
  `Assembly Code` varchar(255) NOT NULL DEFAULT '',
  `Assembly Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assembly types`
--

DROP TABLE IF EXISTS `assembly types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assembly types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKAssembly Types1` (`Assembly Code`),
  CONSTRAINT `FKAssembly Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `building type settings`
--

DROP TABLE IF EXISTS `building type settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `building type settings` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKBuilding Type Settings1` (`Design Option`),
  CONSTRAINT `FKBuilding Type Settings1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cable tray fitting type`
--

DROP TABLE IF EXISTS `cable tray fitting type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cable tray fitting type` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCable Tray Fitting Type1` (`Assembly Code`),
  CONSTRAINT `FKCable Tray Fitting Type1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cable tray fittings`
--

DROP TABLE IF EXISTS `cable tray fittings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cable tray fittings` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Service Type` varchar(255) DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCable Tray Fittings1` (`Type Id`),
  KEY `FKCable Tray Fittings2` (`Phase Created`),
  KEY `FKCable Tray Fittings3` (`Phase Demolished`),
  KEY `FKCable Tray Fittings4` (`Design Option`),
  CONSTRAINT `FKCable Tray Fittings1` FOREIGN KEY (`Type Id`) REFERENCES `cable tray fitting type` (`Id`),
  CONSTRAINT `FKCable Tray Fittings2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCable Tray Fittings3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCable Tray Fittings4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cable tray types`
--

DROP TABLE IF EXISTS `cable tray types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cable tray types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Bend Radius Multiplier` double DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCable Tray Types1` (`Assembly Code`),
  CONSTRAINT `FKCable Tray Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cable trays`
--

DROP TABLE IF EXISTS `cable trays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cable trays` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Service Type` varchar(255) DEFAULT NULL,
  `Bottom Elevation` double DEFAULT NULL,
  `Top Elevation` double DEFAULT NULL,
  `Width` double DEFAULT NULL,
  `Height` double DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCable Trays1` (`Type Id`),
  KEY `FKCable Trays2` (`Phase Created`),
  KEY `FKCable Trays3` (`Phase Demolished`),
  KEY `FKCable Trays4` (`Design Option`),
  CONSTRAINT `FKCable Trays1` FOREIGN KEY (`Type Id`) REFERENCES `cable tray types` (`Id`),
  CONSTRAINT `FKCable Trays2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCable Trays3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCable Trays4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `casework`
--

DROP TABLE IF EXISTS `casework`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `casework` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCasework1` (`Type Id`),
  KEY `FKCasework2` (`Phase Created`),
  KEY `FKCasework3` (`Phase Demolished`),
  KEY `FKCasework4` (`Design Option`),
  KEY `FKCasework5` (`Level`),
  CONSTRAINT `FKCasework1` FOREIGN KEY (`Type Id`) REFERENCES `casework types` (`Id`),
  CONSTRAINT `FKCasework2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCasework3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCasework4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKCasework5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `casework types`
--

DROP TABLE IF EXISTS `casework types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `casework types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Depth` double DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Width` double DEFAULT NULL,
  `Height` double DEFAULT NULL,
  `Finish` varchar(255) DEFAULT NULL,
  `Construction Type` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCasework Types1` (`OmniClass Number`),
  KEY `FKCasework Types2` (`Assembly Code`),
  CONSTRAINT `FKCasework Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKCasework Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ceiling types`
--

DROP TABLE IF EXISTS `ceiling types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ceiling types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Structural Material` int(11) DEFAULT NULL,
  `Roughness` int(11) DEFAULT NULL,
  `Absorptance` double DEFAULT NULL,
  `Thermal mass` double DEFAULT NULL,
  `Thermal Resistance (R)` double DEFAULT NULL,
  `Heat Transfer Coefficient (U)` double DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCeiling Types1` (`Structural Material`),
  KEY `FKCeiling Types2` (`Assembly Code`),
  CONSTRAINT `FKCeiling Types1` FOREIGN KEY (`Structural Material`) REFERENCES `materials` (`Id`),
  CONSTRAINT `FKCeiling Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ceilings`
--

DROP TABLE IF EXISTS `ceilings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ceilings` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Height Offset From Level` double DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Perimeter` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCeilings1` (`Type Id`),
  KEY `FKCeilings2` (`Phase Created`),
  KEY `FKCeilings3` (`Phase Demolished`),
  KEY `FKCeilings4` (`Design Option`),
  KEY `FKCeilings5` (`Level`),
  CONSTRAINT `FKCeilings1` FOREIGN KEY (`Type Id`) REFERENCES `ceiling types` (`Id`),
  CONSTRAINT `FKCeilings2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCeilings3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCeilings4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKCeilings5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `column types`
--

DROP TABLE IF EXISTS `column types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `column types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKColumn Types1` (`OmniClass Number`),
  KEY `FKColumn Types2` (`Assembly Code`),
  CONSTRAINT `FKColumn Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKColumn Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `columns`
--

DROP TABLE IF EXISTS `columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `columns` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Top Offset` double DEFAULT NULL,
  `Base Offset` double DEFAULT NULL,
  `Top Level` int(11) DEFAULT NULL,
  `Base Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKColumns1` (`Type Id`),
  KEY `FKColumns2` (`Phase Created`),
  KEY `FKColumns3` (`Phase Demolished`),
  KEY `FKColumns4` (`Design Option`),
  CONSTRAINT `FKColumns1` FOREIGN KEY (`Type Id`) REFERENCES `column types` (`Id`),
  CONSTRAINT `FKColumns2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKColumns3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKColumns4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `communication device types`
--

DROP TABLE IF EXISTS `communication device types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communication device types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCommunication Device Types1` (`OmniClass Number`),
  KEY `FKCommunication Device Types2` (`Assembly Code`),
  CONSTRAINT `FKCommunication Device Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKCommunication Device Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `communication devices`
--

DROP TABLE IF EXISTS `communication devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communication devices` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Panel` varchar(255) DEFAULT NULL,
  `Circuit Number` varchar(255) DEFAULT NULL,
  `Electrical Data` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCommunication Devices1` (`Type Id`),
  KEY `FKCommunication Devices2` (`Phase Created`),
  KEY `FKCommunication Devices3` (`Phase Demolished`),
  KEY `FKCommunication Devices4` (`Design Option`),
  KEY `FKCommunication Devices5` (`Level`),
  CONSTRAINT `FKCommunication Devices1` FOREIGN KEY (`Type Id`) REFERENCES `communication device types` (`Id`),
  CONSTRAINT `FKCommunication Devices2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCommunication Devices3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCommunication Devices4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKCommunication Devices5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conduit fitting type`
--

DROP TABLE IF EXISTS `conduit fitting type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conduit fitting type` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKConduit Fitting Type1` (`Assembly Code`),
  CONSTRAINT `FKConduit Fitting Type1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conduit fittings`
--

DROP TABLE IF EXISTS `conduit fittings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conduit fittings` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Service Type` varchar(255) DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKConduit Fittings1` (`Type Id`),
  KEY `FKConduit Fittings2` (`Phase Created`),
  KEY `FKConduit Fittings3` (`Phase Demolished`),
  KEY `FKConduit Fittings4` (`Design Option`),
  CONSTRAINT `FKConduit Fittings1` FOREIGN KEY (`Type Id`) REFERENCES `conduit fitting type` (`Id`),
  CONSTRAINT `FKConduit Fittings2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKConduit Fittings3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKConduit Fittings4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conduit types`
--

DROP TABLE IF EXISTS `conduit types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conduit types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Standard` int(11) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKConduit Types1` (`Assembly Code`),
  CONSTRAINT `FKConduit Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `conduits`
--

DROP TABLE IF EXISTS `conduits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `conduits` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Service Type` varchar(255) DEFAULT NULL,
  `Outside Diameter` double DEFAULT NULL,
  `Inside Diameter` double DEFAULT NULL,
  `Bottom Elevation` double DEFAULT NULL,
  `Top Elevation` double DEFAULT NULL,
  `Diameter(Trade Size)` double DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKConduits1` (`Type Id`),
  KEY `FKConduits2` (`Phase Created`),
  KEY `FKConduits3` (`Phase Demolished`),
  KEY `FKConduits4` (`Design Option`),
  CONSTRAINT `FKConduits1` FOREIGN KEY (`Type Id`) REFERENCES `conduit types` (`Id`),
  CONSTRAINT `FKConduits2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKConduits3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKConduits4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `construction types`
--

DROP TABLE IF EXISTS `construction types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `construction types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `constructions`
--

DROP TABLE IF EXISTS `constructions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `constructions` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKConstructions1` (`Type Id`),
  KEY `FKConstructions2` (`Phase Created`),
  KEY `FKConstructions3` (`Phase Demolished`),
  KEY `FKConstructions4` (`Design Option`),
  CONSTRAINT `FKConstructions1` FOREIGN KEY (`Type Id`) REFERENCES `construction types` (`Id`),
  CONSTRAINT `FKConstructions2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKConstructions3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKConstructions4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `curtain panel types`
--

DROP TABLE IF EXISTS `curtain panel types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curtain panel types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Visual Light Transmittance` double DEFAULT NULL,
  `Solar Heat Gain Coefficient` double DEFAULT NULL,
  `Thermal Resistance (R)` double DEFAULT NULL,
  `Heat Transfer Coefficient (U)` double DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Finish` varchar(255) DEFAULT NULL,
  `Construction Type` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCurtain Panel Types1` (`OmniClass Number`),
  KEY `FKCurtain Panel Types2` (`Assembly Code`),
  CONSTRAINT `FKCurtain Panel Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKCurtain Panel Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `curtain panels`
--

DROP TABLE IF EXISTS `curtain panels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curtain panels` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Width` double DEFAULT NULL,
  `Height` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCurtain Panels1` (`Phase Created`),
  KEY `FKCurtain Panels2` (`Phase Demolished`),
  KEY `FKCurtain Panels3` (`Design Option`),
  CONSTRAINT `FKCurtain Panels1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCurtain Panels2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCurtain Panels3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `curtain system types`
--

DROP TABLE IF EXISTS `curtain system types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curtain system types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCurtain System Types1` (`Assembly Code`),
  CONSTRAINT `FKCurtain System Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `curtain systems`
--

DROP TABLE IF EXISTS `curtain systems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curtain systems` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCurtain Systems1` (`Type Id`),
  KEY `FKCurtain Systems2` (`Phase Created`),
  KEY `FKCurtain Systems3` (`Phase Demolished`),
  KEY `FKCurtain Systems4` (`Design Option`),
  CONSTRAINT `FKCurtain Systems1` FOREIGN KEY (`Type Id`) REFERENCES `curtain system types` (`Id`),
  CONSTRAINT `FKCurtain Systems2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCurtain Systems3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCurtain Systems4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `curtain wall mullion types`
--

DROP TABLE IF EXISTS `curtain wall mullion types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curtain wall mullion types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCurtain Wall Mullion Types1` (`Assembly Code`),
  CONSTRAINT `FKCurtain Wall Mullion Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `curtain wall mullions`
--

DROP TABLE IF EXISTS `curtain wall mullions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curtain wall mullions` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKCurtain Wall Mullions1` (`Type Id`),
  KEY `FKCurtain Wall Mullions2` (`Phase Created`),
  KEY `FKCurtain Wall Mullions3` (`Phase Demolished`),
  KEY `FKCurtain Wall Mullions4` (`Design Option`),
  CONSTRAINT `FKCurtain Wall Mullions1` FOREIGN KEY (`Type Id`) REFERENCES `curtain wall mullion types` (`Id`),
  CONSTRAINT `FKCurtain Wall Mullions2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCurtain Wall Mullions3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKCurtain Wall Mullions4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `data device types`
--

DROP TABLE IF EXISTS `data device types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data device types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKData Device Types1` (`OmniClass Number`),
  KEY `FKData Device Types2` (`Assembly Code`),
  CONSTRAINT `FKData Device Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKData Device Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `data devices`
--

DROP TABLE IF EXISTS `data devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data devices` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Panel` varchar(255) DEFAULT NULL,
  `Circuit Number` varchar(255) DEFAULT NULL,
  `Electrical Data` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKData Devices1` (`Type Id`),
  KEY `FKData Devices2` (`Phase Created`),
  KEY `FKData Devices3` (`Phase Demolished`),
  KEY `FKData Devices4` (`Design Option`),
  KEY `FKData Devices5` (`Level`),
  CONSTRAINT `FKData Devices1` FOREIGN KEY (`Type Id`) REFERENCES `data device types` (`Id`),
  CONSTRAINT `FKData Devices2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKData Devices3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKData Devices4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKData Devices5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `demand factor types`
--

DROP TABLE IF EXISTS `demand factor types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demand factor types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Load Classification` varchar(255) DEFAULT NULL,
  `Connected Load` double DEFAULT NULL,
  `Estimated Demand Load` double DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDemand Factor Types1` (`Assembly Code`),
  CONSTRAINT `FKDemand Factor Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `demand factors`
--

DROP TABLE IF EXISTS `demand factors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demand factors` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDemand Factors1` (`Type Id`),
  KEY `FKDemand Factors2` (`Phase Created`),
  KEY `FKDemand Factors3` (`Phase Demolished`),
  KEY `FKDemand Factors4` (`Design Option`),
  CONSTRAINT `FKDemand Factors1` FOREIGN KEY (`Type Id`) REFERENCES `demand factor types` (`Id`),
  CONSTRAINT `FKDemand Factors2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDemand Factors3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDemand Factors4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `design option sets`
--

DROP TABLE IF EXISTS `design option sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `design option sets` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Primary Option Id` int(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDesign Option Sets1` (`Primary Option Id`),
  CONSTRAINT `FKDesign Option Sets1` FOREIGN KEY (`Primary Option Id`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `design options`
--

DROP TABLE IF EXISTS `design options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `design options` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option Set Id` int(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `distribution system`
--

DROP TABLE IF EXISTS `distribution system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution system` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Line to Line Voltage` int(11) DEFAULT NULL,
  `Line to Ground Voltage` int(11) DEFAULT NULL,
  `Phase` varchar(255) DEFAULT NULL,
  `Configuration` varchar(255) DEFAULT NULL,
  `Wires` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDistribution System1` (`Assembly Code`),
  CONSTRAINT `FKDistribution System1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `distribution systems`
--

DROP TABLE IF EXISTS `distribution systems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribution systems` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDistribution Systems1` (`Type Id`),
  KEY `FKDistribution Systems2` (`Phase Created`),
  KEY `FKDistribution Systems3` (`Phase Demolished`),
  KEY `FKDistribution Systems4` (`Design Option`),
  CONSTRAINT `FKDistribution Systems1` FOREIGN KEY (`Type Id`) REFERENCES `distribution system` (`Id`),
  CONSTRAINT `FKDistribution Systems2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDistribution Systems3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDistribution Systems4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `division profiles`
--

DROP TABLE IF EXISTS `division profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `division profiles` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDivision Profiles1` (`OmniClass Number`),
  KEY `FKDivision Profiles2` (`Assembly Code`),
  CONSTRAINT `FKDivision Profiles1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKDivision Profiles2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `door types`
--

DROP TABLE IF EXISTS `door types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `door types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Visual Light Transmittance` double DEFAULT NULL,
  `Solar Heat Gain Coefficient` double DEFAULT NULL,
  `Thermal Resistance (R)` double DEFAULT NULL,
  `Heat Transfer Coefficient (U)` double DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Rough Width` double DEFAULT NULL,
  `Rough Height` double DEFAULT NULL,
  `Thickness` double DEFAULT NULL,
  `Width` double DEFAULT NULL,
  `Height` double DEFAULT NULL,
  `Operation` varchar(255) DEFAULT NULL,
  `Construction Type` varchar(255) DEFAULT NULL,
  `Fire Rating` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDoor Types1` (`OmniClass Number`),
  KEY `FKDoor Types2` (`Assembly Code`),
  CONSTRAINT `FKDoor Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKDoor Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `doors`
--

DROP TABLE IF EXISTS `doors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doors` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Head Height` double DEFAULT NULL,
  `Sill Height` double DEFAULT NULL,
  `Frame Material` varchar(255) DEFAULT NULL,
  `Frame Type` varchar(255) DEFAULT NULL,
  `Finish` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDoors1` (`Type Id`),
  KEY `FKDoors2` (`Phase Created`),
  KEY `FKDoors3` (`Phase Demolished`),
  KEY `FKDoors4` (`Design Option`),
  KEY `FKDoors5` (`Level`),
  CONSTRAINT `FKDoors1` FOREIGN KEY (`Type Id`) REFERENCES `door types` (`Id`),
  CONSTRAINT `FKDoors2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDoors3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDoors4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKDoors5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duct accessories`
--

DROP TABLE IF EXISTS `duct accessories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duct accessories` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `System Abbreviation` varchar(255) DEFAULT NULL,
  `Free Size` varchar(255) DEFAULT NULL,
  `Overall Size` varchar(255) DEFAULT NULL,
  `Lining Thickness` double DEFAULT NULL,
  `Lining Type` varchar(255) DEFAULT NULL,
  `Insulation Thickness` double DEFAULT NULL,
  `Insulation Type` varchar(255) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `System Type` int(11) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDuct Accessories1` (`Type Id`),
  KEY `FKDuct Accessories2` (`Phase Created`),
  KEY `FKDuct Accessories3` (`Phase Demolished`),
  KEY `FKDuct Accessories4` (`Design Option`),
  CONSTRAINT `FKDuct Accessories1` FOREIGN KEY (`Type Id`) REFERENCES `duct accessory types` (`Id`),
  CONSTRAINT `FKDuct Accessories2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDuct Accessories3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDuct Accessories4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duct accessory types`
--

DROP TABLE IF EXISTS `duct accessory types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duct accessory types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDuct Accessory Types1` (`OmniClass Number`),
  KEY `FKDuct Accessory Types2` (`Assembly Code`),
  CONSTRAINT `FKDuct Accessory Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKDuct Accessory Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duct fitting types`
--

DROP TABLE IF EXISTS `duct fitting types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duct fitting types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDuct Fitting Types1` (`OmniClass Number`),
  KEY `FKDuct Fitting Types2` (`Assembly Code`),
  CONSTRAINT `FKDuct Fitting Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKDuct Fitting Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duct fittings`
--

DROP TABLE IF EXISTS `duct fittings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duct fittings` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `System Abbreviation` varchar(255) DEFAULT NULL,
  `Free Size` varchar(255) DEFAULT NULL,
  `Overall Size` varchar(255) DEFAULT NULL,
  `Lining Thickness` double DEFAULT NULL,
  `Lining Type` varchar(255) DEFAULT NULL,
  `Insulation Thickness` double DEFAULT NULL,
  `Insulation Type` varchar(255) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `System Type` int(11) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDuct Fittings1` (`Type Id`),
  KEY `FKDuct Fittings2` (`Phase Created`),
  KEY `FKDuct Fittings3` (`Phase Demolished`),
  KEY `FKDuct Fittings4` (`Design Option`),
  CONSTRAINT `FKDuct Fittings1` FOREIGN KEY (`Type Id`) REFERENCES `duct fitting types` (`Id`),
  CONSTRAINT `FKDuct Fittings2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDuct Fittings3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDuct Fittings4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duct placeholders`
--

DROP TABLE IF EXISTS `duct placeholders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duct placeholders` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `System Abbreviation` varchar(255) DEFAULT NULL,
  `Free Size` varchar(255) DEFAULT NULL,
  `Overall Size` varchar(255) DEFAULT NULL,
  `Lining Thickness` double DEFAULT NULL,
  `Lining Type` varchar(255) DEFAULT NULL,
  `Insulation Thickness` double DEFAULT NULL,
  `Insulation Type` varchar(255) DEFAULT NULL,
  `System Type` int(11) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Bottom Elevation` double DEFAULT NULL,
  `Top Elevation` double DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Size Lock` int(11) DEFAULT NULL,
  `Additional Flow` double DEFAULT NULL,
  `Hydraulic Diameter` double DEFAULT NULL,
  `Reynolds number` double DEFAULT NULL,
  `Equivalent Diameter` double DEFAULT NULL,
  `Section` int(11) DEFAULT NULL,
  `Loss Coefficient` double DEFAULT NULL,
  `Velocity Pressure` double DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Friction` double DEFAULT NULL,
  `Pressure Drop` double DEFAULT NULL,
  `Velocity` double DEFAULT NULL,
  `Diameter` double DEFAULT NULL,
  `Height` double DEFAULT NULL,
  `Width` double DEFAULT NULL,
  `Flow` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDuct Placeholders1` (`Type Id`),
  KEY `FKDuct Placeholders2` (`Phase Created`),
  KEY `FKDuct Placeholders3` (`Phase Demolished`),
  KEY `FKDuct Placeholders4` (`Design Option`),
  CONSTRAINT `FKDuct Placeholders1` FOREIGN KEY (`Type Id`) REFERENCES `duct types` (`Id`),
  CONSTRAINT `FKDuct Placeholders2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDuct Placeholders3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDuct Placeholders4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duct system types`
--

DROP TABLE IF EXISTS `duct system types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duct system types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Abbreviation` varchar(255) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duct systems`
--

DROP TABLE IF EXISTS `duct systems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duct systems` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Number of Elements` int(11) DEFAULT NULL,
  `System Equipment` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Flow` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDuct Systems1` (`Type Id`),
  KEY `FKDuct Systems2` (`Design Option`),
  CONSTRAINT `FKDuct Systems1` FOREIGN KEY (`Type Id`) REFERENCES `duct system types` (`Id`),
  CONSTRAINT `FKDuct Systems2` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duct types`
--

DROP TABLE IF EXISTS `duct types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duct types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDuct Types1` (`Assembly Code`),
  CONSTRAINT `FKDuct Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ducts`
--

DROP TABLE IF EXISTS `ducts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ducts` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `System Abbreviation` varchar(255) DEFAULT NULL,
  `Free Size` varchar(255) DEFAULT NULL,
  `Overall Size` varchar(255) DEFAULT NULL,
  `Lining Thickness` double DEFAULT NULL,
  `Lining Type` varchar(255) DEFAULT NULL,
  `Insulation Thickness` double DEFAULT NULL,
  `Insulation Type` varchar(255) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `System Type` int(11) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Bottom Elevation` double DEFAULT NULL,
  `Top Elevation` double DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Size Lock` int(11) DEFAULT NULL,
  `Additional Flow` double DEFAULT NULL,
  `Hydraulic Diameter` double DEFAULT NULL,
  `Reynolds number` double DEFAULT NULL,
  `Equivalent Diameter` double DEFAULT NULL,
  `Section` int(11) DEFAULT NULL,
  `Loss Coefficient` double DEFAULT NULL,
  `Velocity Pressure` double DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Friction` double DEFAULT NULL,
  `Pressure Drop` double DEFAULT NULL,
  `Velocity` double DEFAULT NULL,
  `Diameter` double DEFAULT NULL,
  `Height` double DEFAULT NULL,
  `Width` double DEFAULT NULL,
  `Flow` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKDucts1` (`Type Id`),
  KEY `FKDucts2` (`Phase Created`),
  KEY `FKDucts3` (`Phase Demolished`),
  KEY `FKDucts4` (`Design Option`),
  CONSTRAINT `FKDucts1` FOREIGN KEY (`Type Id`) REFERENCES `duct types` (`Id`),
  CONSTRAINT `FKDucts2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDucts3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKDucts4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electrical circuits`
--

DROP TABLE IF EXISTS `electrical circuits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electrical circuits` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  `Number of Elements` int(11) DEFAULT NULL,
  `Frame` double DEFAULT NULL,
  `Load Classification` varchar(255) DEFAULT NULL,
  `Panel` varchar(255) DEFAULT NULL,
  `Circuit Number` varchar(255) DEFAULT NULL,
  `of Runs` int(11) DEFAULT NULL,
  `of Hot Conductors` int(11) DEFAULT NULL,
  `of Neutral Conductors` int(11) DEFAULT NULL,
  `of Ground Conductors` int(11) DEFAULT NULL,
  `Load Name` varchar(255) DEFAULT NULL,
  `Apparent Load Phase C` double DEFAULT NULL,
  `Apparent Load Phase B` double DEFAULT NULL,
  `Apparent Load Phase A` double DEFAULT NULL,
  `True Load Phase C` double DEFAULT NULL,
  `True Load Phase B` double DEFAULT NULL,
  `True Load Phase A` double DEFAULT NULL,
  `True Current` double DEFAULT NULL,
  `True Current Phase A` double DEFAULT NULL,
  `True Current Phase B` double DEFAULT NULL,
  `True Current Phase C` double DEFAULT NULL,
  `Apparent Current` double DEFAULT NULL,
  `Apparent Current Phase A` double DEFAULT NULL,
  `Apparent Current Phase B` double DEFAULT NULL,
  `Apparent Current Phase C` double DEFAULT NULL,
  `Voltage Drop` double DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Rating` double DEFAULT NULL,
  `Wire Size` varchar(255) DEFAULT NULL,
  `Wire Type` int(11) DEFAULT NULL,
  `System Type` varchar(255) DEFAULT NULL,
  `True Load` double DEFAULT NULL,
  `Power Factor State` varchar(255) DEFAULT NULL,
  `Power Factor` double DEFAULT NULL,
  `Apparent Load` double DEFAULT NULL,
  `Balanced Load` int(11) DEFAULT NULL,
  `Voltage` double DEFAULT NULL,
  `Number of Poles` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKElectrical Circuits1` (`Design Option`),
  CONSTRAINT `FKElectrical Circuits1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electrical demand factor definitions`
--

DROP TABLE IF EXISTS `electrical demand factor definitions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electrical demand factor definitions` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKElectrical Demand Factor Definitions1` (`Design Option`),
  CONSTRAINT `FKElectrical Demand Factor Definitions1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electrical equipment`
--

DROP TABLE IF EXISTS `electrical equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electrical equipment` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Location` varchar(255) DEFAULT NULL,
  `Current Phase C` double DEFAULT NULL,
  `Current Phase B` double DEFAULT NULL,
  `Current Phase A` double DEFAULT NULL,
  `Total Estimated Demand Current` double DEFAULT NULL,
  `Total Connected Current` double DEFAULT NULL,
  `Total Demand Factor` double DEFAULT NULL,
  `Number of Wires` int(11) DEFAULT NULL,
  `Number of Phases` int(11) DEFAULT NULL,
  `Supply From` varchar(255) DEFAULT NULL,
  `Feed` varchar(255) DEFAULT NULL,
  `Modifications` varchar(255) DEFAULT NULL,
  `Enclosure` varchar(255) DEFAULT NULL,
  `Mains` double DEFAULT NULL,
  `Mounting` varchar(255) DEFAULT NULL,
  `Short Circuit Rating` varchar(255) DEFAULT NULL,
  `Max #1 Pole Breakers` int(11) DEFAULT NULL,
  `Panel Name` varchar(255) DEFAULT NULL,
  `Total Estimated Demand` double DEFAULT NULL,
  `Total Connected` double DEFAULT NULL,
  `Apparent Load Phase C` double DEFAULT NULL,
  `Apparent Load Phase B` double DEFAULT NULL,
  `Apparent Load Phase A` double DEFAULT NULL,
  `Electrical Data` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKElectrical Equipment1` (`Type Id`),
  KEY `FKElectrical Equipment2` (`Phase Created`),
  KEY `FKElectrical Equipment3` (`Phase Demolished`),
  KEY `FKElectrical Equipment4` (`Design Option`),
  KEY `FKElectrical Equipment5` (`Level`),
  CONSTRAINT `FKElectrical Equipment1` FOREIGN KEY (`Type Id`) REFERENCES `electrical equipment types` (`Id`),
  CONSTRAINT `FKElectrical Equipment2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKElectrical Equipment3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKElectrical Equipment4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKElectrical Equipment5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electrical equipment types`
--

DROP TABLE IF EXISTS `electrical equipment types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electrical equipment types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Neutral Rating` double DEFAULT NULL,
  `Neutral Bus` int(11) DEFAULT NULL,
  `Ground Bus` int(11) DEFAULT NULL,
  `Bussing` varchar(255) DEFAULT NULL,
  `SubFeed Lugs` int(11) DEFAULT NULL,
  `MCB Rating` double DEFAULT NULL,
  `Mains Type` varchar(255) DEFAULT NULL,
  `Voltage` varchar(255) DEFAULT NULL,
  `Wattage` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKElectrical Equipment Types1` (`OmniClass Number`),
  KEY `FKElectrical Equipment Types2` (`Assembly Code`),
  CONSTRAINT `FKElectrical Equipment Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKElectrical Equipment Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electrical fixture types`
--

DROP TABLE IF EXISTS `electrical fixture types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electrical fixture types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKElectrical Fixture Types1` (`OmniClass Number`),
  KEY `FKElectrical Fixture Types2` (`Assembly Code`),
  CONSTRAINT `FKElectrical Fixture Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKElectrical Fixture Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electrical fixtures`
--

DROP TABLE IF EXISTS `electrical fixtures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electrical fixtures` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Panel` varchar(255) DEFAULT NULL,
  `Circuit Number` varchar(255) DEFAULT NULL,
  `Electrical Data` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKElectrical Fixtures1` (`Type Id`),
  KEY `FKElectrical Fixtures2` (`Phase Created`),
  KEY `FKElectrical Fixtures3` (`Phase Demolished`),
  KEY `FKElectrical Fixtures4` (`Design Option`),
  KEY `FKElectrical Fixtures5` (`Level`),
  CONSTRAINT `FKElectrical Fixtures1` FOREIGN KEY (`Type Id`) REFERENCES `electrical fixture types` (`Id`),
  CONSTRAINT `FKElectrical Fixtures2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKElectrical Fixtures3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKElectrical Fixtures4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKElectrical Fixtures5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electrical load classification parameter element`
--

DROP TABLE IF EXISTS `electrical load classification parameter element`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electrical load classification parameter element` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKElectrical Load Classification Parameter Element1` (`Design Option`),
  CONSTRAINT `FKElectrical Load Classification Parameter Element1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `electrical load classifications`
--

DROP TABLE IF EXISTS `electrical load classifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electrical load classifications` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKElectrical Load Classifications1` (`Design Option`),
  CONSTRAINT `FKElectrical Load Classifications1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fascia types`
--

DROP TABLE IF EXISTS `fascia types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fascia types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Profile` int(11) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Material` int(11) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFascia Types1` (`Profile`),
  KEY `FKFascia Types2` (`Assembly Code`),
  KEY `FKFascia Types3` (`Material`),
  CONSTRAINT `FKFascia Types1` FOREIGN KEY (`Profile`) REFERENCES `profiles` (`Id`),
  CONSTRAINT `FKFascia Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`),
  CONSTRAINT `FKFascia Types3` FOREIGN KEY (`Material`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fascias`
--

DROP TABLE IF EXISTS `fascias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fascias` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFascias1` (`Type Id`),
  KEY `FKFascias2` (`Phase Created`),
  KEY `FKFascias3` (`Phase Demolished`),
  KEY `FKFascias4` (`Design Option`),
  CONSTRAINT `FKFascias1` FOREIGN KEY (`Type Id`) REFERENCES `fascia types` (`Id`),
  CONSTRAINT `FKFascias2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFascias3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFascias4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fire alarm device types`
--

DROP TABLE IF EXISTS `fire alarm device types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fire alarm device types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFire Alarm Device Types1` (`OmniClass Number`),
  KEY `FKFire Alarm Device Types2` (`Assembly Code`),
  CONSTRAINT `FKFire Alarm Device Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKFire Alarm Device Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fire alarm devices`
--

DROP TABLE IF EXISTS `fire alarm devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fire alarm devices` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Panel` varchar(255) DEFAULT NULL,
  `Circuit Number` varchar(255) DEFAULT NULL,
  `Electrical Data` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFire Alarm Devices1` (`Type Id`),
  KEY `FKFire Alarm Devices2` (`Phase Created`),
  KEY `FKFire Alarm Devices3` (`Phase Demolished`),
  KEY `FKFire Alarm Devices4` (`Design Option`),
  KEY `FKFire Alarm Devices5` (`Level`),
  CONSTRAINT `FKFire Alarm Devices1` FOREIGN KEY (`Type Id`) REFERENCES `fire alarm device types` (`Id`),
  CONSTRAINT `FKFire Alarm Devices2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFire Alarm Devices3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFire Alarm Devices4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKFire Alarm Devices5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flex duct types`
--

DROP TABLE IF EXISTS `flex duct types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flex duct types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFlex Duct Types1` (`Assembly Code`),
  CONSTRAINT `FKFlex Duct Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flex ducts`
--

DROP TABLE IF EXISTS `flex ducts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flex ducts` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `System Abbreviation` varchar(255) DEFAULT NULL,
  `Free Size` varchar(255) DEFAULT NULL,
  `Overall Size` varchar(255) DEFAULT NULL,
  `Lining Thickness` double DEFAULT NULL,
  `Lining Type` varchar(255) DEFAULT NULL,
  `Insulation Thickness` double DEFAULT NULL,
  `Insulation Type` varchar(255) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `System Type` int(11) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Size Lock` int(11) DEFAULT NULL,
  `Additional Flow` double DEFAULT NULL,
  `Hydraulic Diameter` double DEFAULT NULL,
  `Reynolds number` double DEFAULT NULL,
  `Equivalent Diameter` double DEFAULT NULL,
  `Section` int(11) DEFAULT NULL,
  `Loss Coefficient` double DEFAULT NULL,
  `Velocity Pressure` double DEFAULT NULL,
  `Friction` double DEFAULT NULL,
  `Pressure Drop` double DEFAULT NULL,
  `Velocity` double DEFAULT NULL,
  `Diameter` double DEFAULT NULL,
  `Height` double DEFAULT NULL,
  `Width` double DEFAULT NULL,
  `Flow` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFlex Ducts1` (`Type Id`),
  KEY `FKFlex Ducts2` (`Phase Created`),
  KEY `FKFlex Ducts3` (`Phase Demolished`),
  KEY `FKFlex Ducts4` (`Design Option`),
  CONSTRAINT `FKFlex Ducts1` FOREIGN KEY (`Type Id`) REFERENCES `flex duct types` (`Id`),
  CONSTRAINT `FKFlex Ducts2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFlex Ducts3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFlex Ducts4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flex pipe types`
--

DROP TABLE IF EXISTS `flex pipe types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flex pipe types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Roughness` double DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFlex Pipe Types1` (`Assembly Code`),
  CONSTRAINT `FKFlex Pipe Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flex pipes`
--

DROP TABLE IF EXISTS `flex pipes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flex pipes` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `System Abbreviation` varchar(255) DEFAULT NULL,
  `Overall Size` varchar(255) DEFAULT NULL,
  `Insulation Thickness` double DEFAULT NULL,
  `Insulation Type` varchar(255) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `System Type` int(11) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Fixture Units` double DEFAULT NULL,
  `Outside Diameter` double DEFAULT NULL,
  `Additional Flow` double DEFAULT NULL,
  `Diameter` double DEFAULT NULL,
  `Flow` double DEFAULT NULL,
  `Inside Diameter` double DEFAULT NULL,
  `Reynolds Number` double DEFAULT NULL,
  `Relative Roughness` double DEFAULT NULL,
  `Flow State` varchar(255) DEFAULT NULL,
  `Friction Factor` double DEFAULT NULL,
  `Velocity` double DEFAULT NULL,
  `Friction` double DEFAULT NULL,
  `Pressure Drop` double DEFAULT NULL,
  `Section` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFlex Pipes1` (`Type Id`),
  KEY `FKFlex Pipes2` (`Phase Created`),
  KEY `FKFlex Pipes3` (`Phase Demolished`),
  KEY `FKFlex Pipes4` (`Design Option`),
  CONSTRAINT `FKFlex Pipes1` FOREIGN KEY (`Type Id`) REFERENCES `flex pipe types` (`Id`),
  CONSTRAINT `FKFlex Pipes2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFlex Pipes3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFlex Pipes4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floor types`
--

DROP TABLE IF EXISTS `floor types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floor types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Structural Material` int(11) DEFAULT NULL,
  `Roughness` int(11) DEFAULT NULL,
  `Absorptance` double DEFAULT NULL,
  `Thermal mass` double DEFAULT NULL,
  `Thermal Resistance (R)` double DEFAULT NULL,
  `Heat Transfer Coefficient (U)` double DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFloor Types1` (`Structural Material`),
  KEY `FKFloor Types2` (`Assembly Code`),
  CONSTRAINT `FKFloor Types1` FOREIGN KEY (`Structural Material`) REFERENCES `materials` (`Id`),
  CONSTRAINT `FKFloor Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `floors`
--

DROP TABLE IF EXISTS `floors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `floors` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Estimated Reinforcement Volume` double DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Structural` int(11) DEFAULT NULL,
  `Perimeter` double DEFAULT NULL,
  `Height Offset From Level` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFloors1` (`Type Id`),
  KEY `FKFloors2` (`Phase Created`),
  KEY `FKFloors3` (`Phase Demolished`),
  KEY `FKFloors4` (`Design Option`),
  KEY `FKFloors5` (`Level`),
  CONSTRAINT `FKFloors1` FOREIGN KEY (`Type Id`) REFERENCES `floor types` (`Id`),
  CONSTRAINT `FKFloors2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFloors3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFloors4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKFloors5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fluid types`
--

DROP TABLE IF EXISTS `fluid types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fluid types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFluid Types1` (`Assembly Code`),
  CONSTRAINT `FKFluid Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fluids`
--

DROP TABLE IF EXISTS `fluids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fluids` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFluids1` (`Type Id`),
  KEY `FKFluids2` (`Phase Created`),
  KEY `FKFluids3` (`Phase Demolished`),
  KEY `FKFluids4` (`Design Option`),
  CONSTRAINT `FKFluids1` FOREIGN KEY (`Type Id`) REFERENCES `fluid types` (`Id`),
  CONSTRAINT `FKFluids2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFluids3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFluids4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `furniture`
--

DROP TABLE IF EXISTS `furniture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `furniture` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFurniture1` (`Type Id`),
  KEY `FKFurniture2` (`Phase Created`),
  KEY `FKFurniture3` (`Phase Demolished`),
  KEY `FKFurniture4` (`Design Option`),
  KEY `FKFurniture5` (`Level`),
  CONSTRAINT `FKFurniture1` FOREIGN KEY (`Type Id`) REFERENCES `furniture types` (`Id`),
  CONSTRAINT `FKFurniture2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFurniture3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFurniture4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKFurniture5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `furniture system types`
--

DROP TABLE IF EXISTS `furniture system types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `furniture system types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFurniture System Types1` (`OmniClass Number`),
  KEY `FKFurniture System Types2` (`Assembly Code`),
  CONSTRAINT `FKFurniture System Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKFurniture System Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `furniture systems`
--

DROP TABLE IF EXISTS `furniture systems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `furniture systems` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFurniture Systems1` (`Type Id`),
  KEY `FKFurniture Systems2` (`Phase Created`),
  KEY `FKFurniture Systems3` (`Phase Demolished`),
  KEY `FKFurniture Systems4` (`Design Option`),
  KEY `FKFurniture Systems5` (`Level`),
  CONSTRAINT `FKFurniture Systems1` FOREIGN KEY (`Type Id`) REFERENCES `furniture system types` (`Id`),
  CONSTRAINT `FKFurniture Systems2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFurniture Systems3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKFurniture Systems4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKFurniture Systems5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `furniture types`
--

DROP TABLE IF EXISTS `furniture types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `furniture types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKFurniture Types1` (`OmniClass Number`),
  KEY `FKFurniture Types2` (`Assembly Code`),
  CONSTRAINT `FKFurniture Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKFurniture Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `generic model types`
--

DROP TABLE IF EXISTS `generic model types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `generic model types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKGeneric Model Types1` (`OmniClass Number`),
  KEY `FKGeneric Model Types2` (`Assembly Code`),
  CONSTRAINT `FKGeneric Model Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKGeneric Model Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `generic models`
--

DROP TABLE IF EXISTS `generic models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `generic models` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKGeneric Models1` (`Type Id`),
  KEY `FKGeneric Models2` (`Phase Created`),
  KEY `FKGeneric Models3` (`Phase Demolished`),
  KEY `FKGeneric Models4` (`Design Option`),
  KEY `FKGeneric Models5` (`Level`),
  CONSTRAINT `FKGeneric Models1` FOREIGN KEY (`Type Id`) REFERENCES `generic model types` (`Id`),
  CONSTRAINT `FKGeneric Models2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKGeneric Models3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKGeneric Models4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKGeneric Models5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gutter types`
--

DROP TABLE IF EXISTS `gutter types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gutter types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Profile` int(11) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Material` int(11) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKGutter Types1` (`Profile`),
  KEY `FKGutter Types2` (`Assembly Code`),
  KEY `FKGutter Types3` (`Material`),
  CONSTRAINT `FKGutter Types1` FOREIGN KEY (`Profile`) REFERENCES `profiles` (`Id`),
  CONSTRAINT `FKGutter Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`),
  CONSTRAINT `FKGutter Types3` FOREIGN KEY (`Material`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gutters`
--

DROP TABLE IF EXISTS `gutters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gutters` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKGutters1` (`Type Id`),
  KEY `FKGutters2` (`Phase Created`),
  KEY `FKGutters3` (`Phase Demolished`),
  KEY `FKGutters4` (`Design Option`),
  CONSTRAINT `FKGutters1` FOREIGN KEY (`Type Id`) REFERENCES `gutter types` (`Id`),
  CONSTRAINT `FKGutters2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKGutters3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKGutters4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hvac load schedules`
--

DROP TABLE IF EXISTS `hvac load schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hvac load schedules` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKHVAC Load Schedules1` (`Design Option`),
  CONSTRAINT `FKHVAC Load Schedules1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hvac zones`
--

DROP TABLE IF EXISTS `hvac zones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hvac zones` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  `Coil Bypass` double DEFAULT NULL,
  `Calculated Area per Cooling Load` double DEFAULT NULL,
  `Calculated Area per Heating Load` double DEFAULT NULL,
  `Use Air Changes Per Hour` int(11) DEFAULT NULL,
  `Use Outside Air Per Area` int(11) DEFAULT NULL,
  `Use Outside Air Per Person` int(11) DEFAULT NULL,
  `Use Dehumidification Set Point` int(11) DEFAULT NULL,
  `Use Humidification Set Point` int(11) DEFAULT NULL,
  `Gross Area` double DEFAULT NULL,
  `Gross Volume` double DEFAULT NULL,
  `Phase Id` int(11) DEFAULT NULL,
  `Calculated Supply Airflow per area` double DEFAULT NULL,
  `Calculated Cooling Load per area` double DEFAULT NULL,
  `Calculated Heating Load per area` double DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Outdoor Air Rate  Air Changes per Hour` double DEFAULT NULL,
  `Outdoor Air per Area` double DEFAULT NULL,
  `Outdoor Air per Person` double DEFAULT NULL,
  `Dehumidification Set Point` double DEFAULT NULL,
  `Humidification Set Point` double DEFAULT NULL,
  `Cooling Air Temperature` double DEFAULT NULL,
  `Heating Air Temperature` double DEFAULT NULL,
  `Cooling Set Point` double DEFAULT NULL,
  `Heating Set Point` double DEFAULT NULL,
  `Calculated Supply Airflow` double DEFAULT NULL,
  `Calculated Cooling Load` double DEFAULT NULL,
  `Calculated Heating Load` double DEFAULT NULL,
  `Service Type` varchar(255) DEFAULT NULL,
  `Occupied Volume` double DEFAULT NULL,
  `Perimeter` double DEFAULT NULL,
  `Occupied Area` double DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKHVAC Zones1` (`Design Option`),
  CONSTRAINT `FKHVAC Zones1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `internal area loads`
--

DROP TABLE IF EXISTS `internal area loads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `internal area loads` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Nature` int(11) DEFAULT NULL,
  `All non 0 loads` varchar(255) DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Fz 3` double DEFAULT NULL,
  `Fy 3` double DEFAULT NULL,
  `Fx 3` double DEFAULT NULL,
  `Fz 2` double DEFAULT NULL,
  `Fy 2` double DEFAULT NULL,
  `Fx 2` double DEFAULT NULL,
  `Fz 1` double DEFAULT NULL,
  `Fy 1` double DEFAULT NULL,
  `Fx 1` double DEFAULT NULL,
  `Is Reaction` int(11) DEFAULT NULL,
  `Load Case` int(11) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKInternal Area Loads1` (`Phase Created`),
  KEY `FKInternal Area Loads2` (`Phase Demolished`),
  KEY `FKInternal Area Loads3` (`Design Option`),
  CONSTRAINT `FKInternal Area Loads1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKInternal Area Loads2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKInternal Area Loads3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `internal line loads`
--

DROP TABLE IF EXISTS `internal line loads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `internal line loads` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Nature` int(11) DEFAULT NULL,
  `All non 0 loads` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mz 2` double DEFAULT NULL,
  `My 2` double DEFAULT NULL,
  `Mx 2` double DEFAULT NULL,
  `Mz 1` double DEFAULT NULL,
  `My 1` double DEFAULT NULL,
  `Mx 1` double DEFAULT NULL,
  `Fz 2` double DEFAULT NULL,
  `Fy 2` double DEFAULT NULL,
  `Fx 2` double DEFAULT NULL,
  `Fz 1` double DEFAULT NULL,
  `Fy 1` double DEFAULT NULL,
  `Fx 1` double DEFAULT NULL,
  `Is Reaction` int(11) DEFAULT NULL,
  `Uniform Load` int(11) DEFAULT NULL,
  `Load Case` int(11) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKInternal Line Loads1` (`Phase Created`),
  KEY `FKInternal Line Loads2` (`Phase Demolished`),
  KEY `FKInternal Line Loads3` (`Design Option`),
  CONSTRAINT `FKInternal Line Loads1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKInternal Line Loads2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKInternal Line Loads3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `internal point loads`
--

DROP TABLE IF EXISTS `internal point loads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `internal point loads` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Nature` int(11) DEFAULT NULL,
  `All non 0 loads` varchar(255) DEFAULT NULL,
  `Mz` double DEFAULT NULL,
  `My` double DEFAULT NULL,
  `Mx` double DEFAULT NULL,
  `Fz` double DEFAULT NULL,
  `Fy` double DEFAULT NULL,
  `Fx` double DEFAULT NULL,
  `Is Reaction` int(11) DEFAULT NULL,
  `Load Case` int(11) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKInternal Point Loads1` (`Phase Created`),
  KEY `FKInternal Point Loads2` (`Phase Demolished`),
  KEY `FKInternal Point Loads3` (`Design Option`),
  CONSTRAINT `FKInternal Point Loads1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKInternal Point Loads2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKInternal Point Loads3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `level types`
--

DROP TABLE IF EXISTS `level types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `level types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `levels`
--

DROP TABLE IF EXISTS `levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `levels` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Structural` int(11) DEFAULT NULL,
  `Building Story` int(11) DEFAULT NULL,
  `Story Above` int(11) DEFAULT NULL,
  `Elevation` double DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKLevels1` (`Type Id`),
  KEY `FKLevels2` (`Design Option`),
  CONSTRAINT `FKLevels1` FOREIGN KEY (`Type Id`) REFERENCES `level types` (`Id`),
  CONSTRAINT `FKLevels2` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lighting device types`
--

DROP TABLE IF EXISTS `lighting device types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lighting device types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKLighting Device Types1` (`OmniClass Number`),
  KEY `FKLighting Device Types2` (`Assembly Code`),
  CONSTRAINT `FKLighting Device Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKLighting Device Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lighting devices`
--

DROP TABLE IF EXISTS `lighting devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lighting devices` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Switch ID` varchar(255) DEFAULT NULL,
  `Panel` varchar(255) DEFAULT NULL,
  `Circuit Number` varchar(255) DEFAULT NULL,
  `Electrical Data` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKLighting Devices1` (`Type Id`),
  KEY `FKLighting Devices2` (`Phase Created`),
  KEY `FKLighting Devices3` (`Phase Demolished`),
  KEY `FKLighting Devices4` (`Design Option`),
  KEY `FKLighting Devices5` (`Level`),
  CONSTRAINT `FKLighting Devices1` FOREIGN KEY (`Type Id`) REFERENCES `lighting device types` (`Id`),
  CONSTRAINT `FKLighting Devices2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKLighting Devices3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKLighting Devices4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKLighting Devices5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lighting fixture types`
--

DROP TABLE IF EXISTS `lighting fixture types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lighting fixture types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Light Source Symbol Length` double DEFAULT NULL,
  `Light Source Symbol Size` double DEFAULT NULL,
  `Emit from Circle Diameter` double DEFAULT NULL,
  `Emit from Rectangle Length` double DEFAULT NULL,
  `Emit from Rectangle Width` double DEFAULT NULL,
  `Emit from Line Length` double DEFAULT NULL,
  `Emit Shape Visible in Rendering` int(11) DEFAULT NULL,
  `Dimming Lamp Color Temperature Shift` varchar(255) DEFAULT NULL,
  `Luminaire Dirt Depreciation` double DEFAULT NULL,
  `Lamp Lumen Depreciation` double DEFAULT NULL,
  `Surface Depreciation Loss` double DEFAULT NULL,
  `Lamp Tilt Loss` double DEFAULT NULL,
  `Voltage Loss` double DEFAULT NULL,
  `Temperature Loss` double DEFAULT NULL,
  `Initial Color Temperature` double DEFAULT NULL,
  `Illuminance` double DEFAULT NULL,
  `Luminous Intensity` double DEFAULT NULL,
  `Efficacy` double DEFAULT NULL,
  `Wattage` double DEFAULT NULL,
  `Keynote` varchar(255) DEFAULT NULL,
  `Photometric Web File` varchar(255) DEFAULT NULL,
  `Apparent Load` double DEFAULT NULL,
  `Ballast Loss` double DEFAULT NULL,
  `Total Light Loss Factor` double DEFAULT NULL,
  `Spot Field Angle` double DEFAULT NULL,
  `Spot Beam Angle` double DEFAULT NULL,
  `Tilt Angle` double DEFAULT NULL,
  `Luminous Flux` double DEFAULT NULL,
  `Lamp` varchar(255) DEFAULT NULL,
  `Wattage Comments` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKLighting Fixture Types1` (`OmniClass Number`),
  KEY `FKLighting Fixture Types2` (`Assembly Code`),
  CONSTRAINT `FKLighting Fixture Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKLighting Fixture Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lighting fixtures`
--

DROP TABLE IF EXISTS `lighting fixtures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lighting fixtures` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Switch ID` varchar(255) DEFAULT NULL,
  `Panel` varchar(255) DEFAULT NULL,
  `Circuit Number` varchar(255) DEFAULT NULL,
  `Electrical Data` varchar(255) DEFAULT NULL,
  `Coefficient of Utilization` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKLighting Fixtures1` (`Type Id`),
  KEY `FKLighting Fixtures2` (`Phase Created`),
  KEY `FKLighting Fixtures3` (`Phase Demolished`),
  KEY `FKLighting Fixtures4` (`Design Option`),
  KEY `FKLighting Fixtures5` (`Level`),
  CONSTRAINT `FKLighting Fixtures1` FOREIGN KEY (`Type Id`) REFERENCES `lighting fixture types` (`Id`),
  CONSTRAINT `FKLighting Fixtures2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKLighting Fixtures3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKLighting Fixtures4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKLighting Fixtures5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `line loads`
--

DROP TABLE IF EXISTS `line loads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `line loads` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Nature` int(11) DEFAULT NULL,
  `All non 0 loads` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mz 2` double DEFAULT NULL,
  `My 2` double DEFAULT NULL,
  `Mx 2` double DEFAULT NULL,
  `Mz 1` double DEFAULT NULL,
  `My 1` double DEFAULT NULL,
  `Mx 1` double DEFAULT NULL,
  `Fz 2` double DEFAULT NULL,
  `Fy 2` double DEFAULT NULL,
  `Fx 2` double DEFAULT NULL,
  `Fz 1` double DEFAULT NULL,
  `Fy 1` double DEFAULT NULL,
  `Fx 1` double DEFAULT NULL,
  `Is Reaction` int(11) DEFAULT NULL,
  `Uniform Load` int(11) DEFAULT NULL,
  `Orient to` varchar(255) DEFAULT NULL,
  `Load Case` int(11) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKLine Loads1` (`Phase Created`),
  KEY `FKLine Loads2` (`Phase Demolished`),
  KEY `FKLine Loads3` (`Design Option`),
  CONSTRAINT `FKLine Loads1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKLine Loads2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKLine Loads3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mass`
--

DROP TABLE IF EXISTS `mass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mass` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Gross Volume` double DEFAULT NULL,
  `Gross Surface Area` double DEFAULT NULL,
  `Gross Floor Area` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Base Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKMass1` (`Type Id`),
  KEY `FKMass2` (`Phase Created`),
  KEY `FKMass3` (`Phase Demolished`),
  KEY `FKMass4` (`Design Option`),
  CONSTRAINT `FKMass1` FOREIGN KEY (`Type Id`) REFERENCES `mass types` (`Id`),
  CONSTRAINT `FKMass2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mass exterior wall`
--

DROP TABLE IF EXISTS `mass exterior wall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mass exterior wall` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Values` varchar(255) DEFAULT NULL,
  `Target Sill Height` double DEFAULT NULL,
  `Shade Depth` double DEFAULT NULL,
  `Glazing is Shaded` int(11) DEFAULT NULL,
  `Target Percentage Glazing` double DEFAULT NULL,
  `Underground` int(11) DEFAULT NULL,
  `Mass Exterior Wall Area` double DEFAULT NULL,
  `Subcategory` int(11) DEFAULT NULL,
  `Conceptual Constructions` int(11) DEFAULT NULL,
  `Graphical Appearance` int(11) DEFAULT NULL,
  `Mass: Comments` varchar(255) DEFAULT NULL,
  `Mass: Type Comments` varchar(255) DEFAULT NULL,
  `Mass: Family and Type` varchar(255) DEFAULT NULL,
  `Mass: Family` varchar(255) DEFAULT NULL,
  `Mass: Type` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKMass Exterior Wall1` (`Phase Created`),
  KEY `FKMass Exterior Wall2` (`Phase Demolished`),
  KEY `FKMass Exterior Wall3` (`Design Option`),
  KEY `FKMass Exterior Wall4` (`Graphical Appearance`),
  CONSTRAINT `FKMass Exterior Wall1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Exterior Wall2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Exterior Wall3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKMass Exterior Wall4` FOREIGN KEY (`Graphical Appearance`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mass floor`
--

DROP TABLE IF EXISTS `mass floor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mass floor` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Slab` int(11) DEFAULT NULL,
  `Conceptual Constructions` int(11) DEFAULT NULL,
  `Graphical Appearance` int(11) DEFAULT NULL,
  `Mass: Description` varchar(255) DEFAULT NULL,
  `Mass: Comments` varchar(255) DEFAULT NULL,
  `Mass: Type Comments` varchar(255) DEFAULT NULL,
  `Mass: Family and Type` varchar(255) DEFAULT NULL,
  `Mass: Family` varchar(255) DEFAULT NULL,
  `Usage` varchar(255) DEFAULT NULL,
  `Level` varchar(255) DEFAULT NULL,
  `Mass: Type` varchar(255) DEFAULT NULL,
  `Floor Volume` double DEFAULT NULL,
  `Exterior Surface Area` double DEFAULT NULL,
  `Floor Area` double DEFAULT NULL,
  `Floor Perimeter` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKMass Floor1` (`Phase Created`),
  KEY `FKMass Floor2` (`Phase Demolished`),
  KEY `FKMass Floor3` (`Design Option`),
  KEY `FKMass Floor4` (`Graphical Appearance`),
  CONSTRAINT `FKMass Floor1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Floor2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Floor3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKMass Floor4` FOREIGN KEY (`Graphical Appearance`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mass glazing`
--

DROP TABLE IF EXISTS `mass glazing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mass glazing` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Mass Window Area` double DEFAULT NULL,
  `Conceptual Constructions` int(11) DEFAULT NULL,
  `Graphical Appearance` int(11) DEFAULT NULL,
  `Mass: Comments` varchar(255) DEFAULT NULL,
  `Mass: Type Comments` varchar(255) DEFAULT NULL,
  `Mass: Family and Type` varchar(255) DEFAULT NULL,
  `Mass: Family` varchar(255) DEFAULT NULL,
  `Mass: Type` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKMass Glazing1` (`Phase Created`),
  KEY `FKMass Glazing2` (`Phase Demolished`),
  KEY `FKMass Glazing3` (`Design Option`),
  KEY `FKMass Glazing4` (`Graphical Appearance`),
  CONSTRAINT `FKMass Glazing1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Glazing2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Glazing3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKMass Glazing4` FOREIGN KEY (`Graphical Appearance`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mass interior wall`
--

DROP TABLE IF EXISTS `mass interior wall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mass interior wall` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Mass Interior Wall Area` double DEFAULT NULL,
  `Conceptual Constructions` int(11) DEFAULT NULL,
  `Graphical Appearance` int(11) DEFAULT NULL,
  `Mass: Comments` varchar(255) DEFAULT NULL,
  `Mass: Type Comments` varchar(255) DEFAULT NULL,
  `Mass: Family and Type` varchar(255) DEFAULT NULL,
  `Mass: Family` varchar(255) DEFAULT NULL,
  `Mass: Type` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKMass Interior Wall1` (`Phase Created`),
  KEY `FKMass Interior Wall2` (`Phase Demolished`),
  KEY `FKMass Interior Wall3` (`Design Option`),
  KEY `FKMass Interior Wall4` (`Graphical Appearance`),
  CONSTRAINT `FKMass Interior Wall1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Interior Wall2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Interior Wall3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKMass Interior Wall4` FOREIGN KEY (`Graphical Appearance`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mass opening`
--

DROP TABLE IF EXISTS `mass opening`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mass opening` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Mass Opening Area` double DEFAULT NULL,
  `Conceptual Constructions` int(11) DEFAULT NULL,
  `Graphical Appearance` int(11) DEFAULT NULL,
  `Mass: Comments` varchar(255) DEFAULT NULL,
  `Mass: Type Comments` varchar(255) DEFAULT NULL,
  `Mass: Family and Type` varchar(255) DEFAULT NULL,
  `Mass: Family` varchar(255) DEFAULT NULL,
  `Mass: Type` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKMass Opening1` (`Phase Created`),
  KEY `FKMass Opening2` (`Phase Demolished`),
  KEY `FKMass Opening3` (`Design Option`),
  KEY `FKMass Opening4` (`Graphical Appearance`),
  CONSTRAINT `FKMass Opening1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Opening2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Opening3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKMass Opening4` FOREIGN KEY (`Graphical Appearance`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mass roof`
--

DROP TABLE IF EXISTS `mass roof`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mass roof` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Values` varchar(255) DEFAULT NULL,
  `Skylight Width & Depth` double DEFAULT NULL,
  `Target Percentage Skylights` double DEFAULT NULL,
  `Underground` int(11) DEFAULT NULL,
  `Mass Roof Area` double DEFAULT NULL,
  `Subcategory` int(11) DEFAULT NULL,
  `Conceptual Constructions` int(11) DEFAULT NULL,
  `Graphical Appearance` int(11) DEFAULT NULL,
  `Mass: Comments` varchar(255) DEFAULT NULL,
  `Mass: Type Comments` varchar(255) DEFAULT NULL,
  `Mass: Family and Type` varchar(255) DEFAULT NULL,
  `Mass: Family` varchar(255) DEFAULT NULL,
  `Mass: Type` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKMass Roof1` (`Phase Created`),
  KEY `FKMass Roof2` (`Phase Demolished`),
  KEY `FKMass Roof3` (`Design Option`),
  KEY `FKMass Roof4` (`Graphical Appearance`),
  CONSTRAINT `FKMass Roof1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Roof2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Roof3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKMass Roof4` FOREIGN KEY (`Graphical Appearance`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mass skylight`
--

DROP TABLE IF EXISTS `mass skylight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mass skylight` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Mass Skylight Area` double DEFAULT NULL,
  `Conceptual Constructions` int(11) DEFAULT NULL,
  `Graphical Appearance` int(11) DEFAULT NULL,
  `Mass: Comments` varchar(255) DEFAULT NULL,
  `Mass: Type Comments` varchar(255) DEFAULT NULL,
  `Mass: Family and Type` varchar(255) DEFAULT NULL,
  `Mass: Family` varchar(255) DEFAULT NULL,
  `Mass: Type` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKMass Skylight1` (`Phase Created`),
  KEY `FKMass Skylight2` (`Phase Demolished`),
  KEY `FKMass Skylight3` (`Design Option`),
  KEY `FKMass Skylight4` (`Graphical Appearance`),
  CONSTRAINT `FKMass Skylight1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Skylight2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMass Skylight3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKMass Skylight4` FOREIGN KEY (`Graphical Appearance`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mass types`
--

DROP TABLE IF EXISTS `mass types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mass types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKMass Types1` (`OmniClass Number`),
  KEY `FKMass Types2` (`Assembly Code`),
  CONSTRAINT `FKMass Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKMass Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `material quantities`
--

DROP TABLE IF EXISTS `material quantities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material quantities` (
  `Element Id` int(11) NOT NULL DEFAULT '0',
  `Material Id` int(11) NOT NULL DEFAULT '0',
  `As Paint` int(11) NOT NULL DEFAULT '0',
  `Area` double DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  PRIMARY KEY (`Element Id`,`Material Id`,`As Paint`),
  KEY `FKMaterial Quantities1` (`Material Id`),
  CONSTRAINT `FKMaterial Quantities1` FOREIGN KEY (`Material Id`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `materials`
--

DROP TABLE IF EXISTS `materials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materials` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Recycled Content` double DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mechanical equipment`
--

DROP TABLE IF EXISTS `mechanical equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mechanical equipment` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Panel` varchar(255) DEFAULT NULL,
  `Circuit Number` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKMechanical Equipment1` (`Type Id`),
  KEY `FKMechanical Equipment2` (`Phase Created`),
  KEY `FKMechanical Equipment3` (`Phase Demolished`),
  KEY `FKMechanical Equipment4` (`Design Option`),
  KEY `FKMechanical Equipment5` (`Level`),
  CONSTRAINT `FKMechanical Equipment1` FOREIGN KEY (`Type Id`) REFERENCES `mechanical equipment types` (`Id`),
  CONSTRAINT `FKMechanical Equipment2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMechanical Equipment3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKMechanical Equipment4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKMechanical Equipment5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mechanical equipment types`
--

DROP TABLE IF EXISTS `mechanical equipment types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mechanical equipment types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKMechanical Equipment Types1` (`OmniClass Number`),
  KEY `FKMechanical Equipment Types2` (`Assembly Code`),
  CONSTRAINT `FKMechanical Equipment Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKMechanical Equipment Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nurse call device types`
--

DROP TABLE IF EXISTS `nurse call device types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nurse call device types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKNurse Call Device Types1` (`OmniClass Number`),
  KEY `FKNurse Call Device Types2` (`Assembly Code`),
  CONSTRAINT `FKNurse Call Device Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKNurse Call Device Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nurse call devices`
--

DROP TABLE IF EXISTS `nurse call devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nurse call devices` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Panel` varchar(255) DEFAULT NULL,
  `Circuit Number` varchar(255) DEFAULT NULL,
  `Electrical Data` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKNurse Call Devices1` (`Type Id`),
  KEY `FKNurse Call Devices2` (`Phase Created`),
  KEY `FKNurse Call Devices3` (`Phase Demolished`),
  KEY `FKNurse Call Devices4` (`Design Option`),
  KEY `FKNurse Call Devices5` (`Level`),
  CONSTRAINT `FKNurse Call Devices1` FOREIGN KEY (`Type Id`) REFERENCES `nurse call device types` (`Id`),
  CONSTRAINT `FKNurse Call Devices2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKNurse Call Devices3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKNurse Call Devices4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKNurse Call Devices5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `omniclass numbers`
--

DROP TABLE IF EXISTS `omniclass numbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `omniclass numbers` (
  `OmniClass Number` varchar(255) NOT NULL DEFAULT '',
  `OmniClass Title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`OmniClass Number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `panel schedule templates - branch panel`
--

DROP TABLE IF EXISTS `panel schedule templates - branch panel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel schedule templates - branch panel` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPanel Schedule Templates - Branch Panel1` (`Design Option`),
  CONSTRAINT `FKPanel Schedule Templates - Branch Panel1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `panel schedule templates - data panel`
--

DROP TABLE IF EXISTS `panel schedule templates - data panel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel schedule templates - data panel` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPanel Schedule Templates - Data Panel1` (`Design Option`),
  CONSTRAINT `FKPanel Schedule Templates - Data Panel1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `panel schedule templates - switchboard`
--

DROP TABLE IF EXISTS `panel schedule templates - switchboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panel schedule templates - switchboard` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPanel Schedule Templates - Switchboard1` (`Design Option`),
  CONSTRAINT `FKPanel Schedule Templates - Switchboard1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parking`
--

DROP TABLE IF EXISTS `parking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parking` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKParking1` (`Type Id`),
  KEY `FKParking2` (`Phase Created`),
  KEY `FKParking3` (`Phase Demolished`),
  KEY `FKParking4` (`Design Option`),
  KEY `FKParking5` (`Level`),
  CONSTRAINT `FKParking1` FOREIGN KEY (`Type Id`) REFERENCES `parking types` (`Id`),
  CONSTRAINT `FKParking2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKParking3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKParking4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKParking5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parking types`
--

DROP TABLE IF EXISTS `parking types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parking types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKParking Types1` (`OmniClass Number`),
  KEY `FKParking Types2` (`Assembly Code`),
  CONSTRAINT `FKParking Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKParking Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `phases`
--

DROP TABLE IF EXISTS `phases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phases` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Sequence Number` int(11) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipe accessories`
--

DROP TABLE IF EXISTS `pipe accessories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipe accessories` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `System Abbreviation` varchar(255) DEFAULT NULL,
  `Overall Size` varchar(255) DEFAULT NULL,
  `Insulation Thickness` double DEFAULT NULL,
  `Insulation Type` varchar(255) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `System Type` int(11) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Maximum Size` double DEFAULT NULL,
  `Minimum Size` double DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipe Accessories1` (`Type Id`),
  KEY `FKPipe Accessories2` (`Phase Created`),
  KEY `FKPipe Accessories3` (`Phase Demolished`),
  KEY `FKPipe Accessories4` (`Design Option`),
  CONSTRAINT `FKPipe Accessories1` FOREIGN KEY (`Type Id`) REFERENCES `pipe accessory types` (`Id`),
  CONSTRAINT `FKPipe Accessories2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipe Accessories3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipe Accessories4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipe accessory types`
--

DROP TABLE IF EXISTS `pipe accessory types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipe accessory types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipe Accessory Types1` (`OmniClass Number`),
  KEY `FKPipe Accessory Types2` (`Assembly Code`),
  CONSTRAINT `FKPipe Accessory Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKPipe Accessory Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipe connection types`
--

DROP TABLE IF EXISTS `pipe connection types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipe connection types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipe Connection Types1` (`Assembly Code`),
  CONSTRAINT `FKPipe Connection Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipe connections`
--

DROP TABLE IF EXISTS `pipe connections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipe connections` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipe Connections1` (`Type Id`),
  KEY `FKPipe Connections2` (`Phase Created`),
  KEY `FKPipe Connections3` (`Phase Demolished`),
  KEY `FKPipe Connections4` (`Design Option`),
  CONSTRAINT `FKPipe Connections1` FOREIGN KEY (`Type Id`) REFERENCES `pipe connection types` (`Id`),
  CONSTRAINT `FKPipe Connections2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipe Connections3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipe Connections4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipe fitting types`
--

DROP TABLE IF EXISTS `pipe fitting types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipe fitting types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipe Fitting Types1` (`OmniClass Number`),
  KEY `FKPipe Fitting Types2` (`Assembly Code`),
  CONSTRAINT `FKPipe Fitting Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKPipe Fitting Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipe fittings`
--

DROP TABLE IF EXISTS `pipe fittings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipe fittings` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `System Abbreviation` varchar(255) DEFAULT NULL,
  `Overall Size` varchar(255) DEFAULT NULL,
  `Insulation Thickness` double DEFAULT NULL,
  `Insulation Type` varchar(255) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `System Type` int(11) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Maximum Size` double DEFAULT NULL,
  `Minimum Size` double DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipe Fittings1` (`Type Id`),
  KEY `FKPipe Fittings2` (`Phase Created`),
  KEY `FKPipe Fittings3` (`Phase Demolished`),
  KEY `FKPipe Fittings4` (`Design Option`),
  CONSTRAINT `FKPipe Fittings1` FOREIGN KEY (`Type Id`) REFERENCES `pipe fitting types` (`Id`),
  CONSTRAINT `FKPipe Fittings2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipe Fittings3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipe Fittings4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipe material types`
--

DROP TABLE IF EXISTS `pipe material types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipe material types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Roughness` double DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipe Material Types1` (`Assembly Code`),
  CONSTRAINT `FKPipe Material Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipe materials`
--

DROP TABLE IF EXISTS `pipe materials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipe materials` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipe Materials1` (`Type Id`),
  KEY `FKPipe Materials2` (`Phase Created`),
  KEY `FKPipe Materials3` (`Phase Demolished`),
  KEY `FKPipe Materials4` (`Design Option`),
  CONSTRAINT `FKPipe Materials1` FOREIGN KEY (`Type Id`) REFERENCES `pipe material types` (`Id`),
  CONSTRAINT `FKPipe Materials2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipe Materials3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipe Materials4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipe placeholders`
--

DROP TABLE IF EXISTS `pipe placeholders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipe placeholders` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `System Abbreviation` varchar(255) DEFAULT NULL,
  `Overall Size` varchar(255) DEFAULT NULL,
  `Insulation Thickness` double DEFAULT NULL,
  `Insulation Type` varchar(255) DEFAULT NULL,
  `System Type` int(11) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Segment Description` varchar(255) DEFAULT NULL,
  `Connection Type` varchar(255) DEFAULT NULL,
  `Fixture Units` double DEFAULT NULL,
  `Outside Diameter` double DEFAULT NULL,
  `Invert Elevation` double DEFAULT NULL,
  `Additional Flow` double DEFAULT NULL,
  `Diameter` double DEFAULT NULL,
  `Flow` double DEFAULT NULL,
  `Inside Diameter` double DEFAULT NULL,
  `Reynolds Number` double DEFAULT NULL,
  `Relative Roughness` double DEFAULT NULL,
  `Flow State` varchar(255) DEFAULT NULL,
  `Friction Factor` double DEFAULT NULL,
  `Velocity` double DEFAULT NULL,
  `Friction` double DEFAULT NULL,
  `Pressure Drop` double DEFAULT NULL,
  `Roughness` double DEFAULT NULL,
  `Material` int(11) DEFAULT NULL,
  `ScheduleType` int(11) DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Section` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipe Placeholders1` (`Type Id`),
  KEY `FKPipe Placeholders2` (`Phase Created`),
  KEY `FKPipe Placeholders3` (`Phase Demolished`),
  KEY `FKPipe Placeholders4` (`Design Option`),
  KEY `FKPipe Placeholders5` (`Material`),
  CONSTRAINT `FKPipe Placeholders1` FOREIGN KEY (`Type Id`) REFERENCES `pipe types` (`Id`),
  CONSTRAINT `FKPipe Placeholders2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipe Placeholders3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipe Placeholders4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKPipe Placeholders5` FOREIGN KEY (`Material`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipe schedule types`
--

DROP TABLE IF EXISTS `pipe schedule types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipe schedule types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipe Schedule Types1` (`Assembly Code`),
  CONSTRAINT `FKPipe Schedule Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipe schedules`
--

DROP TABLE IF EXISTS `pipe schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipe schedules` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipe Schedules1` (`Type Id`),
  KEY `FKPipe Schedules2` (`Phase Created`),
  KEY `FKPipe Schedules3` (`Phase Demolished`),
  KEY `FKPipe Schedules4` (`Design Option`),
  CONSTRAINT `FKPipe Schedules1` FOREIGN KEY (`Type Id`) REFERENCES `pipe schedule types` (`Id`),
  CONSTRAINT `FKPipe Schedules2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipe Schedules3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipe Schedules4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipe segments`
--

DROP TABLE IF EXISTS `pipe segments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipe segments` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  `Segment Description` varchar(255) DEFAULT NULL,
  `Connection Type` varchar(255) DEFAULT NULL,
  `Roughness` double DEFAULT NULL,
  `Material` int(11) DEFAULT NULL,
  `ScheduleType` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipe Segments1` (`Design Option`),
  KEY `FKPipe Segments2` (`Material`),
  CONSTRAINT `FKPipe Segments1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKPipe Segments2` FOREIGN KEY (`Material`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipe types`
--

DROP TABLE IF EXISTS `pipe types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipe types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipe Types1` (`Assembly Code`),
  CONSTRAINT `FKPipe Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pipes`
--

DROP TABLE IF EXISTS `pipes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipes` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `System Abbreviation` varchar(255) DEFAULT NULL,
  `Overall Size` varchar(255) DEFAULT NULL,
  `Insulation Thickness` double DEFAULT NULL,
  `Insulation Type` varchar(255) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `System Type` int(11) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Segment Description` varchar(255) DEFAULT NULL,
  `Connection Type` varchar(255) DEFAULT NULL,
  `Fixture Units` double DEFAULT NULL,
  `Outside Diameter` double DEFAULT NULL,
  `Invert Elevation` double DEFAULT NULL,
  `Additional Flow` double DEFAULT NULL,
  `Diameter` double DEFAULT NULL,
  `Flow` double DEFAULT NULL,
  `Inside Diameter` double DEFAULT NULL,
  `Reynolds Number` double DEFAULT NULL,
  `Relative Roughness` double DEFAULT NULL,
  `Flow State` varchar(255) DEFAULT NULL,
  `Friction Factor` double DEFAULT NULL,
  `Velocity` double DEFAULT NULL,
  `Friction` double DEFAULT NULL,
  `Pressure Drop` double DEFAULT NULL,
  `Roughness` double DEFAULT NULL,
  `Material` int(11) DEFAULT NULL,
  `ScheduleType` int(11) DEFAULT NULL,
  `Size` varchar(255) DEFAULT NULL,
  `Section` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPipes1` (`Type Id`),
  KEY `FKPipes2` (`Phase Created`),
  KEY `FKPipes3` (`Phase Demolished`),
  KEY `FKPipes4` (`Design Option`),
  KEY `FKPipes5` (`Material`),
  CONSTRAINT `FKPipes1` FOREIGN KEY (`Type Id`) REFERENCES `pipe types` (`Id`),
  CONSTRAINT `FKPipes2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipes3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPipes4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKPipes5` FOREIGN KEY (`Material`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `piping system types`
--

DROP TABLE IF EXISTS `piping system types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piping system types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Abbreviation` varchar(255) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `Fluid Type` int(11) DEFAULT NULL,
  `Fluid Temperature` double DEFAULT NULL,
  `Fluid Viscosity` double DEFAULT NULL,
  `Fluid Density` double DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `piping systems`
--

DROP TABLE IF EXISTS `piping systems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piping systems` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Number of Elements` int(11) DEFAULT NULL,
  `System Equipment` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Fixture Units` double DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Flow` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPiping Systems1` (`Type Id`),
  KEY `FKPiping Systems2` (`Design Option`),
  CONSTRAINT `FKPiping Systems1` FOREIGN KEY (`Type Id`) REFERENCES `piping system types` (`Id`),
  CONSTRAINT `FKPiping Systems2` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `planting`
--

DROP TABLE IF EXISTS `planting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planting` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPlanting1` (`Type Id`),
  KEY `FKPlanting2` (`Phase Created`),
  KEY `FKPlanting3` (`Phase Demolished`),
  KEY `FKPlanting4` (`Design Option`),
  CONSTRAINT `FKPlanting1` FOREIGN KEY (`Type Id`) REFERENCES `planting types` (`Id`),
  CONSTRAINT `FKPlanting2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPlanting3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPlanting4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `planting types`
--

DROP TABLE IF EXISTS `planting types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planting types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPlanting Types1` (`OmniClass Number`),
  KEY `FKPlanting Types2` (`Assembly Code`),
  CONSTRAINT `FKPlanting Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKPlanting Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `plumbing fixture types`
--

DROP TABLE IF EXISTS `plumbing fixture types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plumbing fixture types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `WFU` double DEFAULT NULL,
  `HWFU` double DEFAULT NULL,
  `CWFU` double DEFAULT NULL,
  `Vent Connection` int(11) DEFAULT NULL,
  `Waste Connection` int(11) DEFAULT NULL,
  `CW Connection` int(11) DEFAULT NULL,
  `HW Connection` int(11) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPlumbing Fixture Types1` (`OmniClass Number`),
  KEY `FKPlumbing Fixture Types2` (`Assembly Code`),
  CONSTRAINT `FKPlumbing Fixture Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKPlumbing Fixture Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `plumbing fixtures`
--

DROP TABLE IF EXISTS `plumbing fixtures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plumbing fixtures` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `System Abbreviation` varchar(255) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `System Type` int(11) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Trap` varchar(255) DEFAULT NULL,
  `Drain` varchar(255) DEFAULT NULL,
  `Supply Pipe` varchar(255) DEFAULT NULL,
  `Supply Fitting` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPlumbing Fixtures1` (`Type Id`),
  KEY `FKPlumbing Fixtures2` (`Phase Created`),
  KEY `FKPlumbing Fixtures3` (`Phase Demolished`),
  KEY `FKPlumbing Fixtures4` (`Design Option`),
  KEY `FKPlumbing Fixtures5` (`Level`),
  CONSTRAINT `FKPlumbing Fixtures1` FOREIGN KEY (`Type Id`) REFERENCES `plumbing fixture types` (`Id`),
  CONSTRAINT `FKPlumbing Fixtures2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPlumbing Fixtures3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPlumbing Fixtures4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKPlumbing Fixtures5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `point loads`
--

DROP TABLE IF EXISTS `point loads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `point loads` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Nature` int(11) DEFAULT NULL,
  `All non 0 loads` varchar(255) DEFAULT NULL,
  `Mz` double DEFAULT NULL,
  `My` double DEFAULT NULL,
  `Mx` double DEFAULT NULL,
  `Fz` double DEFAULT NULL,
  `Fy` double DEFAULT NULL,
  `Fx` double DEFAULT NULL,
  `Is Reaction` int(11) DEFAULT NULL,
  `Orient to` varchar(255) DEFAULT NULL,
  `Load Case` int(11) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKPoint Loads1` (`Phase Created`),
  KEY `FKPoint Loads2` (`Phase Demolished`),
  KEY `FKPoint Loads3` (`Design Option`),
  CONSTRAINT `FKPoint Loads1` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPoint Loads2` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKPoint Loads3` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKProfiles1` (`OmniClass Number`),
  KEY `FKProfiles2` (`Assembly Code`),
  CONSTRAINT `FKProfiles1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKProfiles2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project information`
--

DROP TABLE IF EXISTS `project information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project information` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Project Issue Date` varchar(255) DEFAULT NULL,
  `Project Status` varchar(255) DEFAULT NULL,
  `Client Name` varchar(255) DEFAULT NULL,
  `Project Address` varchar(255) DEFAULT NULL,
  `Project Name` varchar(255) DEFAULT NULL,
  `Project Number` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `property line types`
--

DROP TABLE IF EXISTS `property line types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property line types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKProperty Line Types1` (`Assembly Code`),
  CONSTRAINT `FKProperty Line Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `property lines`
--

DROP TABLE IF EXISTS `property lines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property lines` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKProperty Lines1` (`Type Id`),
  KEY `FKProperty Lines2` (`Design Option`),
  CONSTRAINT `FKProperty Lines1` FOREIGN KEY (`Type Id`) REFERENCES `property line types` (`Id`),
  CONSTRAINT `FKProperty Lines2` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `railing types`
--

DROP TABLE IF EXISTS `railing types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `railing types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Railing Height` double DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKRailing Types1` (`Assembly Code`),
  CONSTRAINT `FKRailing Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `railings`
--

DROP TABLE IF EXISTS `railings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `railings` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `TreadStringer Offset` double DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Base Offset` double DEFAULT NULL,
  `Base Level` int(11) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKRailings1` (`Type Id`),
  KEY `FKRailings2` (`Phase Created`),
  KEY `FKRailings3` (`Phase Demolished`),
  KEY `FKRailings4` (`Design Option`),
  KEY `FKRailings5` (`Base Level`),
  CONSTRAINT `FKRailings1` FOREIGN KEY (`Type Id`) REFERENCES `railing types` (`Id`),
  CONSTRAINT `FKRailings2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKRailings3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKRailings4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKRailings5` FOREIGN KEY (`Base Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ramp types`
--

DROP TABLE IF EXISTS `ramp types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ramp types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKRamp Types1` (`Assembly Code`),
  CONSTRAINT `FKRamp Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ramps`
--

DROP TABLE IF EXISTS `ramps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ramps` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Multistory Top Level` int(11) DEFAULT NULL,
  `Top Offset` double DEFAULT NULL,
  `Base Offset` double DEFAULT NULL,
  `Width` double DEFAULT NULL,
  `Top Level` int(11) DEFAULT NULL,
  `Base Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKRamps1` (`Type Id`),
  KEY `FKRamps2` (`Phase Created`),
  KEY `FKRamps3` (`Phase Demolished`),
  KEY `FKRamps4` (`Design Option`),
  KEY `FKRamps5` (`Multistory Top Level`),
  KEY `FKRamps6` (`Top Level`),
  KEY `FKRamps7` (`Base Level`),
  CONSTRAINT `FKRamps1` FOREIGN KEY (`Type Id`) REFERENCES `ramp types` (`Id`),
  CONSTRAINT `FKRamps2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKRamps3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKRamps4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKRamps5` FOREIGN KEY (`Multistory Top Level`) REFERENCES `levels` (`Id`),
  CONSTRAINT `FKRamps6` FOREIGN KEY (`Top Level`) REFERENCES `levels` (`Id`),
  CONSTRAINT `FKRamps7` FOREIGN KEY (`Base Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roof types`
--

DROP TABLE IF EXISTS `roof types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roof types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Structural Material` int(11) DEFAULT NULL,
  `Roughness` int(11) DEFAULT NULL,
  `Absorptance` double DEFAULT NULL,
  `Thermal mass` double DEFAULT NULL,
  `Thermal Resistance (R)` double DEFAULT NULL,
  `Heat Transfer Coefficient (U)` double DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKRoof Types1` (`Structural Material`),
  KEY `FKRoof Types2` (`Assembly Code`),
  CONSTRAINT `FKRoof Types1` FOREIGN KEY (`Structural Material`) REFERENCES `materials` (`Id`),
  CONSTRAINT `FKRoof Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roofs`
--

DROP TABLE IF EXISTS `roofs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roofs` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Fascia Depth` double DEFAULT NULL,
  `Rafter Cut` varchar(255) DEFAULT NULL,
  `Rafter or Truss` varchar(255) DEFAULT NULL,
  `Base Level` int(11) DEFAULT NULL,
  `Cutoff Offset` double DEFAULT NULL,
  `Cutoff Level` int(11) DEFAULT NULL,
  `Base Offset From Level` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKRoofs1` (`Type Id`),
  KEY `FKRoofs2` (`Phase Created`),
  KEY `FKRoofs3` (`Phase Demolished`),
  KEY `FKRoofs4` (`Design Option`),
  KEY `FKRoofs5` (`Base Level`),
  KEY `FKRoofs6` (`Cutoff Level`),
  CONSTRAINT `FKRoofs1` FOREIGN KEY (`Type Id`) REFERENCES `roof types` (`Id`),
  CONSTRAINT `FKRoofs2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKRoofs3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKRoofs4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKRoofs5` FOREIGN KEY (`Base Level`) REFERENCES `levels` (`Id`),
  CONSTRAINT `FKRoofs6` FOREIGN KEY (`Cutoff Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `room associations`
--

DROP TABLE IF EXISTS `room associations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room associations` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Id` int(11) NOT NULL DEFAULT '0',
  `Design Option Id` int(11) NOT NULL DEFAULT '0',
  `Room Id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`,`Phase Id`,`Design Option Id`),
  KEY `FKRoom Associations1` (`Phase Id`),
  KEY `FKRoom Associations2` (`Room Id`),
  CONSTRAINT `FKRoom Associations1` FOREIGN KEY (`Phase Id`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKRoom Associations2` FOREIGN KEY (`Room Id`) REFERENCES `rooms` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `room from to associations`
--

DROP TABLE IF EXISTS `room from to associations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room from to associations` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Phase Id` int(11) NOT NULL DEFAULT '0',
  `Design Option Id` int(11) NOT NULL DEFAULT '0',
  `From Room` int(11) DEFAULT NULL,
  `To Room` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`,`Phase Id`,`Design Option Id`),
  KEY `FKRoom From To Associations1` (`Phase Id`),
  KEY `FKRoom From To Associations2` (`From Room`),
  KEY `FKRoom From To Associations3` (`To Room`),
  CONSTRAINT `FKRoom From To Associations1` FOREIGN KEY (`Phase Id`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKRoom From To Associations2` FOREIGN KEY (`From Room`) REFERENCES `rooms` (`Id`),
  CONSTRAINT `FKRoom From To Associations3` FOREIGN KEY (`To Room`) REFERENCES `rooms` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  `Phase Id` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Perimeter` double DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Occupancy` varchar(255) DEFAULT NULL,
  `Department` varchar(255) DEFAULT NULL,
  `Base Finish` varchar(255) DEFAULT NULL,
  `Ceiling Finish` varchar(255) DEFAULT NULL,
  `Wall Finish` varchar(255) DEFAULT NULL,
  `Floor Finish` varchar(255) DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Number` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Occupant` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKRooms1` (`Design Option`),
  KEY `FKRooms2` (`Phase Id`),
  KEY `FKRooms3` (`Level`),
  CONSTRAINT `FKRooms1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKRooms2` FOREIGN KEY (`Phase Id`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKRooms3` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `routing preferences`
--

DROP TABLE IF EXISTS `routing preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routing preferences` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKRouting Preferences1` (`Design Option`),
  CONSTRAINT `FKRouting Preferences1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `security device types`
--

DROP TABLE IF EXISTS `security device types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `security device types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKSecurity Device Types1` (`OmniClass Number`),
  KEY `FKSecurity Device Types2` (`Assembly Code`),
  CONSTRAINT `FKSecurity Device Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKSecurity Device Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `security devices`
--

DROP TABLE IF EXISTS `security devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `security devices` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Panel` varchar(255) DEFAULT NULL,
  `Circuit Number` varchar(255) DEFAULT NULL,
  `Electrical Data` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKSecurity Devices1` (`Type Id`),
  KEY `FKSecurity Devices2` (`Phase Created`),
  KEY `FKSecurity Devices3` (`Phase Demolished`),
  KEY `FKSecurity Devices4` (`Design Option`),
  KEY `FKSecurity Devices5` (`Level`),
  CONSTRAINT `FKSecurity Devices1` FOREIGN KEY (`Type Id`) REFERENCES `security device types` (`Id`),
  CONSTRAINT `FKSecurity Devices2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKSecurity Devices3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKSecurity Devices4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKSecurity Devices5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `site`
--

DROP TABLE IF EXISTS `site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKSite1` (`Type Id`),
  KEY `FKSite2` (`Phase Created`),
  KEY `FKSite3` (`Phase Demolished`),
  KEY `FKSite4` (`Design Option`),
  CONSTRAINT `FKSite1` FOREIGN KEY (`Type Id`) REFERENCES `site types` (`Id`),
  CONSTRAINT `FKSite2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKSite3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKSite4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `site types`
--

DROP TABLE IF EXISTS `site types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKSite Types1` (`OmniClass Number`),
  KEY `FKSite Types2` (`Assembly Code`),
  CONSTRAINT `FKSite Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKSite Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slab edge types`
--

DROP TABLE IF EXISTS `slab edge types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slab edge types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Profile` int(11) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Material` int(11) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKSlab Edge Types1` (`Profile`),
  KEY `FKSlab Edge Types2` (`Assembly Code`),
  KEY `FKSlab Edge Types3` (`Material`),
  CONSTRAINT `FKSlab Edge Types1` FOREIGN KEY (`Profile`) REFERENCES `profiles` (`Id`),
  CONSTRAINT `FKSlab Edge Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`),
  CONSTRAINT `FKSlab Edge Types3` FOREIGN KEY (`Material`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `slab edges`
--

DROP TABLE IF EXISTS `slab edges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slab edges` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKSlab Edges1` (`Type Id`),
  KEY `FKSlab Edges2` (`Phase Created`),
  KEY `FKSlab Edges3` (`Phase Demolished`),
  KEY `FKSlab Edges4` (`Design Option`),
  CONSTRAINT `FKSlab Edges1` FOREIGN KEY (`Type Id`) REFERENCES `slab edge types` (`Id`),
  CONSTRAINT `FKSlab Edges2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKSlab Edges3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKSlab Edges4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `space type settings`
--

DROP TABLE IF EXISTS `space type settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `space type settings` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKSpace Type Settings1` (`Design Option`),
  CONSTRAINT `FKSpace Type Settings1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spaces`
--

DROP TABLE IF EXISTS `spaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spaces` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  `Lighting Calculation Luminaire Plane` double DEFAULT NULL,
  `Room Cavity Ratio` double DEFAULT NULL,
  `Average Estimated Illumination` double DEFAULT NULL,
  `Floor Reflectance` double DEFAULT NULL,
  `Wall Reflectance` double DEFAULT NULL,
  `Ceiling Reflectance` double DEFAULT NULL,
  `Lighting Calculation Workplane` double DEFAULT NULL,
  `Plenum` int(11) DEFAULT NULL,
  `Occupiable` int(11) DEFAULT NULL,
  `Room Number` varchar(255) DEFAULT NULL,
  `Room Name` varchar(255) DEFAULT NULL,
  `Calculated Supply Airflow per area` double DEFAULT NULL,
  `Calculated Cooling Load per area` double DEFAULT NULL,
  `Calculated Heating Load per area` double DEFAULT NULL,
  `Actual Lighting Load per area` double DEFAULT NULL,
  `Actual Power Load per area` double DEFAULT NULL,
  `Heat Load Values` varchar(255) DEFAULT NULL,
  `Design Cooling Load` double DEFAULT NULL,
  `Calculated Cooling Load` double DEFAULT NULL,
  `Design Heating Load` double DEFAULT NULL,
  `Calculated Heating Load` double DEFAULT NULL,
  `Construction Type` int(11) DEFAULT NULL,
  `Sensible Heat Gain per person` double DEFAULT NULL,
  `Specified Lighting Load` double DEFAULT NULL,
  `Specified Power Load` double DEFAULT NULL,
  `Actual Lighting Load` double DEFAULT NULL,
  `Actual Power Load` double DEFAULT NULL,
  `Design Other Load per area` double DEFAULT NULL,
  `Design HVAC Load per area` double DEFAULT NULL,
  `Specified Lighting Load per area` double DEFAULT NULL,
  `Specified Power Load per area` double DEFAULT NULL,
  `Actual Exhaust Airflow` double DEFAULT NULL,
  `Actual Return Airflow` double DEFAULT NULL,
  `Actual Supply Airflow` double DEFAULT NULL,
  `Latent Heat Gain per person` double DEFAULT NULL,
  `Total Heat Gain per person` double DEFAULT NULL,
  `Calculated Supply Airflow` double DEFAULT NULL,
  `Specified Exhaust Airflow` double DEFAULT NULL,
  `Specified Return Airflow` double DEFAULT NULL,
  `Specified Supply Airflow` double DEFAULT NULL,
  `Area per Person` double DEFAULT NULL,
  `Number of People` double DEFAULT NULL,
  `Space Type` int(11) DEFAULT NULL,
  `Condition Type` varchar(255) DEFAULT NULL,
  `Phase Id` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Perimeter` double DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Number` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKSpaces1` (`Design Option`),
  KEY `FKSpaces2` (`Phase Id`),
  KEY `FKSpaces3` (`Level`),
  CONSTRAINT `FKSpaces1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKSpaces2` FOREIGN KEY (`Phase Id`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKSpaces3` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `specialty equipment`
--

DROP TABLE IF EXISTS `specialty equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specialty equipment` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Panel` varchar(255) DEFAULT NULL,
  `Circuit Number` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKSpecialty Equipment1` (`Type Id`),
  KEY `FKSpecialty Equipment2` (`Phase Created`),
  KEY `FKSpecialty Equipment3` (`Phase Demolished`),
  KEY `FKSpecialty Equipment4` (`Design Option`),
  KEY `FKSpecialty Equipment5` (`Level`),
  CONSTRAINT `FKSpecialty Equipment1` FOREIGN KEY (`Type Id`) REFERENCES `specialty equipment types` (`Id`),
  CONSTRAINT `FKSpecialty Equipment2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKSpecialty Equipment3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKSpecialty Equipment4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKSpecialty Equipment5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `specialty equipment types`
--

DROP TABLE IF EXISTS `specialty equipment types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specialty equipment types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKSpecialty Equipment Types1` (`OmniClass Number`),
  KEY `FKSpecialty Equipment Types2` (`Assembly Code`),
  CONSTRAINT `FKSpecialty Equipment Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKSpecialty Equipment Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sprinkler types`
--

DROP TABLE IF EXISTS `sprinkler types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sprinkler types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Orifice Size` double DEFAULT NULL,
  `Temperature Rating` double DEFAULT NULL,
  `K-Factor` double DEFAULT NULL,
  `Pressure Class` varchar(255) DEFAULT NULL,
  `Orifice` varchar(255) DEFAULT NULL,
  `Coverage` varchar(255) DEFAULT NULL,
  `Response` varchar(255) DEFAULT NULL,
  `Diameter` double DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKSprinkler Types1` (`OmniClass Number`),
  KEY `FKSprinkler Types2` (`Assembly Code`),
  CONSTRAINT `FKSprinkler Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKSprinkler Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sprinklers`
--

DROP TABLE IF EXISTS `sprinklers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sprinklers` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `System Abbreviation` varchar(255) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `System Type` int(11) DEFAULT NULL,
  `System Classification` varchar(255) DEFAULT NULL,
  `System Name` varchar(255) DEFAULT NULL,
  `Flow` double DEFAULT NULL,
  `Pressure Drop` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKSprinklers1` (`Type Id`),
  KEY `FKSprinklers2` (`Phase Created`),
  KEY `FKSprinklers3` (`Phase Demolished`),
  KEY `FKSprinklers4` (`Design Option`),
  KEY `FKSprinklers5` (`Level`),
  CONSTRAINT `FKSprinklers1` FOREIGN KEY (`Type Id`) REFERENCES `sprinkler types` (`Id`),
  CONSTRAINT `FKSprinklers2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKSprinklers3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKSprinklers4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKSprinklers5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stair types`
--

DROP TABLE IF EXISTS `stair types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stair types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Tread Thickness` double DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStair Types1` (`Assembly Code`),
  CONSTRAINT `FKStair Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stairs`
--

DROP TABLE IF EXISTS `stairs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stairs` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Actual Number of Risers` int(11) DEFAULT NULL,
  `Multistory Top Level` int(11) DEFAULT NULL,
  `Top Offset` double DEFAULT NULL,
  `Base Offset` double DEFAULT NULL,
  `Actual Riser Height` double DEFAULT NULL,
  `Width` double DEFAULT NULL,
  `Top Level` int(11) DEFAULT NULL,
  `Base Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStairs1` (`Type Id`),
  KEY `FKStairs2` (`Phase Created`),
  KEY `FKStairs3` (`Phase Demolished`),
  KEY `FKStairs4` (`Design Option`),
  KEY `FKStairs5` (`Multistory Top Level`),
  KEY `FKStairs6` (`Top Level`),
  KEY `FKStairs7` (`Base Level`),
  CONSTRAINT `FKStairs1` FOREIGN KEY (`Type Id`) REFERENCES `stair types` (`Id`),
  CONSTRAINT `FKStairs2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStairs3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStairs4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKStairs5` FOREIGN KEY (`Multistory Top Level`) REFERENCES `levels` (`Id`),
  CONSTRAINT `FKStairs6` FOREIGN KEY (`Top Level`) REFERENCES `levels` (`Id`),
  CONSTRAINT `FKStairs7` FOREIGN KEY (`Base Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural column types`
--

DROP TABLE IF EXISTS `structural column types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural column types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Column Types1` (`OmniClass Number`),
  KEY `FKStructural Column Types2` (`Assembly Code`),
  CONSTRAINT `FKStructural Column Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKStructural Column Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural columns`
--

DROP TABLE IF EXISTS `structural columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural columns` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Estimated Reinforcement Volume` double DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Structural Material` int(11) DEFAULT NULL,
  `Top Offset` double DEFAULT NULL,
  `Base Offset` double DEFAULT NULL,
  `Top Level` int(11) DEFAULT NULL,
  `Base Level` int(11) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Columns1` (`Type Id`),
  KEY `FKStructural Columns2` (`Phase Created`),
  KEY `FKStructural Columns3` (`Phase Demolished`),
  KEY `FKStructural Columns4` (`Design Option`),
  KEY `FKStructural Columns5` (`Structural Material`),
  CONSTRAINT `FKStructural Columns1` FOREIGN KEY (`Type Id`) REFERENCES `structural column types` (`Id`),
  CONSTRAINT `FKStructural Columns2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Columns3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Columns4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKStructural Columns5` FOREIGN KEY (`Structural Material`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural fabric reinforcement`
--

DROP TABLE IF EXISTS `structural fabric reinforcement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural fabric reinforcement` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Cut Sheet Mass` double DEFAULT NULL,
  `Cut Overall Width` double DEFAULT NULL,
  `Cut Overall Length` double DEFAULT NULL,
  `Minor Lap Splice Length` double DEFAULT NULL,
  `Major Lap Splice Length` double DEFAULT NULL,
  `Location` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Fabric Reinforcement1` (`Type Id`),
  KEY `FKStructural Fabric Reinforcement2` (`Phase Created`),
  KEY `FKStructural Fabric Reinforcement3` (`Phase Demolished`),
  KEY `FKStructural Fabric Reinforcement4` (`Design Option`),
  CONSTRAINT `FKStructural Fabric Reinforcement1` FOREIGN KEY (`Type Id`) REFERENCES `structural fabric reinforcement types` (`Id`),
  CONSTRAINT `FKStructural Fabric Reinforcement2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Fabric Reinforcement3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Fabric Reinforcement4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural fabric reinforcement types`
--

DROP TABLE IF EXISTS `structural fabric reinforcement types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural fabric reinforcement types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Sheet Mass per Unit Area` double DEFAULT NULL,
  `Minor Reinforcement Area` double DEFAULT NULL,
  `Major Reinforcement Area` double DEFAULT NULL,
  `Sheet Mass` double DEFAULT NULL,
  `Minor Spacing` double DEFAULT NULL,
  `Minor Number of Wires` int(11) DEFAULT NULL,
  `Minor End Overhang` double DEFAULT NULL,
  `Minor Start Overhang` double DEFAULT NULL,
  `Width` double DEFAULT NULL,
  `Overall Width` double DEFAULT NULL,
  `Major Spacing` double DEFAULT NULL,
  `Major Number of Wires` int(11) DEFAULT NULL,
  `Major End Overhang` double DEFAULT NULL,
  `Major Start Overhang` double DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Overall Length` double DEFAULT NULL,
  `Minor Direction Wire Type` int(11) DEFAULT NULL,
  `Major Direction Wire Type` int(11) DEFAULT NULL,
  `Physical Material Asset` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Material` int(11) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Fabric Reinforcement Types1` (`Assembly Code`),
  KEY `FKStructural Fabric Reinforcement Types2` (`Material`),
  CONSTRAINT `FKStructural Fabric Reinforcement Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`),
  CONSTRAINT `FKStructural Fabric Reinforcement Types2` FOREIGN KEY (`Material`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural foundation types`
--

DROP TABLE IF EXISTS `structural foundation types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural foundation types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Structural Material` int(11) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Foundation Types1` (`Structural Material`),
  KEY `FKStructural Foundation Types2` (`OmniClass Number`),
  KEY `FKStructural Foundation Types3` (`Assembly Code`),
  CONSTRAINT `FKStructural Foundation Types1` FOREIGN KEY (`Structural Material`) REFERENCES `materials` (`Id`),
  CONSTRAINT `FKStructural Foundation Types2` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKStructural Foundation Types3` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural foundations`
--

DROP TABLE IF EXISTS `structural foundations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural foundations` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Estimated Reinforcement Volume` double DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Elevation at Bottom` double DEFAULT NULL,
  `Width` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Foundations1` (`Type Id`),
  KEY `FKStructural Foundations2` (`Phase Created`),
  KEY `FKStructural Foundations3` (`Phase Demolished`),
  KEY `FKStructural Foundations4` (`Design Option`),
  CONSTRAINT `FKStructural Foundations1` FOREIGN KEY (`Type Id`) REFERENCES `structural foundation types` (`Id`),
  CONSTRAINT `FKStructural Foundations2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Foundations3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Foundations4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural framing`
--

DROP TABLE IF EXISTS `structural framing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural framing` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Estimated Reinforcement Volume` double DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Structural Material` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Cut Length` double DEFAULT NULL,
  `Reference Level` int(11) DEFAULT NULL,
  `Structural Usage` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Framing1` (`Type Id`),
  KEY `FKStructural Framing2` (`Phase Created`),
  KEY `FKStructural Framing3` (`Phase Demolished`),
  KEY `FKStructural Framing4` (`Design Option`),
  KEY `FKStructural Framing5` (`Structural Material`),
  KEY `FKStructural Framing6` (`Level`),
  KEY `FKStructural Framing7` (`Reference Level`),
  CONSTRAINT `FKStructural Framing1` FOREIGN KEY (`Type Id`) REFERENCES `structural framing types` (`Id`),
  CONSTRAINT `FKStructural Framing2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Framing3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Framing4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKStructural Framing5` FOREIGN KEY (`Structural Material`) REFERENCES `materials` (`Id`),
  CONSTRAINT `FKStructural Framing6` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`),
  CONSTRAINT `FKStructural Framing7` FOREIGN KEY (`Reference Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural framing types`
--

DROP TABLE IF EXISTS `structural framing types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural framing types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Fire Rating` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Framing Types1` (`OmniClass Number`),
  KEY `FKStructural Framing Types2` (`Assembly Code`),
  CONSTRAINT `FKStructural Framing Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKStructural Framing Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural rebar`
--

DROP TABLE IF EXISTS `structural rebar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural rebar` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Reinforcement Volume` double DEFAULT NULL,
  `Bend Diameter` double DEFAULT NULL,
  `Schedule Mark` varchar(255) DEFAULT NULL,
  `Style` varchar(255) DEFAULT NULL,
  `Bar Length` double DEFAULT NULL,
  `Shape` int(11) DEFAULT NULL,
  `Spacing` double DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Layout Rule` varchar(255) DEFAULT NULL,
  `Hook At End` int(11) DEFAULT NULL,
  `Hook At Start` int(11) DEFAULT NULL,
  `Total Bar Length` double DEFAULT NULL,
  `Bar Diameter` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Rebar1` (`Type Id`),
  KEY `FKStructural Rebar2` (`Phase Created`),
  KEY `FKStructural Rebar3` (`Phase Demolished`),
  KEY `FKStructural Rebar4` (`Design Option`),
  CONSTRAINT `FKStructural Rebar1` FOREIGN KEY (`Type Id`) REFERENCES `structural rebar types` (`Id`),
  CONSTRAINT `FKStructural Rebar2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Rebar3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Rebar4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural rebar types`
--

DROP TABLE IF EXISTS `structural rebar types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural rebar types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Deformation` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Material` int(11) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Rebar Types1` (`Assembly Code`),
  KEY `FKStructural Rebar Types2` (`Material`),
  CONSTRAINT `FKStructural Rebar Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`),
  CONSTRAINT `FKStructural Rebar Types2` FOREIGN KEY (`Material`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural stiffener types`
--

DROP TABLE IF EXISTS `structural stiffener types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural stiffener types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Stiffener Types1` (`OmniClass Number`),
  KEY `FKStructural Stiffener Types2` (`Assembly Code`),
  CONSTRAINT `FKStructural Stiffener Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKStructural Stiffener Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural stiffeners`
--

DROP TABLE IF EXISTS `structural stiffeners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural stiffeners` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Stiffeners1` (`Type Id`),
  KEY `FKStructural Stiffeners2` (`Phase Created`),
  KEY `FKStructural Stiffeners3` (`Phase Demolished`),
  KEY `FKStructural Stiffeners4` (`Design Option`),
  CONSTRAINT `FKStructural Stiffeners1` FOREIGN KEY (`Type Id`) REFERENCES `structural stiffener types` (`Id`),
  CONSTRAINT `FKStructural Stiffeners2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Stiffeners3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Stiffeners4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural truss types`
--

DROP TABLE IF EXISTS `structural truss types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural truss types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Truss Types1` (`Assembly Code`),
  CONSTRAINT `FKStructural Truss Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `structural trusses`
--

DROP TABLE IF EXISTS `structural trusses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `structural trusses` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKStructural Trusses1` (`Type Id`),
  KEY `FKStructural Trusses2` (`Phase Created`),
  KEY `FKStructural Trusses3` (`Phase Demolished`),
  KEY `FKStructural Trusses4` (`Design Option`),
  CONSTRAINT `FKStructural Trusses1` FOREIGN KEY (`Type Id`) REFERENCES `structural truss types` (`Id`),
  CONSTRAINT `FKStructural Trusses2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Trusses3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKStructural Trusses4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `switch system`
--

DROP TABLE IF EXISTS `switch system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `switch system` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Design Option` int(11) DEFAULT NULL,
  `Switch ID` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKSwitch System1` (`Design Option`),
  CONSTRAINT `FKSwitch System1` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `telephone device types`
--

DROP TABLE IF EXISTS `telephone device types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telephone device types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKTelephone Device Types1` (`OmniClass Number`),
  KEY `FKTelephone Device Types2` (`Assembly Code`),
  CONSTRAINT `FKTelephone Device Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKTelephone Device Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `telephone devices`
--

DROP TABLE IF EXISTS `telephone devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telephone devices` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Panel` varchar(255) DEFAULT NULL,
  `Circuit Number` varchar(255) DEFAULT NULL,
  `Electrical Data` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKTelephone Devices1` (`Type Id`),
  KEY `FKTelephone Devices2` (`Phase Created`),
  KEY `FKTelephone Devices3` (`Phase Demolished`),
  KEY `FKTelephone Devices4` (`Design Option`),
  KEY `FKTelephone Devices5` (`Level`),
  CONSTRAINT `FKTelephone Devices1` FOREIGN KEY (`Type Id`) REFERENCES `telephone device types` (`Id`),
  CONSTRAINT `FKTelephone Devices2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKTelephone Devices3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKTelephone Devices4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKTelephone Devices5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `topography`
--

DROP TABLE IF EXISTS `topography`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topography` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Net cutfill` double DEFAULT NULL,
  `Projected Area` double DEFAULT NULL,
  `Fill` double DEFAULT NULL,
  `Cut` double DEFAULT NULL,
  `Surface Area` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKTopography1` (`Type Id`),
  KEY `FKTopography2` (`Phase Created`),
  KEY `FKTopography3` (`Phase Demolished`),
  KEY `FKTopography4` (`Design Option`),
  CONSTRAINT `FKTopography1` FOREIGN KEY (`Type Id`) REFERENCES `topography types` (`Id`),
  CONSTRAINT `FKTopography2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKTopography3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKTopography4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `topography types`
--

DROP TABLE IF EXISTS `topography types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topography types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKTopography Types1` (`Assembly Code`),
  CONSTRAINT `FKTopography Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `voltage types`
--

DROP TABLE IF EXISTS `voltage types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voltage types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Actual Voltage` double DEFAULT NULL,
  `Minimum Voltage` double DEFAULT NULL,
  `Maximum Voltage` double DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKVoltage Types1` (`Assembly Code`),
  CONSTRAINT `FKVoltage Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `voltages`
--

DROP TABLE IF EXISTS `voltages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voltages` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKVoltages1` (`Type Id`),
  KEY `FKVoltages2` (`Phase Created`),
  KEY `FKVoltages3` (`Phase Demolished`),
  KEY `FKVoltages4` (`Design Option`),
  CONSTRAINT `FKVoltages1` FOREIGN KEY (`Type Id`) REFERENCES `voltage types` (`Id`),
  CONSTRAINT `FKVoltages2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKVoltages3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKVoltages4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wall sweep types`
--

DROP TABLE IF EXISTS `wall sweep types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wall sweep types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Subcategory of Walls` int(11) DEFAULT NULL,
  `Profile` int(11) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Material` int(11) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWall Sweep Types1` (`Profile`),
  KEY `FKWall Sweep Types2` (`Assembly Code`),
  KEY `FKWall Sweep Types3` (`Material`),
  CONSTRAINT `FKWall Sweep Types1` FOREIGN KEY (`Profile`) REFERENCES `profiles` (`Id`),
  CONSTRAINT `FKWall Sweep Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`),
  CONSTRAINT `FKWall Sweep Types3` FOREIGN KEY (`Material`) REFERENCES `materials` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wall sweeps`
--

DROP TABLE IF EXISTS `wall sweeps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wall sweeps` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWall Sweeps1` (`Type Id`),
  KEY `FKWall Sweeps2` (`Phase Created`),
  KEY `FKWall Sweeps3` (`Phase Demolished`),
  KEY `FKWall Sweeps4` (`Design Option`),
  CONSTRAINT `FKWall Sweeps1` FOREIGN KEY (`Type Id`) REFERENCES `wall sweep types` (`Id`),
  CONSTRAINT `FKWall Sweeps2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWall Sweeps3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWall Sweeps4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wall types`
--

DROP TABLE IF EXISTS `wall types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wall types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Structural Material` int(11) DEFAULT NULL,
  `Roughness` int(11) DEFAULT NULL,
  `Absorptance` double DEFAULT NULL,
  `Thermal mass` double DEFAULT NULL,
  `Thermal Resistance (R)` double DEFAULT NULL,
  `Heat Transfer Coefficient (U)` double DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Fire Rating` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  `Width` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWall Types1` (`Structural Material`),
  KEY `FKWall Types2` (`Assembly Code`),
  CONSTRAINT `FKWall Types1` FOREIGN KEY (`Structural Material`) REFERENCES `materials` (`Id`),
  CONSTRAINT `FKWall Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `walls`
--

DROP TABLE IF EXISTS `walls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `walls` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Estimated Reinforcement Volume` double DEFAULT NULL,
  `Volume` double DEFAULT NULL,
  `Area` double DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Length` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Structural Usage` varchar(255) DEFAULT NULL,
  `Top Offset` double DEFAULT NULL,
  `Base Offset` double DEFAULT NULL,
  `Base Constraint` int(11) DEFAULT NULL,
  `Unconnected Height` double DEFAULT NULL,
  `Top Constraint` int(11) DEFAULT NULL,
  `Room Bounding` int(11) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWalls1` (`Type Id`),
  KEY `FKWalls2` (`Phase Created`),
  KEY `FKWalls3` (`Phase Demolished`),
  KEY `FKWalls4` (`Design Option`),
  CONSTRAINT `FKWalls1` FOREIGN KEY (`Type Id`) REFERENCES `wall types` (`Id`),
  CONSTRAINT `FKWalls2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWalls3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWalls4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `window types`
--

DROP TABLE IF EXISTS `window types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `window types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Visual Light Transmittance` double DEFAULT NULL,
  `Solar Heat Gain Coefficient` double DEFAULT NULL,
  `Thermal Resistance (R)` double DEFAULT NULL,
  `Heat Transfer Coefficient (U)` double DEFAULT NULL,
  `OmniClass Number` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Rough Width` double DEFAULT NULL,
  `Rough Height` double DEFAULT NULL,
  `Width` double DEFAULT NULL,
  `Height` double DEFAULT NULL,
  `Operation` varchar(255) DEFAULT NULL,
  `Construction Type` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWindow Types1` (`OmniClass Number`),
  KEY `FKWindow Types2` (`Assembly Code`),
  CONSTRAINT `FKWindow Types1` FOREIGN KEY (`OmniClass Number`) REFERENCES `omniclass numbers` (`OmniClass Number`),
  CONSTRAINT `FKWindow Types2` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `windows`
--

DROP TABLE IF EXISTS `windows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `windows` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Assembly Name` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Host Id` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Head Height` double DEFAULT NULL,
  `Sill Height` double DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWindows1` (`Type Id`),
  KEY `FKWindows2` (`Phase Created`),
  KEY `FKWindows3` (`Phase Demolished`),
  KEY `FKWindows4` (`Design Option`),
  KEY `FKWindows5` (`Level`),
  CONSTRAINT `FKWindows1` FOREIGN KEY (`Type Id`) REFERENCES `window types` (`Id`),
  CONSTRAINT `FKWindows2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWindows3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWindows4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`),
  CONSTRAINT `FKWindows5` FOREIGN KEY (`Level`) REFERENCES `levels` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wire insulation types`
--

DROP TABLE IF EXISTS `wire insulation types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wire insulation types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWire Insulation Types1` (`Assembly Code`),
  CONSTRAINT `FKWire Insulation Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wire insulations`
--

DROP TABLE IF EXISTS `wire insulations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wire insulations` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWire Insulations1` (`Type Id`),
  KEY `FKWire Insulations2` (`Phase Created`),
  KEY `FKWire Insulations3` (`Phase Demolished`),
  KEY `FKWire Insulations4` (`Design Option`),
  CONSTRAINT `FKWire Insulations1` FOREIGN KEY (`Type Id`) REFERENCES `wire insulation types` (`Id`),
  CONSTRAINT `FKWire Insulations2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWire Insulations3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWire Insulations4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wire material types`
--

DROP TABLE IF EXISTS `wire material types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wire material types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWire Material Types1` (`Assembly Code`),
  CONSTRAINT `FKWire Material Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wire materials`
--

DROP TABLE IF EXISTS `wire materials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wire materials` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWire Materials1` (`Type Id`),
  KEY `FKWire Materials2` (`Phase Created`),
  KEY `FKWire Materials3` (`Phase Demolished`),
  KEY `FKWire Materials4` (`Design Option`),
  CONSTRAINT `FKWire Materials1` FOREIGN KEY (`Type Id`) REFERENCES `wire material types` (`Id`),
  CONSTRAINT `FKWire Materials2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWire Materials3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWire Materials4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wire temperature rating types`
--

DROP TABLE IF EXISTS `wire temperature rating types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wire temperature rating types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWire Temperature Rating Types1` (`Assembly Code`),
  CONSTRAINT `FKWire Temperature Rating Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wire temperature ratings`
--

DROP TABLE IF EXISTS `wire temperature ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wire temperature ratings` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWire Temperature Ratings1` (`Type Id`),
  KEY `FKWire Temperature Ratings2` (`Phase Created`),
  KEY `FKWire Temperature Ratings3` (`Phase Demolished`),
  KEY `FKWire Temperature Ratings4` (`Design Option`),
  CONSTRAINT `FKWire Temperature Ratings1` FOREIGN KEY (`Type Id`) REFERENCES `wire temperature rating types` (`Id`),
  CONSTRAINT `FKWire Temperature Ratings2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWire Temperature Ratings3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWire Temperature Ratings4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wire types`
--

DROP TABLE IF EXISTS `wire types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wire types` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Keynote` varchar(255) DEFAULT NULL,
  `Share Neutral Conductor` int(11) DEFAULT NULL,
  `Share Ground Conductor` int(11) DEFAULT NULL,
  `Material` int(11) DEFAULT NULL,
  `Temperature Rating` int(11) DEFAULT NULL,
  `Insulation` int(11) DEFAULT NULL,
  `Max Size` varchar(255) DEFAULT NULL,
  `Neutral Multiplier` double DEFAULT NULL,
  `Neutral Included in Balanced Load` int(11) DEFAULT NULL,
  `Neutral Size` varchar(255) DEFAULT NULL,
  `Conduit Type` varchar(255) DEFAULT NULL,
  `Model` varchar(255) DEFAULT NULL,
  `Manufacturer` varchar(255) DEFAULT NULL,
  `Type Comments` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `Assembly Code` varchar(255) DEFAULT NULL,
  `Family Name` varchar(255) DEFAULT NULL,
  `Type Name` varchar(255) DEFAULT NULL,
  `Type Mark` varchar(255) DEFAULT NULL,
  `Cost` double DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWire Types1` (`Assembly Code`),
  CONSTRAINT `FKWire Types1` FOREIGN KEY (`Assembly Code`) REFERENCES `assembly codes` (`Assembly Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wires`
--

DROP TABLE IF EXISTS `wires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wires` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `Type Id` int(11) DEFAULT NULL,
  `Phase Created` int(11) DEFAULT NULL,
  `Phase Demolished` int(11) DEFAULT NULL,
  `Design Option` int(11) DEFAULT NULL,
  `Circuit Description` varchar(255) DEFAULT NULL,
  `Circuit Load Name` varchar(255) DEFAULT NULL,
  `Number of Conductors` int(11) DEFAULT NULL,
  `Tick Marks` varchar(255) DEFAULT NULL,
  `Panel` varchar(255) DEFAULT NULL,
  `Circuits` varchar(255) DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `Hot Conductors` int(11) DEFAULT NULL,
  `Neutral Conductors` int(11) DEFAULT NULL,
  `Ground Conductors` int(11) DEFAULT NULL,
  `Wire Size` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `Mark` varchar(255) DEFAULT NULL,
  `Learning Content` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FKWires1` (`Type Id`),
  KEY `FKWires2` (`Phase Created`),
  KEY `FKWires3` (`Phase Demolished`),
  KEY `FKWires4` (`Design Option`),
  CONSTRAINT `FKWires1` FOREIGN KEY (`Type Id`) REFERENCES `wire types` (`Id`),
  CONSTRAINT `FKWires2` FOREIGN KEY (`Phase Created`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWires3` FOREIGN KEY (`Phase Demolished`) REFERENCES `phases` (`Id`),
  CONSTRAINT `FKWires4` FOREIGN KEY (`Design Option`) REFERENCES `design options` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `seng2050`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `seng2050` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `seng2050`;

--
-- Current Database: `test`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `test` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `test`;

--
-- Current Database: `user`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `user` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `user`;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` tinyint(4) DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `lastpostdate` datetime DEFAULT NULL,
  `lastuser` int(11) DEFAULT NULL,
  `groupname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `files` (
  `filename` varchar(100) NOT NULL,
  `groupname` varchar(100) NOT NULL,
  PRIMARY KEY (`filename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `groupname` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) DEFAULT NULL,
  `category_id` tinyint(4) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `postcreator` int(11) DEFAULT NULL,
  `content` text,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `topics`
--

DROP TABLE IF EXISTS `topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` tinyint(4) DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `replydate` datetime DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `lastuser` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(20) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `sex` char(1) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `usertype` varchar(35) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `userDB`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `userdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `userDB`;

--
-- Table structure for table `TransactionHistory`
--

DROP TABLE IF EXISTS `TransactionHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TransactionHistory` (
  `transHistID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(10) NOT NULL,
  `transID` int(11) NOT NULL,
  PRIMARY KEY (`transHistID`),
  KEY `userID` (`userID`),
  KEY `transID` (`transID`),
  CONSTRAINT `transactionhistory_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`),
  CONSTRAINT `transactionhistory_ibfk_2` FOREIGN KEY (`transID`) REFERENCES `Transactions` (`transID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Transactions`
--

DROP TABLE IF EXISTS `Transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Transactions` (
  `transID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(10) NOT NULL,
  `purchaseDate` datetime NOT NULL,
  `ticketID` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`transID`),
  UNIQUE KEY `ticketID` (`ticketID`),
  KEY `userID` (`userID`),
  CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserInformation`
--

DROP TABLE IF EXISTS `UserInformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserInformation` (
  `userID` int(10) NOT NULL,
  `userTypeID` int(1) NOT NULL,
  `firstname` char(15) NOT NULL,
  `lastname` char(15) NOT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `joindate` datetime NOT NULL,
  PRIMARY KEY (`userID`),
  KEY `userTypeID` (`userTypeID`),
  CONSTRAINT `userinformation_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`),
  CONSTRAINT `userinformation_ibfk_2` FOREIGN KEY (`userTypeID`) REFERENCES `UserTypes` (`UserTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserSession`
--

DROP TABLE IF EXISTS `UserSession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserSession` (
  `sessionID` varchar(32) NOT NULL,
  `userID` int(10) NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`sessionID`),
  KEY `userID` (`userID`),
  CONSTRAINT `usersession_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserTypes`
--

DROP TABLE IF EXISTS `UserTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserTypes` (
  `UserTypeID` int(1) NOT NULL AUTO_INCREMENT,
  `userType` varchar(20) NOT NULL,
  PRIMARY KEY (`UserTypeID`),
  UNIQUE KEY `userType` (`userType`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `userID` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password_MD5` varchar(32) NOT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-10 22:33:27
